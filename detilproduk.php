<?php
include "connection.php";

$sqlProduk = "SELECT * from produk WHERE id_produk = ".$_GET['id'];
$queryProduk = mysqli_query($con, $sqlProduk);
$getProduk = mysqli_fetch_assoc($queryProduk);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?php echo $getProduk['nama_produk'];?></title>
    <link rel="shortcut icon" href="assets/favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
<!--    <link rel="stylesheet" href="https://sjttransport.gomodo.id/landing/bootstrap/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
<!--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">-->

    <link href="https://sjttransport.gomodo.id/css/material.css" rel="stylesheet"> LAH HOWCOME, bentrok semua, itu cssnya dilokalin aja didonlot dulu masukin local kalo takut berubah
    <link rel="stylesheet" href="https://sjttransport.gomodo.id/css/app.css">
    <link rel="stylesheet" href="css/badge-company.css">
<!--    <link rel="stylesheet" href="https://sjttransport.gomodo.id/css/badge-discount.css">-->
    <link rel="stylesheet" href="https://sjttransport.gomodo.id/css/customer-inline-fix.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>
    <link rel="stylesheet" href="https://sjttransport.gomodo.id/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://sjttransport.gomodo.id/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://sjttransport.gomodo.id/dest-customer/lib/css/touchspin/jquery.bootstrap-touchspin.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <style>
        .address {
            color: #fff;
            padding-bottom: 0.8rem;
            text-align: justify;
        }

        .address p {
            margin-bottom: 0px;
        }

        footer .powered {
            height: auto;
        }

        footer .phone-company span {
            display: inline-flex;
        }

        footer .phone-company {
            margin-top: 1rem;
        }

        footer .phone-company img {
            width: 20px;
        }

        footer p.about, footer .address #address, footer .social {
            font-weight: bold;
            margin-bottom: 0.6rem;
            margin-top: 0;
        }

        footer .address #address {
            margin-bottom: 0.95rem;
        }

        footer .pt-5 {
            padding-top: 2.5rem !important;
        }

        nav.navbar + section {
            margin-top: 49px !important;
        }

        #btn-navbar-toggle {
            cursor: pointer;
            background: none;
            border: none;
            display: none;
        }

        .relative img {
            width: 1.3rem;
        }

        .fa.fa-bars {
            color: #2699FB;
            font-size: 1.5rem;
        }

        .owl-nav .owl-next.disabled, .owl-nav .owl-prev.disabled {
            display: none
        }

        @media  only screen and (max-width: 480px) {
            #btn-navbar-toggle {
                display: block;
            }

            .collapse.navbar-collapse {
                background: #f1f8fd;
                margin-left: -1rem;
                margin-right: -1rem;
                border-top: 1px solid #dcdbdb;
            }

            footer .address #address {
                margin-top: 35px;
            }
        }
    </style>

</head>
<body>
<div class="loading">
    <div class="loading-content">
        <div class="spin">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
        <div class="loading-text">
            Memuat..
        </div>
    </div>
</div>
<nav class="navbar navbar-dark navbar-expand-md bg-style-1 container-fluid navbar-fixed-top" id="topNavbar">
    <div class="container">
        <a href="provider.php?id=<?php echo $getProduk["id_provider"]?>" class="navbar-brand">
            <img src="Images/logo1.png" alt="">
        </a>
        <div class="profile-info">
            <?php
            $sqlProvider = "SELECT * from provider INNER JOIN produk ON produk.id_provider = provider.id_provider WHERE produk.id_produk = ".$_GET['id'];
            $queryProvider = mysqli_query($con, $sqlProvider);
            $getProvider = mysqli_fetch_assoc($queryProvider);
            ?>
            <h3 id="company-name"><?php echo $getProvider['nama_provider'];?></h3>
        </div>
        <span class="relative"></span>
        <button id="btn-navbar-toggle" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item text-center" id="landing-language" role="presentation">
                    <div class="current-language">
                        <div class="description-current-language">
                            Indonesia
                        </div>
                        <div class="flag-current-language">
                            <img class="img-circle" src="assets/idn.png" alt="">
                        </div>
                        <div class="caret-language">
                            <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="language-option" style="display: none;">
                        <ul>
                            <li data-value="en" class="pick-lang">
                                <div class="box-language">
                                    <div class="description-language">
                                        English
                                    </div>
                                    <div class="flag-language">
                                        <img src="https://sewabusbekasi.gomodo.id/landing/img/uk.png" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
<!--                    <form action="#" id="landing-change-language" method="POST">-->
<!--                        <input type="hidden" name="_token" value="27FhwKeNohIIn32jsDFPpbR4MQKDiEBDgWldTquo">-->
<!--                        <input type="hidden" name="lang">-->
<!--                    </form>-->
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content">
    <!-- Breadcrumb -->
    <div class="container pt-5">
        <ul class="breadcrumb">
            <li><a href="provider.php?id=<?php echo $getProvider['id_provider'];?>">Beranda</a></li>
            <li><a><?php echo $getProduk['nama_produk'];?></a></li>
        </ul>
    </div>
    <!-- Content -->
    <div id="product-detail" class="container pb-5">
        <div class="row">
            <div class="col-md-8 left-side-detail">
                <div class="card">
                    <div class="card-header">
                        <div class="owl-carousel">
                            <?php
                            $sqlGambar = "SELECT * FROM picture WHERE id_produk=".$getProduk['id_produk'];
                            $queryGambar = mysqli_query($con, $sqlGambar);
                            while($row=mysqli_fetch_assoc($queryGambar)){
                                ?>
                                <a data-fancybox="gallery" href="Images/<?php echo $row['picture'];?>" data-height="1000">
                                    <img class="card-img-top myImg" id="myImg" src="Images/<?php echo $row['picture'];?>" alt="Card image cap" />
                                </a>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="box-product-tags py-3">
<!--                            PHP LOAD DETAILED CATEGORY-->
                            <?php
                            $sqlDetailedCategory = "SELECT detcat.id_detcat, detcat.nama_detcat FROM brdetcat INNER JOIN detcat ON detcat.id_detcat = brdetcat.id_detcat INNER JOIN produk ON  produk.id_produk = brdetcat.id_produk WHERE brdetcat.id_produk=".$_GET['id'];
                            $queryDetailedCategory = mysqli_query($con, $sqlDetailedCategory);

                            while($rowDet = mysqli_fetch_assoc($queryDetailedCategory)){?>
                                <span class="badge badge-warning product-tags"><?php echo $rowDet['nama_detcat'];?></span>

                            <?php
                            }
                            ?>
                        </div>
                        <h3>
                            <?php echo $getProduk['nama_produk'];?>
                        </h3>
                        <table class="table-product">
                            <tr>
                                <td>
                                    <img src="assets/pin.png" alt="">
                                </td>
<!--                                PHP Load Kota-->
                                <?php
                                $sqlKota = "SELECT kota.nama_kota FROM kota INNER JOIN produk ON produk.id_kota = kota.id_kota WHERE produk.id_kota=".$getProduk['id_kota'];
                                $queryKota = mysqli_query($con, $sqlKota);
                                $getKota = mysqli_fetch_assoc($queryKota);
                                ?>
                                <td colspan="3">
                                    <?php echo $getKota['nama_kota'];?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="assets/wall-clock.png" alt="">
                                </td>
                                <td colspan="3">
                                    <?php echo $getProduk['durasi'];?>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <img src="assets/calendar.png" alt="">
                                </td>
                                <td>
                                    <?= date("d M Y", strtotime($getProduk['tanggal_awal'])) ?>
                                </td>
                                <td>
                                    <span class="badge badge-warning bg-light-blue color-primary-blue">sampai</span>
                                </td>
                                <td>
                                    <?= date('d M Y', strtotime($getProduk['tanggal_akhir'])) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="assets/160-chat.png" alt="">
                                </td>

<!--                                PHP GET GUIDE LANGUAGE-->
                                <?php
                                $sqlLanguage = "SELECT bahasa.id_bahasa, bahasa.nama_bahasa FROM brbahasa INNER JOIN bahasa ON bahasa.id_bahasa = brbahasa.id_bahasa INNER JOIN produk ON produk.id_produk = brbahasa.id_produk WHERE brbahasa.id_produk=".$_GET['id'];
                                $queryLanguage = mysqli_query($con, $sqlLanguage);
                                ?>
                                <td colspan="3">
                                    Dipandu dalam
                                    <?php
                                    while ($rowBahasa = mysqli_fetch_assoc($queryLanguage)){
                                        echo $rowBahasa['nama_bahasa'].", ";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <h3>Tentang "<?php echo $getProduk['nama_produk'];?>"</h3>
                        <p class="card-text">
                        <p><?php echo $getProduk['tentang'];?></p>
                        </p>
                        <hr>
                        <h3>Catatan Penting</h3>
                        <p class="card-text">
                        <p><?php echo $getProduk['catatan'];?></p>
                        </p>
                        <hr>
                        <div class="product-map embed-responsive embed-responsive-21by9">
                            <?php
                            $tempat = str_replace(' ', '+', $getProduk['nama_tempat']);
                            ?>

                            <iframe
                                    width="600" height="450" frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAA0ahi90GdyWNhrBRbIka2GFShNGVPWWs&q=<?php echo $tempat?>"
                                    allowfullscreen>
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <form method="POST" action="form-pemesanan.php?id=<?php echo $_GET['id'];?>">
                    <div class="card">
                        <script>
                            var disabled = [];
                        </script>

                        <div class="row mt-3 embed-remove-d-none d-none">
                            <div class="col-embed-4 mb-3">
                                <div class="owl-carousel mt-1">
                                    <a data-fancybox="gallery-widget" href="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-bus-pariwisata-trac-5.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-bus-pariwisata-trac-5.jpg"
                                                                                                                                                                                           alt="Card image cap"></a>
                                    <a data-fancybox="gallery-widget" href="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-infohargasewabusjakarta-bus-trac-pariwisata.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-infohargasewabusjakarta-bus-trac-pariwisata.jpg"
                                                                                                                                                                                                                 alt="Card image cap"></a>
                                    <a data-fancybox="gallery-widget" href="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-medium-bus1-4.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-medium-bus1-4.jpg"
                                                                                                                                                                                   alt="Card image cap"></a>
                                    <a data-fancybox="gallery-widget" href="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-Rentalbuspariwisata.com-trac-coaster-23s_2.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://sewabusbekasi.gomodo.id/uploads/products/1554629326-Rentalbuspariwisata.com-trac-coaster-23s_2.jpg"
                                                                                                                                                                                                                alt="Card image cap"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body hide-notice-err embed-row">
                            <div class="embed-col-6">
                                <div class="form-group">
                                    <label for="">Pilih Tanggal</label>
                                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"
                                               name="schedule_date"
                                               data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                                        <div class="input-group-append" data-target="#datetimepicker1"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Pax</label>
                                    <input id="banyak" type="number" name="pax" class="form-control touchspin" value="1" data-min="1">
                                    <span hidden id="harga_dasar"><?php echo $getProduk['harga']?></span>
                                </div>
                            </div>
                            <div class="embed-col-6">
                                <div class="bg-light-blue p-3 include">
                                    <h3>Harga termasuk</h3>
                                    <ul>
<!--                                        PHP INCLUSION EXCLUSION LOAD-->
                                        <?php
                                        $sqlInclusion = "SELECT inclusion.id_inclusion, inclusion.nama_inclusion FROM brincl INNER JOIN inclusion ON inclusion.id_inclusion = brincl.id_inclusion INNER JOIN produk ON produk.id_produk = brincl.id_produk WHERE brincl.id_produk=".$_GET['id'];
                                        $queryInclusion = mysqli_query($con, $sqlInclusion);

                                        while($rowIncl = mysqli_fetch_assoc($queryInclusion)){?>
                                            <li><?php echo $rowIncl['nama_inclusion'];?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="bg-light-blue p-3 exclude">
                                    <h3>Harga tidak termasuk</h3>
                                    <ul>
                                        <?php
                                        $sqlExclusion = "SELECT exclusion.id_exclusion, exclusion.nama_exclusion FROM brexcl INNER JOIN exclusion ON exclusion.id_exclusion = brexcl.id_exclusion INNER JOIN produk ON produk.id_produk = brexcl.id_produk WHERE brexcl.id_produk=1".$_GET['id'];
                                        $queryExclusion = mysqli_query($con, $sqlExclusion);

                                        while($rowExcl = mysqli_fetch_assoc($queryExclusion)){?>
                                            <li><?php echo $rowExcl['nama_exclusion'];?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="include text-center bg-light-blue mt-4 pb-3" id="ket" style="padding-top:1rem;display:none;">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-light-blue hide-notice-err">
                            <div class="text-center">
                                <h5>Total Pembayaran</h5>
                                <h3 class="bold" id="total_price"></h3>
                                    <button class="btn btn-primary btn-block" id="btn-book">Pesan</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
<footer class="container-fluid bg-dark">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-4 text-white mb-md-0 mb-3 text-justify">
                <p class="text-white about">
                    Tentang kami
                </p>
                <p class="text-white order-0 about-us">
                <p><?php echo $getProvider['tentang'];?></p>
                <p>&nbsp;</p>
                </p>
            </div>
            <div class="col-md-4 order-1 order-lg-1 d-flex">
                <div class="address">
                    <p id="address">Alamat</p>
                    <p><?php echo $getProvider['alamat'];?></p>
                    <div class="phone-company">
                                <span class="text-white">
                                    <img src="assets/048-telephone-1.png" width="16" alt="">
                                    <span><?php echo $getProvider['nohp'];?></span>
                                </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-5">
                <ul class="social">
                    <p>Media Sosial</p>
                    <li>
                        <a href="mailto:#" class="text-white font-weight-normal">
                            <img src="assets/159-email.png" alt=""> <?php echo $getProvider['email'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal" target="_blank">
                            <img src="assets/099-facebook.png" alt=""> <?php echo $getProvider['fb'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal" target="_blank">
                            <img src="assets/080-instagram.png" alt=""> @<?php echo $getProvider['ig'];?>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <div class="powered">
                    <span>POWERED BY</span> <span class="bold"><a href="home.php">GOMODO</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://sewabusbekasi.gomodo.id/landing/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript" src="https://sewabusbekasi.gomodo.id/js/public/material.js"></script>
<script>
    $(document).on('click', '#btn-navbar-toggle', function () {
        let t = $(this);
        $(t.data('target')).slideToggle(100);
    });

    function loadingStart() {
        $('.loading').addClass('show');
    }

    function loadingFinish() {
        $('.loading').removeClass('show');
    }

    toastr.options = {
        "positionClass": "toast-bottom-right",
    };

    $(document).on('click', '.can-copy', function () {
        let t = $(this);
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val(t.find('.data-copied').text()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success(t.find('.data-copied').text(), 'Copied');
    })
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    // Prevent Char in Input Type Number
    $('input[type="number"]').on('change keydown', function onChange(e) {
        if (e.metaKey == false) { // Enable metakey
            if (e.keyCode > 13 && e.keyCode < 48 && e.keyCode != 39 && e.keyCode != 37 || e.keyCode > 57) {
                e.preventDefault(); // Disable char. Enable arrow
            }
            ;
            if (e.shiftKey === true) { // Disable symbols
                if (e.keyCode > 46 && e.keyCode < 65) {
                    e.preventDefault();
                }
            }
        }
    })
    // disable mousewheel on a input number field when in focus
    // (to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="https://sewabusbekasi.gomodo.id/dest-customer/lib/js/momentjs-id.js"></script>
<script src="https://sewabusbekasi.gomodo.id/dest-customer/lib/js/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="https://sewabusbekasi.gomodo.id/js/owl.carousel.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script>
    let minDate;

    minDate = moment();

    $(function () {

        let maxDate = moment("<?= $getProduk['tanggal_akhir'] ?>", 'YYYY-MM-DD');
        if (minDate > maxDate && maxDate > moment()) {

            $('#show-notice-err').removeClass('d-none').addClass('d-block');
            $('.hide-notice-err').hide();
        } else {
            $('#datetimepicker1').datetimepicker({
                format: 'L',
                // date:moment('MM/DD/YYYY'),
                minDate: minDate.set({hour:0,minute:0,second:0,millisecond:0}).toDate(),
                maxDate: maxDate.set({hour:23,minute:59,second:59,millisecond:0}).toDate(),
                daysOfWeekDisabled: disabled,
                locale: 'id'
            });
        }

        $(".owl-carousel").owlCarousel({
            items: 1,
            lazyLoad: true,
            autoplay: true,
            autoplayHoverPause: true,
            nav: window.location === window.parent.location
        });

        function calculatePrice() {
            let pax = $('input[name="pax"]').val();
            let harga = "<?php echo $getProduk['harga']?>";
            let hargaTotal = harga * pax;
            $('#total_price').text('IDR ' + hargaTotal.toLocaleString('id-ID'))
        }

        let touch = $("input[name='pax']");
        touch.TouchSpin({
            min: touch.data('min'),
            max: touch.data('max')
        });
        calculatePrice();
        $(document).on('change', 'input[name=pax]', function () {
            let pax  = parseInt($(this).val());
            if (isNaN(pax)){
                pax = 0;
            }
            $(this).val(pax)
            calculatePrice();
        });
        $(document).on('change.datetimepicker', '#datetimepicker1', function () {
            calculatePrice();
        });
    });

    function calc()
    {
        var price = document.getElementById("harga_dasar").val();
        var noPax = document.getElementById("banyak").val();
        var total = price * noPax;
        document.getElementById("total_price").innerHTML = total;
        // if (!isNaN(total))

    }

    function zero_val(){
        var zero_text = $('#total_price').text();
        var res = parseInt(zero_text.charAt(4));
        if (res === 0){
            $('#btn-book').prop('disabled', true);
        }
    }
</script>
</body>
</html>
