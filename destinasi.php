<?php
    include "connection.php"
?>
<!DOCTYPE html>
<html>
    <head>

        <title>Explore Ribuan Destinasi Wisata Baru di seluruh Indonesia hanya di Gomodo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="shortcut icon" href="https://gomodo.id/explore-assets/favicon.ico" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <link id="main-style" rel="stylesheet" href="https://gomodo.id/explore-assets/css/style.css">

        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/updates.css">

        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/custom.css">

        <link rel="stylesheet" href="https://gomodo.id/explore-assets/css/responsive.css">
    </head>

    <body>
        <div id="page-wrapper">
            <div class="loading">
                <div class="loading-content">
                    <div class="spin">
                        <i class="fa fa-circle-o-notch fa-spin"></i>
                    </div>
                    <div class="loading-text">
                        Loading ...
                    </div>
                </div>
            </div>
            <div id="provider-modal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <div class="provider-login">
                        <h1>Login Sebagai Partner</h1>
                        <a href="#">
                            <button class="button btn-medium uppercase sky-blue1">Login di sini</button>
                        </a>
                    </div>
                    <div class="provider-signup">
                        <h1>Register Sebagai Partner</h1>
                        <a href="#">
                            <button class="button btn-medium uppercase sky-blue1">Daftar di sini</button>
                        </a>
                    </div>
                </div>
            </div>

            <header id="header" class="navbar-static-top">
                <div class="main-header not_home">
                    <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                        Mobile Menu Toggle
                    </a>
                    <div class="container">
                        <h1 class="gomodo_logo logo navbar-brand">
                            <a href="home.php" title="Gomodo">
                                <img src="assets/logo.png" class="logo-gomodo" alt="Gomodo" />
                                <span class="gomodo_text">GOMODO</span>
                            </a>
                        </h1>
                        <nav id="main-menu" role="navigation">
                            <ul class="menu">
                                <li class="menu-item-has-children text-white">
                                    <a href="home.php">Beranda</a>
                                </li>
                                <li class="text-black ">
                                    <a href="transport.php">Transportasi</a>
                                </li>
                                <li class="text-white ">
                                    <a href="aktivitas.php">Aktivitas</a>
                                </li>
                                <li class=" text-white active">
                                    <a href="destinasi.php">Destinasi</a>
                                </li>
                                <li class="text-white ">
                                    <a href="bantuan.html">Bantuan</a>
                                </li>
                                <li class="provider-login-nav text-white">
                                    <a href="">Partner</a>
                                </li>
                                <li class="ribbon">
                                    <a href="#"><img src="https://gomodo.id/landing/img/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                                    <ul class="menu-mini">
                                        <li class="">
                                            <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="https://gomodo.id/landing/img/uk.png" width="15px"> EN</a>
                                        </li>
                                        <li class="active">
                                            <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="https://gomodo.id/landing/img/idn.png" width="15px"> ID</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <form action="#" id="landing-change-language" method="POST">
                            <input type="hidden" name="_token" value="#">
                            <input type="hidden" name="lang">
                        </form>
                    </div>
                    <nav id="mobile-menu-01" class="mobile-menu collapse">
                        <ul id="mobile-primary-menu" class="menu">
                            <li class="menu-item-has-children">
                                <a href="https://gomodo.id">Beranda</a>
                            </li>
                            <li class="">
                                <a href="https://gomodo.id/explore/transports/search">Transportasi</a>
                            </li>
                            <li class="">
                                <a href="https://gomodo.id/explore/all-activities/search">Aktivitas</a>
                            </li>
                            <li class="">
                                <a href="https://gomodo.id/explore/all-destination">Destinasi</a>
                            </li>
                            <li class="active">
                                <a href="https://gomodo.id/explore/help">Bantuan</a>
                            </li>
                            <li class="provider-login-nav">
                                <a href="">Partner</a>
                            </li>
                        </ul>
                        <ul class="mobile-topnav container">
                            <li class="ribbon language menu-color-skin">
                                <a href="#">Indonesia</a>
                                <ul class="menu mini">
                                    <li class=""><a href="#" id="indonesia" data-value="en" title="English">English</a></li>
                                    <li class="active"><a href="#" id="indonesia" data-value="id" title="Indonesia">Indonesia</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <form action="#" id="landing-change-language" method="POST">
                        <input type="hidden" name="_token" value="#">
                        <input type="hidden" name="lang">
                    </form>
                </div>
            </header>
            <form class="d-none" method="post" action="#" id="change-language">
                <input type="hidden" name="_token" value="#" />
                <input type="hidden" name="lang" value="id" />
            </form>
<!--Content Destinasi-->
            <section id="content">
                <div class="container">
                    <div id="main">

                        <div class="tour-packages row add-clearfix image-box">
                        <?php
                            $sql = "SELECT id_provinsi, nama_provinsi, banner from provinsi";
                            $getProvinsi = mysqli_query($con, $sql);

                            while($row = mysqli_fetch_assoc($getProvinsi)){?>
                            <div class="col-sm-6 col-md-4">
                                <article class="box">
                                    <figure>
                                        <a href="<?php echo 'city.php?id='.$row['id_provinsi']?>" />
                                            <img src="Images/<?php echo $row['banner'];?>">
                                                <figcaption>
                                                <?php

                                                    echo "<h2 class='caption-title'>".$row['nama_provinsi']."</h2>";
                                                ?>
                                                </figcaption>

                                        </a>
                                    </figure>
                                </article>
                            </div>
                        <?php
                            }
                        ?>


                        </div>

                    </div>
                </div>
            </section>

            <footer id="footer">
                <div class="footer-wrapper text-md-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <h2>Temukan</h2>
                                <ul class="discover triangle hover row text-left">
                                    <li class="col-xs-6 "><a href="#">Siaran Pers</a></li>
                                    <li class="col-xs-6 "><a href="#">Tentang Kami</a></li>
                                    <li class="col-xs-6"><a href="#">Bermitra dengan Kami</a></li>
                                    <li class="col-xs-6 "><a href="#">Blog</a></li>
                                    <li class="col-xs-6"><a href="#">Login Mitra</a></li>
                                    <li class="col-xs-6 "><a href="#">Karir</a></li>
                                    <li class="col-xs-6 "><a href="bantuan.html">Bantuan</a></li>
                                    <li class="col-xs-6 "><a href="#">Kebijakan</a></li>
                                    <li class="col-xs-6 "><a href="#">Syarat dan Ketentuan</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h2>Berlangganan Email</h2>
                                <p>Segera berlangganan email untuk mendapatkan penawaran menarik &amp; informasi terbaru dari Gomodo!</p>
                                <br />
                                <div class="form-newsletter">
                                    <form id="homepage-newsletter">
                                        <div class="icon-check text-left">
                                            <input type="email" class="input-text width-email-footer"placeholder="Email Anda" maxlength="50" />
                                            <button class="subscribe loading-button width-button-email-footer">Berlangganan</button>
                                        </div>
                                    </form>
                                </div>
                                <br />
                                <span>Kami menghargai privasi Anda.</span>
                            </div>
                            <div class="col-sm-6 col-md-2">
                                <h2>Gomodo</h2>
                                <p>Discover. Book. Empower.</p>
                                <i class="fa fa-phone-square"> 0274 - 4288 - 422</i>
                                <br />
                                <br />
                                <ul class="social-icons clearfix">
                                    <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a title="instagram" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                    <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-facebook-f"></i></i></a></li>
                                    <li class="youtube"><a title="youtube" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <h2 class="margin-bottom-0">Metode Pembayaran</h2>
                                <div class="bank-icons clearfix text-center">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/bri.png" alt="BRI Bank">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/bni.png" alt="BNI Bank">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/mandiri-bank.png" alt="Mandiri Bank">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/master-card.png" alt="Master Card">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/visa.png" alt="Visa">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/verifiedbyvisa.png" alt="Verified by Visa">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/mastercard-securecode.png" alt="Mastercard Securecode">
                                    <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/ssl.png" alt="SSL">
                                    <a href="https://www.certipedia.com/quality_marks/0000065832" target="_blank">
                                        <img class="image-bank" src="https://gomodo.id/explore-assets/images/payment/pci-dss-watermark.png" alt="PCI" style="width:8rem;">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom gray-area">
                    <div class="container">
                        <div class="logo pull-left">
                            <a href="home.php" title="Gomodo">
                                <img src="https://gomodo.id/explore-assets/images/tour/home/logo.png" alt="Travelo HTML5 Template" />
                            </a>
                        </div>
                        <div class="pull-right back-top">
                            <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                        </div>
                        <div class="copyright pull-right">
                            <p>&copy; 2018 Powered by Gomodo Technologies</p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery-1.11.1.min.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery.noconflict.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/modernizr.2.7.1.min.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery.placeholder.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery-ui.1.10.4.min.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="8a404cf331a53624f055eb9b-text/javascript"></script>

    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/bootstrap.js"></script>

    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/jquery.stellar.min.js"></script>

    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/waypoints.min.js"></script>

    <script type="8a404cf331a53624f055eb9b-text/javascript" src="https://gomodo.id/explore-assets/js/theme-scripts.js"></script>
    <script type="8a404cf331a53624f055eb9b-text/javascript">


        if (typeof gomodo !== 'undefined') {
            if (gomodo.error !== undefined) {
                toastr.error(gomodo.error, 'Terjadi Kesalahan');
            }
            if (gomodo.success !== undefined) {
                toastr.success(gomodo.success);
            }
            if (gomodo.warning !== undefined) {
                toastr.warning(gomodo.warning);
            }
            if (gomodo.info !== undefined) {
                toastr.info(gomodo.info);
            }
        }

        tjq('#main-menu ul li').not('.provider-login-nav, li.language, .ribbon').on('click', function (e) {
            tjq('.loading').addClass('show');
        });

        tjq('.ribbon').click(function(e){
             e.preventDefault();
             tjq('.loading').removeClass('show');
         });
        tjq(document).on('submit', 'form#homepage-newsletter', function (e) {
            e.preventDefault();
            loadingStart('form#homepage-newsletter');
            let data = tjq(this).serialize();
            tjq('form#homepage-newsletter').find('span.error').remove();
            tjq('form#homepage-newsletter').find('span.success').remove()
            loadingEnd('form#homepage-newsletter', 'Berlangganan');
            tjq('form#homepage-newsletter').append(`<span class="success">${res.message}</span>`);
        });


        tjq(document).on('click', '#english, #indonesia', function () {
            let form = tjq('#landing-change-language');
            form.find('input[name=lang]').val(tjq(this).data('value'));
            form.submit();
        });

        // Provider Login Modal
        tjq(document).on('click', '.provider-login-nav', function (e) {
            e.preventDefault();
            tjq('#provider-modal').css('display', 'block');
        })
        tjq(document).on('click', '.close', function () {
            tjq('#provider-modal').css('display', 'none');
        });
        tjq(document).on('click', 'li.language', function (e) {
            e.preventDefault();
        });
        tjq(document).ready(function () {
            tjq('.provider-login-nav').removeClass('active');
        });
        tjq(document).on('click', function (event) {
            if (!tjq(event.target).closest(".modal-content, .provider-login-nav").length) {
                tjq('#provider-modal').css('display', 'none');
            }
        });

        // Prevent Char in Input Type Number
        tjq(document).on('change keydown', 'input[type="number"], input[type="tel"], .number', function (e) {
            if (!digitOnly(e)) {
                e.preventDefault();
            }
        });

        function digitOnly(e) {
            let allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight'];
            if (!allowed.includes(e.key)) {
                return false
            }
            return true
        }

        // Disable Paste in input type number
        tjq(document).on('paste', 'input[type="number"], input[type="tel"], .number', function (e) {
            e.preventDefault();
        });
        // Disable Scroll in input type number
        tjq(document).on('wheel', 'input[type="number"], input[type="tel"], .number', function (e) {
            tjq(this).blur();
        });
    </script>

    </body>
</html>