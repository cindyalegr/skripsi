<?php
include "connection.php";



if(isset($_POST["action"]))
{
    if(isset($_POST["province"])){
        $query = "SELECT produk.* FROM produk 
            LEFT JOIN brdetcat ON produk.id_produk = brdetcat.id_produk 
            LEFT JOIN detcat ON brdetcat.id_detcat = detcat.id_detcat
            LEFT JOIN brbahasa ON brbahasa.id_produk = produk.id_produk
            LEFT JOIN bahasa ON brbahasa.id_bahasa = bahasa.id_bahasa
            LEFT JOIN kota ON kota.id_kota = produk.id_kota
            LEFT JOIN provinsi ON provinsi.id_provinsi = kota.id_provinsi
            WHERE provinsi.id_provinsi ='".$_POST["province"]."'";
    }
    else{
        $query = "SELECT produk.* FROM produk 
            LEFT JOIN brdetcat ON produk.id_produk = brdetcat.id_produk 
            LEFT JOIN detcat ON brdetcat.id_detcat = detcat.id_detcat
            LEFT JOIN brbahasa ON brbahasa.id_produk = produk.id_produk
            LEFT JOIN bahasa ON brbahasa.id_bahasa = bahasa.id_bahasa
            WHERE 
                id_category ='".$_POST["q"]."'";
    }
	        
    if(isset($_POST["keyword"])){
        $query .= "AND nama_produk LIKE '%".$_POST["keyword"]."%'";
    }
    
    if(isset($_POST["minimum_price"]) && !empty($_POST["maximum_price"]))
    {
        $query .= ' AND harga >= "'.intval($_POST["minimum_price"]).'" AND harga <= "'.intval($_POST["maximum_price"]).'"';
    }

    if(isset($_POST["guides"]))
    {
        $guides_filter = implode(",", $_POST["guides"]);
        $query .= "
              AND bahasa.id_bahasa IN (".$guides_filter.")
		  ";
    }
    if(isset($_POST["categories"]))
    {
        $categories_filter = implode(",", $_POST["categories"]);
        $query .= "
		        AND detcat.id_detcat IN(".$categories_filter.")
		  ";
    }

    $query .= "GROUP BY produk.id_produk";

    // debug aja querynya
//    var_dump($query); exit();

    $getQuery = mysqli_query($con, $query);

    $total_row = mysqli_num_rows($getQuery);
    $output = '';


    if($total_row > 0)
    {
        while ($row = mysqli_fetch_assoc($getQuery))
        {
            $sqlKota = "SELECT * FROM kota WHERE id_kota =".$row["id_kota"];
            $getKota = mysqli_query($con, $sqlKota);
            $rowKota = mysqli_fetch_assoc($getKota);
            $harga = number_format($row["harga"]);

            $output .= '
            <div class="col-sm-6 col-md-4">
                <a class="card_product_theme" href="detilproduk.php?id='.$row["id_produk"].'" target="_blank">
                    <article class="box detail_box">
                        <figure>
                            <img class="img-product" src="Images/'.$row["thumbnail"].'" alt="">
                        </figure>
                        <div class="details">
                            <h4 class="box-title">'.$row["nama_produk"].'</h4>
                            <hr>
                            <div class="card_short_desc">'.$row["deskripsi"].'</div>
                            <ul class="features check">
                                <li>'.$rowKota["nama_kota"].'</li>
                                <li> '.$row["durasi"].'</li>
                                <li>'.date("d M Y", strtotime($row['tanggal_awal'])).'
                                 - 
                                 '.date("d M Y", strtotime($row['tanggal_akhir'])).'
                                 </li>
                            </ul>
                            <hr>
                            <span>Mulai Dari: </span>
                            <div class="price"> IDR'.$harga.'</div>
                        </div>
                    </article>
                </a>
            </div>
            ';
        }
    }
    else
    {
        $output = '<h3>No Data Found</h3>';
    }
    echo $output;
}
?>