<?php
include "connection.php"
?>

<!DOCTYPE html>

<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Gomodo | Sistem Pemesanan dan Pembayaran untuk Bisnis Jasa dan Pariwisata</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">
    <!-- Theme Styles -->
    <link href="gomodo_files/css.css" rel="stylesheet">
    <link rel="stylesheet" href="gomodo_files/bootstrap.css">
<!--    <link rel="stylesheet" href="gomodo_files/font-awesome.css">-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="gomodo_files/animate.css">
    <link rel="stylesheet" href="gomodo_files/toastr.css">

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="gomodo_files/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="gomodo_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="gomodo_files/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="gomodo_files/responsive.css">

</head>
<body>
<div id="page-wrapper">
    <div class="loading">
        <div class="loading-content">
            <div class="spin">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
            <div class="loading-text">
                Loading ...
            </div>
        </div>
    </div>
    <div id="provider-modal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="provider-login">
                <h1>Login Sebagai Partner</h1>
                <a href="#">
                    <button class="button btn-medium uppercase sky-blue1">Login di sini</button>
                </a>
            </div>
            <div class="provider-signup">
                <h1>Register Sebagai Partner</h1>
                <a href="#">
                    <button class="button btn-medium uppercase sky-blue1">Daftar di sini</button>
                </a>
            </div>
        </div>
    </div>

    <header id="header" class="navbar-static-top">
        <div class="main-header  main-header-black">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                Mobile Menu Toggle
            </a>
            <div class="container">
                <h1 class="gomodo_logo logo navbar-brand">
                    <a href="home.php" title="Gomodo">
                        <img src="assets/logo.png" class="logo-gomodo" alt="Gomodo">
                        <span class="gomodo_text">GOMODO</span>
                    </a>
                </h1>
                <nav id="main-menu" role="navigation">
                    <ul class="menu">
                        <li class="menu-item-has-children text-white">
                            <a href="home.php">Beranda</a>
                        </li>
                        <li class="text-black">
                            <a href="transport.php">Transportasi</a>
                        </li>
                        <li class="text-white">
                            <a href="aktivitas.php">Aktivitas</a>
                        </li>
                        <li class="text-white">
                            <a href="destinasi.php">Destinasi</a>
                        </li>
                        <li class="text-white">
                            <a href="bantuan.html">Bantuan</a>
                        </li>
                        <li class="provider-login-nav text-white">
                            <a href="">Partner</a>
                        </li>
                        <li class="ribbon">
                            <a href="#"><img src="assets/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                            <ul class="menu-mini">
                                <li class="">
                                    <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="assets/uk.png" width="15px"> EN</a>
                                </li>
                                <li class="active">
                                    <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="assets/idn.png" width="15px"> ID</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <form action="#" id="landing-change-language" method="POST">
                    <input type="hidden" name="_token" value="#">
                    <input type="hidden" name="lang">
                </form>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="home.php">Beranda</a>
                    </li>
                    <li class="">
                        <a href="transport.php">Transportasi</a>
                    </li>
                    <li class="">
                        <a href="aktivitas.php">Aktivitas</a>
                    </li>
                    <li class="">
                        <a href="destinasi.php">Destinasi</a>
                    </li>
                    <li class="">
                        <a href="bantuan.html">Bantuan</a>
                    </li>
                    <li class="provider-login-nav">
                        <a href="">Partner</a>
                    </li>
                </ul>

                <ul class="mobile-topnav container">
                    <li class="ribbon language menu-color-skin">
                        <a href="#">Indonesia</a>
                        <ul class="menu mini" style="">
                            <li class=""><a href="#" id="indonesia" data-value="en" title="English">English</a></li>
                            <li class="active"><a href="#" id="indonesia" data-value="id" title="Indonesia">Indonesia</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <form action="#" id="landing-change-language" method="POST">
                <input type="hidden" name="_token" value="#">
                <input type="hidden" name="lang">
            </form>
        </div>
    </header>
    <form class="d-none" method="post" action="#" id="change-language">
        <input type="hidden" name="_token" value="#" />
        <input type="hidden" name="lang" value="id" />
    </form>

    <!-- section content start here -->
    <section id="content" class="tour">
        <div id="slideshow" class="slideshow-bg full-screen" style="height: 791px;">
            <div class="flexslider">
                <ul class="slides">
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
                        <div class="slidebg" style="background-image: url(assets/hero-1.jpeg);background-size: cover;"></div>
                    </li>
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                        <div class="slidebg" style="background-image: url(assets/hero-2.jpeg);background-size: cover;"></div>
                    </li>
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                        <div class="slidebg" style="background-image: url(assets/hero-3.jpeg);background-size: cover;"></div>
                    </li>
                </ul>
            </div>
            <div class="container" id="homepage">
                <div class="table-wrapper full-width">
                    <div class="table-cell">
                        <div class="heading box">
                            <h1 class="title">Yuk, liburan!</h1>
                            <h3 class="sub-title">Pesan paket liburan anda langsung dari penyedia jasa dan wisata</h3>
                        </div>
                        <div class="row">
                            <div class="search-box col-sm-8 col-sm-offset-2">
                                <form action="aktivitas.php" method="GET">
                                    <div class="row">
                                        <div class="col-md-8 form-group">
                                            <input type="text" class="input-text full-width" name="keyword" placeholder="Cari aktivitas, atau atraksi" maxlength="70">
                                        </div>
                                        <!-- s -->
                                        <div class="col-md-4 row">
                                            <div class="col-sm-12 form-group">
                                                <button class="button btn-medium full-width uppercase sky-blue1"><i class="fa fa-search"></i>Cari
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section global-map-area padding-bottom-0">
            <div class="container">
                <div class="row add-clearfix">
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="gomodo_files/list.svg" alt="author">
                            <div class="description">
                                <h4>Penyedia Jasa dan Wisata Lebih banyak.</h4>
                                <p>Temukan penyedia jasa dan wisata yang lebih menarik dan terpercaya.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.3" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="gomodo_files/money-bag.svg">
                            <div class="description">
                                <h4>Paket Harga Lebih Bervariasi.</h4>
                                <p>Temukan harga terbaik sesuai dengan budget anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.6" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="gomodo_files/shopping-cart.svg" align="">
                            <div class="description">
                                <h4>Proses yang Mudah dan Aman.</h4>
                                <p>Semua proses transaksi dilindungi dengan sertifikat keamanan.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.9" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="gomodo_files/network.svg">

                            <div class="description">
                                <h4>Pesan Langsung dari Penyedia Jasa dan Wisata.</h4>
                                <p>Terhubung langsung dengan penyedia yang terverifikasi asosiasi jasa dan wisata.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section white-bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Destinasi Populer</h1>
                    <p>Temukan petualanganmu selanjutnya</p>
                </div>
                <div class="tour-packages row add-clearfix image-box">
                    <?php
                    $sqlProv = "SELECT * from provinsi LIMIT 6";
                    $getProvinsi = mysqli_query($con, $sqlProv);

                    while($row = mysqli_fetch_assoc($getProvinsi)){?>

                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="<?php echo 'city.php?id='.$row['id_provinsi']?>">
                                    <img src="Images/<?php echo $row['banner'];?>" alt="">
                                    <figcaption>
                                        <h2 class="caption-title"><?php echo $row['nama_provinsi'];?></h2>
                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="btn_explore_des">
                    <a href="destinasi.php" class="button full-width sky-blue1 btn_cus">
                        <i class="soap-icon-departure point_icon"></i>
                        Jelajahi Semua Destinasi
                    </a>
                </div>
            </div>
        </div>
        <div class="section popular-activities new_bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Aktivitas Populer</h1>
                    <p>Pengalaman favorit para wisatawan</p>
                </div>
                <div id="activities_slide" class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">
                    <div class="flex-viewport" style="overflow: hidden; position: relative;">
                        <ul class="slides image-box" style="width: 1400%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
<!--                            -->
                        <?php
                        $sqlAktPop = "SELECT * from produk WHERE id_category = 1 ORDER BY id_produk ASC LIMIT 4";
                        $queryAktPop = mysqli_query($con, $sqlAktPop);
                        while($rowAktPop = mysqli_fetch_assoc($queryAktPop)){
                            ?>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="detilproduk.php?id=<?php echo $rowAktPop['id_produk'];?>" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="Images/<?php echo $rowAktPop['thumbnail'];?>" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title"><?php echo $rowAktPop['nama_produk'];?></h4>
                                            <hr>
                                            <div class="card_short_desc"><?php echo $rowAktPop['deskripsi'];?>
                                            </div>
                                            <ul class="features check">
                                                <?php
                                                $sqlKota = "SELECT kota.nama_kota FROM kota INNER JOIN produk ON produk.id_kota = kota.id_kota WHERE produk.id_kota=".$rowAktPop['id_kota'];
                                                $queryKota = mysqli_query($con, $sqlKota);
                                                $getKota = mysqli_fetch_assoc($queryKota);
                                                ?>
                                                <li><?php echo $getKota['nama_kota'];?></li>
                                                <li><?php echo $rowAktPop['durasi'];?></li>
                                                <li>
                                                    <?php echo date("d M Y", strtotime($rowAktPop['tanggal_awal']));?> - <?php echo date("d M Y", strtotime($rowAktPop['tanggal_akhir']));?>
                                                </li>
                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>
                                            <div class="price" > IDR<?php
                                                $harga = number_format($rowAktPop['harga']);
                                                echo $harga;?></div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section new_bg">
                <div class="container">
                    <div class="text-center description block">
                        <h1>Rekomendasi Gomodo</h1>
                        <p>Pilihan aktivitas terbaik dari kami</p>
                    </div>
                    <div id="activities_slide" class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">
                        <div class="flex-viewport" style="overflow: hidden; position: relative;">
                            <ul class="slides image-box" style="width: 1600%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                <!--                            -->
                                <?php
                                $sqlAktPop = "SELECT * from produk WHERE id_category = 1  ORDER BY id_produk DESC LIMIT 4";
                                $queryAktPop = mysqli_query($con, $sqlAktPop);
                                while($rowAktPop = mysqli_fetch_assoc($queryAktPop)){
                                    ?>
                                    <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                        <a class="card_product_theme" href="detilproduk.php?id=<?php echo $rowAktPop['id_produk'];?>" target="_blank">
                                            <article class="box">
                                                <figure>
                                                    <img class="img-product" src="Images/<?php echo $rowAktPop['thumbnail'];?>" alt="" draggable="false">
                                                </figure>
                                                <div class="details">
                                                    <h4 class="box-title"><?php echo $rowAktPop['nama_produk'];?></h4>
                                                    <hr>
                                                    <div class="card_short_desc"><?php echo $rowAktPop['deskripsi'];?>
                                                    </div>
                                                    <ul class="features check">
                                                        <li><?php echo $getKota['nama_kota'];?></li>
                                                        <li><?php echo $rowAktPop['durasi'];?></li>
                                                        <li>
                                                            <?php echo $rowAktPop['tanggal_awal'];?> - <?php echo $rowAktPop['tanggal_akhir'];?>
                                                        </li>
                                                    </ul>
                                                    <hr>
                                                    <span>Mulai Dari: </span>
                                                    <div class="price"> IDR
                                                        <?php $harga2 = number_format($rowAktPop['harga']);
                                                        echo $harga2;?>
                                                    </div>
                                                </div>
                                            </article>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer start here -->
    <footer id="footer">
        <div class="footer-wrapper text-md-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <h2>Temukan</h2>
                        <ul class="discover triangle hover row text-left">
                            <li class="col-xs-6 "><a href="#">Siaran Pers</a></li>
                            <li class="col-xs-6 "><a href="#">Tentang Kami</a></li>
                            <li class="col-xs-6"><a href="#">Bermitra dengan Kami</a></li>
                            <li class="col-xs-6 "><a href="#">Blog</a></li>
                            <li class="col-xs-6"><a href="#">Login Mitra</a></li>
                            <li class="col-xs-6 "><a href="#">Karir</a></li>
                            <li class="col-xs-6 "><a href="bantuan.html">Bantuan</a></li>
                            <li class="col-xs-6 "><a href="#">Kebijakan</a></li>
                            <li class="col-xs-6 "><a href="#">Syarat dan Ketentuan</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2>Berlangganan Email</h2>
                        <p>Segera berlangganan email untuk mendapatkan penawaran menarik &amp; informasi terbaru dari Gomodo!</p>
                        <br />
                        <!--                        TODO: fix newsletter function-->
                        <div class="form-newsletter">
                            <form id="homepage-newsletter">
                                <div class="icon-check text-left">
                                    <input type="email" class="input-text width-email-footer"placeholder="Email Anda" maxlength="50" />
                                    <button class="subscribe loading-button width-button-email-footer">Berlangganan</button>
                                </div>
                            </form>
                        </div>
                        <br />
                        <span>Kami menghargai privasi Anda.</span>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <h2>Gomodo</h2>
                        <p>Discover. Book. Empower.</p>
                        <i class="fa fa-phone-square"> 0274 - 4288 - 422</i>
                        <br />
                        <br />
                        <ul class="social-icons clearfix">
                            <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li>
                            <li class="googleplus"><a title="instagram" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-facebook-f"></i></i></a></li>
                            <li class="youtube"><a title="youtube" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2 class="margin-bottom-0">Metode Pembayaran</h2>
                        <div class="bank-icons clearfix text-center">
                            <img class="image-bank" src="assets/bri.png" alt="BRI Bank">
                            <img class="image-bank" src="assets/bni.png" alt="BNI Bank">
                            <img class="image-bank" src="assets/mandiri-bank.png" alt="Mandiri Bank">
                            <img class="image-bank" src="assets/master-card.png" alt="Master Card">
                            <img class="image-bank" src="assets/visa.png" alt="Visa">
                            <img class="image-bank" src="assets/verifiedbyvisa.png" alt="Verified by Visa">
                            <img class="image-bank" src="assets/mastercard-securecode.png" alt="Mastercard Securecode">
                            <img class="image-bank" src="assets/ssl.png" alt="SSL">
                            <a href="https://www.certipedia.com/quality_marks/0000065832" target="_blank">
                                <img class="image-bank" src="assets/pci-dss-watermark.png" alt="PCI" style="width:8rem;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom gray-area">
            <div class="container">
                <div class="pull-right back-top">
                    <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p>&copy; 2018 Powered by Gomodo Technologies</p>
                </div>
            </div>
        </div>
    </footer>
</div>
<script type="text/javascript" src="gomodo_files/jquery-1.js"></script>
<script type="text/javascript" src="gomodo_files/jquery_003.js"></script>
<script type="text/javascript" src="gomodo_files/modernizr.js"></script>
<script type="text/javascript" src="gomodo_files/jquery-migrate-1.js"></script>
<script type="text/javascript" src="gomodo_files/jquery_004.js"></script>
<script type="text/javascript" src="gomodo_files/jquery-ui.js"></script>
<script type="text/javascript" src="gomodo_files/jquery_005.js"></script>
<script src="gomodo_files/toastr.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="gomodo_files/bootstrap.js"></script>


<!-- parallax -->
<script type="text/javascript" src="gomodo_files/jquery.js"></script>

<!-- waypoint -->
<script type="text/javascript" src="gomodo_files/waypoints.js"></script>

<!-- load page Javascript -->
<script type="text/javascript" src="gomodo_files/theme-scripts.js"></script>

<script>
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi Kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    tjq('#main-menu ul li').not('.provider-login-nav, li.language, .ribbon').on('click', function (e) {
        tjq('.loading').addClass('show');
    });

    // Provider Login Modal
    tjq(document).on('click', '.provider-login-nav', function (e) {
        e.preventDefault();
        tjq('#provider-modal').css('display', 'block');
    })
    tjq(document).on('click', '.close', function () {
        tjq('#provider-modal').css('display', 'none');
    });
    tjq(document).on('click', 'li.language', function (e) {
        e.preventDefault();
    });
    tjq(document).ready(function () {
        tjq('.provider-login-nav').removeClass('active');
    });
    tjq(document).on('click', function (event) {
        if (!tjq(event.target).closest(".modal-content, .provider-login-nav").length) {
            tjq('#provider-modal').css('display', 'none');
        }
    });

    // Prevent Char in Input Type Number
    tjq(document).on('change keydown', 'input[type="number"], input[type="tel"], .number', function (e) {
        if (!digitOnly(e)) {
            e.preventDefault();
        }
    });

    function digitOnly(e) {
        let allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight'];
        if (!allowed.includes(e.key)) {
            return false
        }
        return true
    }

    // Disable Paste in input type number
    tjq(document).on('paste', 'input[type="number"], input[type="tel"], .number', function (e) {
        e.preventDefault();
    });
    // Disable Scroll in input type number
    tjq(document).on('wheel', 'input[type="number"], input[type="tel"], .number', function (e) {
        tjq(this).blur();
    });
</script>
</body>
</html>