<?php
include "connection.php";
?>

<?php
    $sqlProduk = "SELECT * FROM produk WHERE id_produk=".$_GET['id'];
    $queryProduk = mysqli_query($con, $sqlProduk);
    $getProduk = mysqli_fetch_assoc($queryProduk);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?php echo $getProduk['nama_produk'];?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/landing/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="http://majumuju.kadalmacho.top/css/material.css" rel="stylesheet">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/app.css">
    <link rel="shortcut icon" href="Images/logo1.png">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/badge-company.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/badge-discount.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/customer-inline-fix.css">
    <link href="http://majumuju.kadalmacho.top/additional/select2/css/typography.css" type="text/css" rel="stylesheet"/>
    <link href="http://majumuju.kadalmacho.top/additional/select2/css/textfield.css" type="text/css" rel="stylesheet"/>
    <link href="http://majumuju.kadalmacho.top/additional/select2/css/select2.min.css" type="text/css" rel="stylesheet"/>
    <link href="http://majumuju.kadalmacho.top/additional/select2/css/select2-bootstrap.css" type="text/css" rel="stylesheet"/>
    <link href="http://majumuju.kadalmacho.top/additional/select2/css/pmd-select2.css" type="text/css" rel="stylesheet"/>
    <link href="http://majumuju.kadalmacho.top/materialize/js/plugins/dropify/css/dropify.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/additional/default.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/additional/default.date.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/additional/insurance.css">

    <style>
        .address {
            color: #fff;
            padding-bottom: 0.8rem;
            text-align: justify;
        }

        .address p {
            margin-bottom: 0px;
        }

        footer .powered {
            height: auto;
        }

        footer .phone-company span {
            display: inline-flex;
        }

        footer .phone-company {
            margin-top: 1rem;
        }

        footer .phone-company img {
            width: 20px;
        }

        footer p.about, footer .address #address, footer .social {
            font-weight: bold;
            margin-bottom: 0.6rem;
            margin-top: 0;
        }

        footer .address #address {
            margin-bottom: 0.95rem;
        }

        footer .pt-5 {
            padding-top: 2.5rem !important;
        }

        nav.navbar + section {
            margin-top: 49px !important;
        }

        #btn-navbar-toggle {
            cursor: pointer;
            background: none;
            border: none;
            display: none;
        }

        .relative img {
            width: 1.3rem;
        }

        .fa.fa-bars {
            color: #2699FB;
            font-size: 1.5rem;
        }

        .owl-nav .owl-next.disabled, .owl-nav .owl-prev.disabled {
            display: none
        }

        @media  only screen and (max-width: 480px) {
            #btn-navbar-toggle {
                display: block;
            }

            .collapse.navbar-collapse {
                background: #f1f8fd;
                margin-left: -1rem;
                margin-right: -1rem;
                border-top: 1px solid #dcdbdb;
            }

            footer .address #address {
                margin-top: 35px;
            }
        }
    </style>

</head>
<body>
<div class="loading">
    <div class="loading-content">
        <div class="spin">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
        <div class="loading-text">
            Memuat..
        </div>
    </div>
</div>

<!--PHP DATA LOAD-->
<?php
$sqlProvider = "SELECT * FROM provider WHERE id_provider=".$getProduk['id_provider'];
$queryProvider = mysqli_query($con, $sqlProvider);
$getProvider = mysqli_fetch_assoc($queryProvider);

?>

<nav class="navbar navbar-dark navbar-expand-md bg-style-1 container-fluid navbar-fixed-top" id="topNavbar">
    <div class="container">
        <a href="provider.php?id=<?php echo $getProduk['id_provider'];?>" class="navbar-brand">
            <img src="Images/logo1.png" alt="">
        </a>
        <div class="profile-info">
            <h3 id="company-name"><?php echo $getProvider['nama_provider'];?></h3>
        </div>
        <span class="relative"></span>
        <button id="btn-navbar-toggle" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item text-center" id="landing-language" role="presentation">
                    <div class="current-language">
                        <div class="description-current-language">
                            Indonesia
                        </div>
                        <div class="flag-current-language">
                            <img class="img-circle" src="http://majumuju.kadalmacho.top/landing/img/idn.png" alt="">
                        </div>
                        <div class="caret-language">
                            <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="language-option" style="display: none;">
                        <ul>
                            <li data-value="en" class="pick-lang">
                                <div class="box-language">
                                    <div class="description-language">
                                        English
                                    </div>
                                    <div class="flag-language">
                                        <img src="http://majumuju.kadalmacho.top/landing/img/uk.png" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <form action="#" id="landing-change-language" method="POST">
                        <input type="hidden" name="_token">
                        <input type="hidden" name="lang">
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content">
    <form method="POST" action="countdown-pembayaran.php?id=<?php echo $_GET["id"];?>">
        <div class="bg-light-blue block-height">
            <div class="container pt-5">
                <ul class="breadcrumb">
                    <li><a href="provider.php?id=<?php echo $getProduk['id_provider'];?>">Beranda</a></li>
                    <li><a href="detilproduk.php?id=<?php echo $getProduk['id_produk'];?>"><?php echo $getProduk['nama_produk'];?></a>
                    </li>
                    <li><a>Form Pemesanan</a></li>
                </ul>
            </div>
            <div id="product-booking" class="container pb-5">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2 mb-3">
                                <img class="img-fluid w-100" src="Images/<?php echo $getProduk['thumbnail'];?>" alt="">
                            </div>
                            <div class="col-lg-10">
                                <h3><?php echo $getProduk['nama_produk'];?></h3>
                                <div class="box-product-tags py-3">
                                    <?php
                                    $sqlDetcat = "SELECT detcat.* FROM detcat 
                                        INNER JOIN brdetcat ON brdetcat.id_detcat = detcat.id_detcat
                                        INNER JOIN produk ON produk.id_produk = brdetcat.id_produk
                                        WHERE produk.id_produk =". $_GET["id"];
                                    $getDetcat = mysqli_query($con, $sqlDetcat);
                                    while($rowDetcat = mysqli_fetch_assoc($getDetcat)){
                                    ?>
                                        <span class="badge badge-warning product-tags"><?php echo $rowDetcat["nama_detcat"]?></span>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="table-product">
                                <span>
                                     <img src="assets/pin.png" alt="">
                                </span>
                                    <span class="mr-2 fs-smaller">
                                        <?php
                                        $sqlKota = "SELECT * FROM kota WHERE id_kota=".$getProduk['id_kota'];
                                        $queryKota = mysqli_query($con, $sqlKota);
                                        $getKota = mysqli_fetch_assoc($queryKota);
                                        echo $getKota['nama_kota'];
                                        ?>
                                    </span>
                                        <span>
                                        <img src="assets/calendar.png" alt="">
                                    </span>
                                        <span class="mr-2 fs-smaller">
                                         <?php echo $_POST["schedule_date"];?>
                                     </span>
                                        <span>
                                        <img src="assets/person.png" alt="">
                                    </span>
                                    <span class="mr-2 fs-smaller">
                                        <?php echo $_POST["pax"]?>
                                        Pax
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card mt-3" id="booking-guest">
                    <div class="card-body">
                        <h3 class="bold">Pemesan</h3>
                        <div class="row">
                            <div class="col-lg-6">
                                <input name="departure_date" type="hidden" value="<?php echo $_POST["schedule_date"];?>">
                                <input name="pax" type="hidden" value="<?php echo $_POST["pax"];?>">
<!--                                <input name="product" type="hidden" value="--><?php //echo $_GET["id"];?><!--">-->
                                <div class="md-form __parent_form">
                                    <input type="text" id="full_name" class="form-control" name="full_name" required>
                                    <label for="full_name">Nama Lengkap *</label>
                                </div>
                                <div class="md-form __parent_form">
                                    <input type="email" id="email" class="form-control" name="email" required>
                                    <label for="email">Alamat Email *</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="md-form __parent_form">
                                    <input type="text" id="phone_number" class="form-control number" name="phone_number">
                                    <label for="phone_number">Nomor HP </label>
                                </div>
                            </div>
                        </div>
                        <h3 class="bold">Catatan</h3>
                        <p class="bold">Anda dapat menambahkan catatan untuk operator/provider</p>
                        <div class="row">
                            <div class="col-12">
                                <div class="md-form __parent_form">
                                <textarea name="note" id="note" cols="30" rows="3"
                                          class="form-control md-textarea overflow-auto" maxlength="250"></textarea>
                                    <label for="note">Pesan Anda</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3" id="booking-price">
                    <div class="card-body">
                        <h3 class="bold">Detail harga</h3>
                        <table class="table table-borderless mt-5 tbl-no-padding">
                            <tr>
                                <td>
                                    <?php echo $_POST["pax"];?>
                                    Pax
                                </td>
                                <td class="text-right bold nowrap">
                                    IDR <?php
                                    $banyak = $_POST["pax"];
                                    $hargadasar = $getProduk["harga"];
                                    $total = $banyak * $hargadasar;
                                    echo $total;?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer bg-white">
                        <table class="table table-borderless tbl-no-padding">
                            <tr>
                                <td>
                                    <h3 class="bold">Total</h3>
                                </td>
                                <td class="text-right bold fs-20" id="grandTotal">
                                    IDR <?php echo $total;?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="card mt-3" id="choose-payment">
                    <div class="card-body">
                        <h3 class="bold">Metode Pembayaran</h3>
<!--                        <div class="row">-->
<!--                            <div class="col-sm-12">-->
<!--                                <div class="md-form __parent_form" id="data">-->
<!--                                    <input type="hidden" name="payment_list" value="" class="form-control">-->
<!--                                    <input type="hidden" name="payment_method" value="" class="form-control">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="alfamart" name="pricing_primary"
                                       value="0">
                                <label class="custom-control-label" for="alfamart">
                                    Alfamart
                                </label>
                                <img src="assets/1578893361-Alfamart.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="virtual_account" name="pricing_primary"
                                       value="0">
                                <label class="custom-control-label" for="virtual_account">
                                    Bank Transfer
                                </label>
                                <img src="assets/1578893344-Bank%20Transfer.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="dana" name="pricing_primary"
                                       value="1.5">
                                <label class="custom-control-label" for="dana">
                                    DANA
                                </label>
                                <img src="assets/1578893090-DANA.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="credit_card" name="pricing_primary"
                                       value="2.9">
                                <label class="custom-control-label" for="credit_card">
                                    Kartu Kredit
                                </label>
                                <img src="assets/1586757972-KartuKredit.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="kredivo" name="pricing_primary"
                                       value="0">
                                <label class="custom-control-label" for="kredivo">
                                    Kredivo
                                </label>
                                <img src="assets/1578893320-Kredivo.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="ovo" name="pricing_primary"
                                       value="0">
                                <label class="custom-control-label" for="ovo">
                                    OVO
                                </label>
                                <img src="assets/xendit.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Xendit Payment"
                                       id="ovo_live" name="pricing_primary"
                                       value="1.5">
                                <label class="custom-control-label" for="ovo_live">
                                    OVO
                                </label>
                                <img src="assets/1578893015-OVO.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Midtrans Payment"
                                       id="akulaku" name="pricing_primary"
                                       value="1.7">
                                <label class="custom-control-label" for="akulaku">
                                    AkuLaku
                                </label>
                                <img src="assets/akulaku.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                            <div class="custom-control custom-radio">
                                <input type="radio"
                                       class="choose_payment_method custom-control-input Midtrans Payment"
                                       id="gopay" name="pricing_primary"
                                       value="2">
                                <label class="custom-control-label" for="gopay">
                                    Gopay
                                </label>
                                <img src="assets/1578893170-Gopay.png"
                                     class="img-fluid list_payment midtrans-img"
                                     alt="">
                            </div>
                            <hr class="list-row">
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-white">
            <div class="container py-3">
                <div class="row">
                    <div class="col-12 text-md-right text-center">
                        <button class="btn btn-primary" type="submit" id="booking-pay-now">
                            Pesan Sekarang
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<footer class="container-fluid bg-dark">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-4 text-white mb-md-0 mb-3 text-justify">
                <p class="text-white about">
                    Tentang kami
                </p>
                <p class="text-white order-0 about-us">
                <p><?php echo $getProvider['tentang'];?></p>
                </p>
            </div>
            <div class="col-md-4 order-1 order-lg-1 d-flex">
                <div class="address">
                    <p id="address"> Alamat</p>
                    <p><?php echo $getProvider['alamat'];?></p>

                    <div class="phone-company">
                        <span class="text-white">
                            <img src="assets/048-telephone-1.png" width="16" alt="">
                            <span><?php echo $getProvider['nohp'];?></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-5">
                <ul class="social">
                    <p>Media Sosial</p>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/159-email.png" alt=""> <?php echo $getProvider['email'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/099-facebook.png" alt=""> <?php echo $getProvider['fb'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/080-instagram.png" alt=""> <?php echo $getProvider['ig'];?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <div class="powered">
                    <span>POWERED BY</span> <span class="bold"><a href="home.php">GOMODO</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>


<script type="text/javascript" src="http://majumuju.kadalmacho.top/js/public/material.js"></script>

<script>
    $(document).on('click', '#btn-navbar-toggle', function () {
        let t = $(this);
        $(t.data('target')).slideToggle(100);
    });

    function loadingStart() {
        $('.loading').addClass('show');
    }

    function loadingFinish() {
        $('.loading').removeClass('show');
    }

    toastr.options = {
        "positionClass": "toast-bottom-right",
    };

    $(document).on('click', '.can-copy', function () {
        let t = $(this);
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val(t.find('.data-copied').text()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success(t.find('.data-copied').text(), 'Copied');
    })
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    // Prevent Char in Input Type Number
    $('input[type="number"]').on('change keydown', function onChange(e) {
        if (e.metaKey == false) { // Enable metakey
            if (e.keyCode > 13 && e.keyCode < 48 && e.keyCode != 39 && e.keyCode != 37 || e.keyCode > 57) {
                e.preventDefault(); // Disable char. Enable arrow
            }
            ;
            if (e.shiftKey === true) { // Disable symbols
                if (e.keyCode > 46 && e.keyCode < 65) {
                    e.preventDefault();
                }
            }
        }
    })
    // disable mousewheel on a input number field when in focus
    // (to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })

</script>
<script src="http://majumuju.kadalmacho.top/additional/select2/js/global.js"></script>
<script src="http://majumuju.kadalmacho.top/dest-operator/reskin_global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="http://majumuju.kadalmacho.top/dest-customer/lib/js/momentjs-id.js"></script>
<script src="http://majumuju.kadalmacho.top/additional/select2/js/textfield.js"></script>
<script src="http://majumuju.kadalmacho.top/additional/select2/js/select2.full.js"></script>
<script src="http://majumuju.kadalmacho.top/additional/select2/js/pmd-select2.js"></script>
<script src="http://majumuju.kadalmacho.top/additional/picker.js"></script>
<script src="http://majumuju.kadalmacho.top/additional/picker.date.js"></script>
<script type="text/javascript" src="http://majumuju.kadalmacho.top/materialize/js/plugins/dropify/js/dropify.min.js"></script>
</body>
</html>