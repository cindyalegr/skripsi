<?php

include "connection.php";
//GET DETAILED CATEGORY
$sqlKategori = "SELECT detcat.id_detcat, detcat.nama_detcat, COUNT(produk.id_produk) AS jumlah FROM brdetcat INNER JOIN detcat ON detcat.id_detcat = brdetcat.id_detcat INNER JOIN produk ON produk.id_produk = brdetcat.id_produk GROUP BY detcat.id_detcat";
$getKategori = mysqli_query($con, $sqlKategori);

//GET BAHASA
$sqlBahasa = "SELECT bahasa.id_bahasa, bahasa.nama_bahasa, COUNT(produk.id_produk) AS jumlah FROM brbahasa INNER JOIN bahasa ON bahasa.id_bahasa = brbahasa.id_bahasa INNER JOIN produk ON produk.id_produk = brbahasa.id_produk GROUP BY bahasa.id_bahasa";
$getBahasa = mysqli_query($con, $sqlBahasa);

//GET PROVINSI
?>