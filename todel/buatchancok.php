
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>YOUR GOAL PLACE TRAVELLING</title>
    <meta name="keywords" content="DESTINATION GOMODOers"/>
    <meta name="description" content="TRAVELING GOMODOers">
    <meta name="author" content="Gomodo Technologies">
    <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta property="og:image" content="http://kadalmacho.top/landing/img/Logo.png"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image:alt" content="Gomodo | Sistem Pemesanan dan Pembayaran untuk Bisnis Jasa dan Pariwisata"/>
    <meta property="og:url" content="http://kadalmacho.top"/>
    <meta property="fb:app_id" content="887163081476612"/>
    <meta property="og:title" content="YOUR GOAL PLACE TRAVELLING"/>
    <meta property="og:description"
          content="TRAVELING GOMODOers"/>
    <meta name="twitter:card" content="summary"/>

    <!-- Meta Tags -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://kadalmacho.top/explore-assets/favicon.ico" type="image/x-icon">
    <!-- Theme Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="http://kadalmacho.top/explore-assets/css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="http://kadalmacho.top/explore-assets/css/ie.css"/>
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
<div id="page-wrapper">

    <div class="loading">
        <div class="loading-content">
            <div class="spin">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
            <div class="loading-text">
                Memuat..
            </div>
        </div>
    </div>

    <div id="provider-modal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="provider-login">
                <h1>Login Sebagai Partner</h1>
                <a href="http://kadalmacho.top/agent/login">
                    <button class="button btn-medium uppercase sky-blue1">Login di sini</button>
                </a>
            </div>
            <div class="provider-signup">
                <h1>Register Sebagai Partner</h1>
                <a href="http://kadalmacho.top/partner-with-us">
                    <button class="button btn-medium uppercase sky-blue1">Daftar di sini</button>
                </a>
            </div>
        </div>
    </div>

    <!-- header start here -->
    <header id="header" class="navbar-static-top">















        <div class="main-header not_home">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                Mobile Menu Toggle
            </a>
            <div class="container">
                <h1 class="gomodo_logo logo navbar-brand">
                    <a href="http://kadalmacho.top" title="Gomodo">
                        <img src="http://kadalmacho.top/explore-assets/images/logo.png" class="logo-gomodo" alt="Gomodo"/>
                        <span class="gomodo_text">GOMODO</span>
                    </a>
                </h1>
                <nav id="main-menu" role="navigation">
                    <ul class="menu">
                        <li class="menu-item-has-children text-white">
                            <a href="http://kadalmacho.top">Beranda</a>
                        </li>
                        <li class="text-black ">
                            <a href="http://kadalmacho.top/explore/transports/search">Transportasi</a>
                        </li>



                        <li class="text-white ">
                            <a href="http://kadalmacho.top/explore/all-activities/search">Aktivitas</a>
                        </li>
                        <li class=" text-white ">
                            <a href="http://kadalmacho.top/explore/all-destination">Destinasi</a>
                        </li>
                        <li class="text-white ">
                            <a href="http://kadalmacho.top/explore/help">Bantuan</a>
                        </li>
                        <li class="provider-login-nav text-white">
                            <a href="">Partner</a>
                        </li>





                        <li class="ribbon">
                            <a href="#"><img src="http://kadalmacho.top/landing/img/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                            <ul class="menu-mini">
                                <li class="">
                                    <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="http://kadalmacho.top/landing/img/uk.png" width="15px"> EN</a>
                                </li>
                                <li class="active">
                                    <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="http://kadalmacho.top/landing/img/idn.png" width="15px"> ID</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <form action="http://kadalmacho.top/change-language" id="landing-change-language" method="POST">
                    <input type="hidden" name="_token" value="ij5l72vEsdNVqyijyDcB60qKLXOenIfejodOn8Jr">
                    <input type="hidden" name="lang">
                </form>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="http://kadalmacho.top">Beranda</a>
                    </li>
                    <li class="">
                        <a href="http://kadalmacho.top/explore/transports/search">Transportasi</a>
                    </li>



                    <li class="">
                        <a href="http://kadalmacho.top/explore/all-activities/search">Aktivitas</a>
                    </li>
                    <li class="">
                        <a href="http://kadalmacho.top/explore/all-destination">Destinasi</a>
                    </li>
                    <li class="">
                        <a href="http://kadalmacho.top/explore/help">Bantuan</a>
                    </li>
                    <li class="provider-login-nav">
                        <a href="">Partner</a>
                    </li>

                </ul>

                <ul class="mobile-topnav container">
                    <li class="ribbon language menu-color-skin">

                        <a href="#">Indonesia</a>
                        <ul class="menu mini">
                            <li class=""><a href="#" id="indonesia" data-value="en" title="English">English</a></li>
                            <li class="active"><a href="#" id="indonesia" data-value="id" title="Indonesia">Indonesia</a></li>

                        </ul>
                    </li>
                </ul>
            </nav>
            <form action="http://kadalmacho.top/change-language" id="landing-change-language" method="POST">
                <input type="hidden" name="_token" value="ij5l72vEsdNVqyijyDcB60qKLXOenIfejodOn8Jr">
                <input type="hidden" name="lang">
            </form>
        </div>
    </header>
    <form class="d-none" method="post" action="http://kadalmacho.top/change-language" id="change-language">
        <input type="hidden" name="_token" value="ij5l72vEsdNVqyijyDcB60qKLXOenIfejodOn8Jr" />
        <input type="hidden" name="lang" value="id" />
    </form>

    <!-- section content start here -->
    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Eksplor Aktivitas di Aceh Darussalam</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="http://kadalmacho.top">Beranda</a></li>
                <li class="active">Aktivitas di Aceh Darussalam</li>
            </ul>
        </div>
    </div>
    <section id="content">
        <input name="q" type="hidden">
        <input name="city" type="hidden" value="1">
        <input name="transport" type="hidden" value="">
        <div class="container all-activities">
            <div id="main">
                <div class="lds-dual-ring display-none"></div>
                <div class="row">
                    <div class="col-sm-4 col-md-3 sidebar">
                        <h4 class="search-results-title">
                            <i class="soap-icon-search"></i>
                            <div class="total-product-found-container">
                                <b id="total-product">0</b> hasil pencarian ditemukan
                            </div>
                        </h4>
                        <div class="toggle-container filters-container">
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#price-filter"
                                       class="collapsed">Harga</a>
                                </h4>
                                <div id="price-filter" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div id="price-range"></div>
                                        <br/>
                                        <span class="min-price-label pull-left"></span>
                                        <span class="max-price-label pull-right"></span>
                                        <div class="clearer"></div>
                                    </div><!-- end content -->
                                </div>
                            </div>
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#accomodation-type-filter"
                                       class="collapsed">Aktivitas</a>
                                </h4>
                                <div id="accomodation-type-filter" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <ul class="check-square filters-option" id="product-categories">
                                            <li data-id="1"><a
                                                    href="#">Panjat Tebing
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="3"><a
                                                    href="#">Tur Aktif
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="4"><a
                                                    href="#">Wisata Petualangan
                                                    <small>(3)
                                                    </small>

                                                </a></li>
                                            <li data-id="5"><a
                                                    href="#">Penerbangan Akrobatik
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="6"><a
                                                    href="#">Pengalaman dengan Binatang/Menghandel Binatang
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="7"><a
                                                    href="#">Akuarium dan Kebun Binatang
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="14"><a
                                                    href="#">Persewaan Sepeda
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="15"><a
                                                    href="#">Wisata dengan Kapal Motor
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="21"><a
                                                    href="#">Kemah
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="24"><a
                                                    href="#">Sewa Mobil
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="28"><a
                                                    href="#">Wisata Perkotaan
                                                    <small>(2)
                                                    </small>

                                                </a></li>
                                            <li data-id="46"><a
                                                    href="#">Olahraga Ekstrem
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="53"><a
                                                    href="#">Belajar Simulator Penerbangan
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="57"><a
                                                    href="#">Wisata Naik Mobil Beroda Besar
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="70"><a
                                                    href="#">Hiking dan Trekking
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="100"><a
                                                    href="#">Naik ATV
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="126"><a
                                                    href="#">Akrobatik
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="136"><a
                                                    href="#">Wisata Berjalan
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id=""><a
                                                    href="#">
                                                    Non Kategori
                                                    <small>(10)
                                                    </small>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#language-filter"
                                       class="collapsed">Bahasa Pemandu Aktivitas (Jika ada)</a>
                                </h4>
                                <div id="language-filter" class="panel-collapse collapse">
                                    <div class="panel-content">

                                        <ul class="check-square filters-option" id="guides">
                                            <li data-id="2"><a
                                                    href="#">English
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="6"><a
                                                    href="#">French
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="7"><a
                                                    href="#">Italian
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="8"><a
                                                    href="#">Japanese Language
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                            <li data-id="9"><a
                                                    href="#">Javanese Language
                                                    <small>(1)
                                                    </small>

                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button class="btn-medium uppercase sky-blue1 full-width"
                                    id="apply-filter"
                                    data-lang="Saring">Saring
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="activities_list">
                            <div class="lds-dual-ring display-none"></div>
                            <div class="row image-box hotel listing-style1">

                            </div>
                        </div>
                        <a id="load-more"
                           class="button uppercase full-width btn-large box sky-blue1 loading-button"
                           data-lang="Memuat lebih banyak daftar">Memuat lebih banyak daftar</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- footer start here -->
    <footer id="footer">
        <div class="footer-wrapper text-md-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <h2>Temukan</h2>
                        <ul class="discover triangle hover row text-left">
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/pres-releases">Siaran Pers</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/about_us">Tentang Kami</a></li>
                            <li class="col-xs-6"><a href="http://kadalmacho.top/partner-with-us">Bermitra dengan Kami</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/blog">Blog</a></li>
                            <li class="col-xs-6"><a href="http://kadalmacho.top/agent/login">Login Mitra</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/careers">Karir</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/help">Bantuan</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/policy">Kebijakan</a></li>
                            <li class="col-xs-6 "><a href="http://kadalmacho.top/explore/term-condition">Syarat dan Ketentuan</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2>Berlangganan Email</h2>
                        <p>Segera berlangganan email untuk mendapatkan penawaran menarik &amp; informasi terbaru dari Gomodo!</p>
                        <br/>
                        <div class="form-newsletter">
                            <form method="GET" action="http://kadalmacho.top/explore/city/search" accept-charset="UTF-8" id="homepage-newsletter">
                                <div class="icon-check text-left">
                                    <input type="email" class="input-text width-email-footer" name="email" placeholder="Email Anda" maxlength="50"/>
                                    <button class="subscribe loading-button width-button-email-footer">Berlangganan</button>
                                </div>
                            </form>
                        </div>

                        <br/>
                        <span>Kami menghargai privasi Anda.</span>
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <h2>Gomodo</h2>
                        <p>Discover. Book. Empower.</p>
                        <i class="fa fa-phone-square"> 0274 - 4288 - 422</i>
                        <br/>
                        <br/>
                        <ul class="social-icons clearfix">
                            <li class="twitter"><a title="twitter" href="https://twitter.com/Gomodo_official" data-toggle="tooltip" target="_blank"><i
                                        class="soap-icon-twitter"></i></a></li>
                            <li class="googleplus"><a title="instagram" href="https://www.instagram.com/gomodo.official/" data-toggle="tooltip" target="_blank"><i
                                        class="soap-icon-instagram"></i></a></li>
                            <li class="facebook"><a title="facebook" href="https://www.facebook.com/gomodo.official/" data-toggle="tooltip" target="_blank"><i
                                        class="soap-icon-facebook"></i></a></li>
                            <li class="youtube"><a title="youtube" href="https://www.youtube.com/channel/UC63SauNWdORLCBh5J9U5T9g" data-toggle="tooltip" target="_blank"><i
                                        class="soap-icon-youtube"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <h2 class="margin-bottom-0">Metode Pembayaran</h2>
                        <div class="bank-icons clearfix text-center">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/bri.png" alt="BRI Bank">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/bni.png" alt="BNI Bank">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/mandiri-bank.png" alt="Mandiri Bank">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/master-card.png" alt="Master Card">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/visa.png" alt="Visa">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/verifiedbyvisa.png" alt="Verified by Visa">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/mastercard-securecode.png" alt="Mastercard Securecode">
                            <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/ssl.png" alt="SSL">
                            <a href="https://www.certipedia.com/quality_marks/0000065832" target="_blank">
                                <img class="image-bank" src="http://kadalmacho.top/explore-assets/images/payment/pci-dss-watermark.png" alt="PCI" style="width:8rem;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom gray-area">
            <div class="container">
                <div class="logo pull-left">
                    <a href="index.html" title="Gomodo">
                        <img src="http://kadalmacho.top/explore-assets/images/tour/home/logo.png" alt="Travelo HTML5 Template"/>
                    </a>
                </div>
                <div class="pull-right back-top">
                    <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i
                            class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p>&copy; 2018 Powered by Gomodo Technologies</p>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- Javascript -->
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery.noconflict.js"></script>
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/modernizr.2.7.1.min.js"></script>
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery-ui.1.10.4.min.js"></script>
<script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/bootstrap.js"></script>


<!-- parallax -->
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/jquery.stellar.min.js"></script>

<!-- waypoint -->
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/waypoints.min.js"></script>

<!-- load page Javascript -->
<script type="text/javascript" src="http://kadalmacho.top/explore-assets/js/theme-scripts.js"></script>

<script>
    tjq(document).ready(function () {
        if (localStorage.getItem('sidebar') == 1) {
            tjq('#sidebar-recent-post-blog').toggleClass('active');
            tjq('#sidebar-popular-post-blog').removeClass('active');
            tjq('#recent-posts').toggleClass('in active');
            tjq('#popular-posts').removeClass('in active');
        }
    });
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    tjq('#main-menu ul li').not('.provider-login-nav, li.language, .ribbon').on('click', function (e) {
        tjq('.loading').addClass('show');
    });
    // tjq('.ribbon').click(function(e){
    //     e.preventDefault();
    //     tjq('.loading').removeClass('show');
    // });
    tjq(document).on('submit', 'form#homepage-newsletter', function (e) {
        e.preventDefault();
        loadingStart('form#homepage-newsletter');
        let data = tjq(this).serialize();
        tjq('form#homepage-newsletter').find('span.error').remove();
        tjq('form#homepage-newsletter').find('span.success').remove()
        tjq.ajax({
            url: "http://kadalmacho.top/explore/subscribe",
            data: data,
            success: function (res) {
                loadingEnd('form#homepage-newsletter', 'Berlangganan');
                tjq('form#homepage-newsletter').append(`<span class="success">${res.message}</span>`);
            },
            error: function (err) {
                let e;
                if (err.status === 422) {
                    e = err.responseJSON.errors.email[0];

                } else {
                    e = err.responseJSON.message
                }
                loadingEnd('form#homepage-newsletter', 'Berlangganan');
                tjq('form#homepage-newsletter').append(`<span class="error">${e}</span>`)
            }
        })
    });

    tjq('.recent-post-blog').click(function () {
        localStorage.setItem('sidebar', 1);
    });

    tjq('.popular-post-blog').click(function () {
        localStorage.removeItem('sidebar');
    });

    function loadingStart(place) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.addClass('loader');
        loaderButton.attr('disabled', true);
        loaderButton.html('<span class="lds-dual-ring"></span>Loading');
    }

    function loadingEnd(place, initialText) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.removeClass('loader');
        loaderButton.attr('disabled', false);
        loaderButton.html(initialText);
    }

    tjq(document).on('click', '#english, #indonesia', function () {
        let form = tjq('#landing-change-language');
        form.find('input[name=lang]').val(tjq(this).data('value'));
        form.submit();
    });

    // Provider Login Modal
    tjq(document).on('click', '.provider-login-nav', function (e) {
        e.preventDefault();
        tjq('#provider-modal').css('display', 'block');
    })
    tjq(document).on('click', '.close', function () {
        tjq('#provider-modal').css('display', 'none');
    });
    tjq(document).on('click', 'li.language', function (e) {
        e.preventDefault();
    });
    tjq(document).ready(function () {
        tjq('.provider-login-nav').removeClass('active');
    });
    tjq(document).on('click', function (event) {
        if (!tjq(event.target).closest(".modal-content, .provider-login-nav").length) {
            tjq('#provider-modal').css('display', 'none');
        }
    });

    // Prevent Char in Input Type Number
    tjq(document).on('change keydown', 'input[type="number"], input[type="tel"], .number', function (e) {
        if (!digitOnly(e)) {
            e.preventDefault();
        }
    });

    function digitOnly(e) {
        let allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight'];
        if (!allowed.includes(e.key)) {
            return false
        }
        return true
    }

    // Disable Paste in input type number
    tjq(document).on('paste', 'input[type="number"], input[type="tel"], .number', function (e) {
        e.preventDefault();
    });
    // Disable Scroll in input type number
    tjq(document).on('wheel', 'input[type="number"], input[type="tel"], .number', function (e) {
        tjq(this).blur();
    });
</script>
<script>
    let l = 'Menunggu..';
    let page = 1;
    let price = null;
    let total = 0;

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    tjq("#price-range").slider({
        range: true,
        min: 0,
        max: 100000000,
        step: 100000,
        values: [0, 0],
        slide: function (event, ui) {
            if((ui.values[1] - ui.values[0]) <= 1000000){
                return false;
            }
            tjq(".min-price-label").html("IDR " + formatNumber(ui.values[0]));
            tjq(".max-price-label").html("IDR " + formatNumber(ui.values[1]));
            price = ui.values[0] + '-' + ui.values[1];
        }
    });
    tjq('#price-range').draggable();
    tjq(".min-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 0)));
    tjq(".max-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 1)));
    price = tjq("#price-range").slider("values", 0) + '-' + tjq("#price-range").slider("values", 1);

    function render() {
        tjq(document).find('#content button').prop('disabled', true).html(l);
        if (page === 1) {
            loadingStart('#main');
            tjq('.listing-style1').empty();
        }
        let keyword = tjq('input[name=q]').val();
        let city = tjq('input[name=city]').val();
        let sort = tjq('input[name=sortBy]').val();
        let transport = tjq('input[name=transport]').val();
        let data = {
            q: keyword,
            city: city,
            sort: sort,
            price: price,
            page: page,
            transport: transport,
            guides: getGuides(),
            categories: getCategories()
        };

        if (page !== undefined || page !== '' || page !== null) {
            tjq.ajax({
                url: "http://kadalmacho.top/explore/render",
                type: "POST",
                data: data,
                dataType: 'html',
                success: function (data) {
                    loadingEnd('#main','Memuat lebih banyak destinasi');
                    tjq('#main .lds-dual-ring').show();
                    if (page === 1) {
                        tjq('.listing-style1').html(data)
                    } else {
                        tjq('.listing-style1').append(data);
                    }
                    if (tjq(document).find('.pagination-product').length > 0) {
                        tjq('#total-product').text(formatNumber(tjq(document).find('.pagination-product').data('total')));
                        if (tjq(document).find('.pagination-product').attr('data-nextpage') !== '') {
                            page = tjq(document).find('.pagination-product').attr('data-nextpage');
                        } else {
                            page = null
                        }

                    } else {
                        page = null

                    }
                    tjq('#main .lds-dual-ring').hide();
                    tjq(document).find('.pagination-product').remove();
                    if (page === null) {
                        tjq('#load-more').remove();
                    }
                    tjq(document).find('#content button').each(function () {
                        tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
                    })
                },
                error: function (e) {
                    console.log(e)
                    tjq(document).find('#content button').each(function () {
                        tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
                    })
                }
            })
        } else {
            tjq('#load-more').remove();
            tjq(document).find('#content button').each(function () {
                tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
            })
        }
    }

    render();

    tjq(document).on('click', '#load-more', function () {
        if (!tjq(this).prop('disabled')) {
            loadingStart('#main');
            render(page)
        }
    });
    tjq(document).on('click', '#apply-filter', function () {
        if (!tjq(this).prop('disabled')) {
            page = 1;
            render(page)
        }
    });

    function getGuides() {
        let guides = [];
        tjq(document).find('#guides li.active').each(function (i, data) {
            guides.push(tjq(data).data('id'));
        });
        console.log(guides);
        return guides;
    }

    function getCategories() {
        let categories = [];
        tjq(document).find('#product-categories li.active').each(function (i, data) {
            categories.push(tjq(data).data('id'));
        });
        console.log(categories);
        return categories;
    }
</script>

</body>
</html>