<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!-- Page Title -->
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="Gomodo Technologies">
    <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT">
    <meta http-equiv="pragma" content="no-cache">
    <meta property="og:image" content="https://gomodo.id/landing/img/Logo.png">
    <meta property="og:type" content="website">
    <meta property="og:image:alt" content="Gomodo | Sistem Pemesanan dan Pembayaran untuk Bisnis Jasa dan Pariwisata">
    <meta property="og:url" content="https://gomodo.id">
    <meta property="fb:app_id" content="887163081476612">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta name="twitter:card" content="summary">

    <!-- Meta Tags -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://gomodo.id/explore-assets/favicon.ico" type="image/x-icon">
    <!-- Theme Styles -->
    <link href="../gomodo_files/css.css" rel="stylesheet">
    <link rel="stylesheet" href="../gomodo_files/bootstrap.css">
    <link rel="stylesheet" href="../gomodo_files/font-awesome.css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../gomodo_files/animate.css">
    <link rel="stylesheet" href="../gomodo_files/toastr.css">

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="../gomodo_files/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="../gomodo_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="../gomodo_files/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="../gomodo_files/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="https://gomodo.id/explore-assets/css/ie.css"/>
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
<div id="page-wrapper">

    <div class="loading">
        <div class="loading-content">
            <div class="spin">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
            <div class="loading-text">
                Loading ...
            </div>
        </div>
    </div>

    <div id="provider-modal" class="modal">
        <div class="modal-content">
            <span class="close">×</span>
            <div class="provider-login">
                <h1>Login Sebagai Partner</h1>
                <a href="https://gomodo.id/agent/login">
                    <button class="button btn-medium uppercase sky-blue1">Login di sini</button>
                </a>
            </div>
            <div class="provider-signup">
                <h1>Register Sebagai Partner</h1>
                <a href="https://gomodo.id/partner-with-us">
                    <button class="button btn-medium uppercase sky-blue1">Daftar di sini</button>
                </a>
            </div>
        </div>
    </div>

    <!-- header start here -->
    <header id="header" class="navbar-static-top">















        <div class="main-header  main-header-black">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                Mobile Menu Toggle
            </a>
            <div class="container">
                <h1 class="gomodo_logo logo navbar-brand">
                    <a href="https://gomodo.id/" title="Gomodo">
                        <img src="../gomodo_files/logo.png" class="logo-gomodo" alt="Gomodo">
                        <span class="gomodo_text">GOMODO</span>
                    </a>
                </h1>
                <nav id="main-menu" role="navigation">
                    <ul class="menu">
                        <li class="menu-item-has-children text-white">
                            <a href="https://gomodo.id/">Beranda</a>
                        </li>
                        <li class="text-black">
                            <a href="https://gomodo.id/explore/transports/search">Transportasi</a>
                        </li>



                        <li class="text-white">
                            <a href="https://gomodo.id/explore/all-activities/search">Aktivitas</a>
                        </li>
                        <li class="text-white">
                            <a href="https://gomodo.id/explore/all-destination">Destinasi</a>
                        </li>
                        <li class="text-white">
                            <a href="https://gomodo.id/explore/help">Bantuan</a>
                        </li>
                        <li class="provider-login-nav text-white">
                            <a href="">Partner</a>
                        </li>





                        <li class="ribbon">
                            <a href="#"><img src="../gomodo_files/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                            <ul class="menu-mini">
                                <li class="">
                                    <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="../gomodo_files/uk.png" width="15px"> EN</a>
                                </li>
                                <li class="active">
                                    <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="../gomodo_files/idn.png" width="15px"> ID</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <form action="https://gomodo.id/change-language" id="landing-change-language" method="POST">
                    <input type="hidden" name="_token" value="GjyWA0BmbCjxe3bWiLCoMOhW1USmBMekPEO6YKv9">
                    <input type="hidden" name="lang">
                </form>
            </div>

            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="https://gomodo.id/">Beranda</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                    </li>
                    <li class="">
                        <a href="https://gomodo.id/explore/transports/search">Transportasi</a>
                    </li>



                    <li class="">
                        <a href="https://gomodo.id/explore/all-activities/search">Aktivitas</a>
                    </li>
                    <li class="">
                        <a href="https://gomodo.id/explore/all-destination">Destinasi</a>
                    </li>
                    <li class="">
                        <a href="https://gomodo.id/explore/help">Bantuan</a>
                    </li>
                    <li class="provider-login-nav">
                        <a href="">Partner</a>
                    </li>

                </ul>

                <ul class="mobile-topnav container">
                    <li class="ribbon language menu-color-skin">

                        <a href="#">Indonesia</a>
                        <ul class="menu mini" style="">
                            <li class=""><a href="#" id="indonesia" data-value="en" title="English">English</a></li>
                            <li class="active"><a href="#" id="indonesia" data-value="id" title="Indonesia">Indonesia</a></li>

                        </ul>
                    </li>
                </ul>
            </nav>
            <form action="https://gomodo.id/change-language" id="landing-change-language" method="POST">
                <input type="hidden" name="_token" value="GjyWA0BmbCjxe3bWiLCoMOhW1USmBMekPEO6YKv9">
                <input type="hidden" name="lang">
            </form>
        </div>
    </header>
    <form class="d-none" method="post" action="https://gomodo.id/change-language" id="change-language">
        <input type="hidden" name="_token" value="GjyWA0BmbCjxe3bWiLCoMOhW1USmBMekPEO6YKv9">
        <input type="hidden" name="lang" value="id">
    </form>

    <!-- section content start here -->
    <section id="content" class="tour">
        <div id="slideshow" class="slideshow-bg full-screen" style="height: 791px;">
            <div class="flexslider">
                <ul class="slides">
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
                        <div class="slidebg" style="background-image: url(https://gomodo.id/explore-assets/images/homepage/hero-1.jpeg);background-size: cover;"></div>
                    </li>
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                        <div class="slidebg" style="background-image: url(https://gomodo.id/explore-assets/images/homepage/hero-2.jpeg);background-size: cover;"></div>
                    </li>
                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                        <div class="slidebg" style="background-image: url(https://gomodo.id/explore-assets/images/homepage/hero-3.jpeg);background-size: cover;"></div>
                    </li>
                </ul>
            </div>
            <div class="container" id="homepage">
                <div class="table-wrapper full-width">
                    <div class="table-cell">
                        <div class="heading box">
                            <h1 class="title">Yuk, liburan!</h1>
                            <h3 class="sub-title">Pesan paket liburan anda langsung dari penyedia jasa dan wisata</h3>
                        </div>
                        <div class="row">
                            <div class="search-box col-sm-8 col-sm-offset-2">
                                <form action="https://gomodo.id/explore/all-destination/search">
                                    <div class="row">
                                        <div class="col-md-8 form-group">
                                            <input type="text" class="input-text full-width" name="q" placeholder="Cari aktivitas, atau atraksi" maxlength="70">
                                        </div>
                                        <!-- s -->
                                        <div class="col-md-4 row">
                                            <div class="col-sm-12 form-group">
                                                <button class="button btn-medium full-width uppercase sky-blue1"><i class="fa fa-search"></i> &nbsp;&nbsp;Cari
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="section global-map-area padding-bottom-0">
            <div class="container">
                <div class="row add-clearfix">
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="../gomodo_files/list.svg" alt="author">

                            <div class="description">
                                <h4>Penyedia Jasa dan Wisata Lebih banyak.</h4>
                                <p>Temukan penyedia jasa dan wisata yang lebih menarik dan terpercaya.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.3" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="../gomodo_files/money-bag.svg">

                            <div class="description">
                                <h4>Paket Harga Lebih Bervariasi.</h4>
                                <p>Temukan harga terbaik sesuai dengan budget anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.6" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="../gomodo_files/shopping-cart.svg" align="">

                            <div class="description">
                                <h4>Proses yang Mudah dan Aman.</h4>
                                <p>Semua proses transaksi dilindungi dengan sertifikat keamanan.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="icon-box style6 animated small-box" data-animation-type="slideInUp" data-animation-delay="0.9" style="display: inline-flex!important;">
                            <img class="image-header-layout" src="../gomodo_files/network.svg">

                            <div class="description">
                                <h4>Pesan Langsung dari Penyedia Jasa dan Wisata.</h4>
                                <p>Terhubung langsung dengan penyedia yang terverifikasi asosiasi jasa dan wisata.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section white-bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Destinasi Populer</h1>
                    <p>Temukan petualanganmu selanjutnya</p>
                </div>
                <div class="tour-packages row add-clearfix image-box">
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=78">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">Badakhshan</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=888">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">Ain Defla</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=111">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">Durres</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=105">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">Berat</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=1">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">Aceh</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <article class="box animated" data-animation-type="fadeInLeft">
                            <figure>
                                <a href="https://gomodo.id/explore/city/search?city=14">
                                    <img src="../gomodo_files/1440x893.png" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">DI Yogyakarta</h2>


                                    </figcaption>
                                </a>
                            </figure>
                        </article>
                    </div>
                </div>
                <div class="btn_explore_des">
                    <a href="https://gomodo.id/explore/all-destination" class="button full-width sky-blue1 btn_cus">
                        <i class="soap-icon-departure point_icon"></i>
                        Jelajahi Semua Destinasi
                    </a>
                </div>
            </div>
        </div>
        <div class="section popular-activities new_bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Aktivitas Populer</h1>
                    <p>Pengalaman favorit para wisatawan</p>
                </div>
                <div id="activities_slide" class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">

                    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides image-box" style="width: 1400%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;" data-thumb-alt="">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU52115610222369814?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/J2ui0mMfiq8xAoyVeoyAfCbWevPXEwAf1uMX7ycH.jpg" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Cobain/\;'custom</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>ASHKASHAM</li>


                                                <li>                                                         20 Days
                                                </li>


                                                <li>
                                                    27 Nov 2019
                                                    - 01 May 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 49,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li data-thumb-alt="" style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215631846255713?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">cobain simple</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>EL ABADIA</li>


                                                <li>                                                         5 Hours
                                                </li>


                                                <li>
                                                    16 Jul 2019
                                                    - 30 Sep 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215659396347343?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">custom is my life</h4>
                                            <hr>
                                            <div class="card_short_desc">nanjak gunung andong via basecamp sawit
                                            </div>
                                            <ul class="features check">
                                                <li>BASHKIA DURRES</li>


                                                <li>                                                         5 Hours
                                                </li>


                                                <li>
                                                    17 Aug 2019
                                                    - 31 Mar 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 55,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU5213157355529354?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Anu</h4>
                                            <hr>
                                            <div class="card_short_desc">hjfjjfhjh`
                                            </div>
                                            <ul class="features check">
                                                <li>BANAJ</li>


                                                <li>                                                         10 Hours
                                                </li>


                                                <li>
                                                    27 Nov 2019
                                                    - 30 Apr 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 10,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU5252415458206402383?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Jalan-Jalan Yogyakarta</h4>
                                            <hr>
                                            <div class="card_short_desc">brief desription
                                            </div>
                                            <ul class="features check">
                                                <li>KAB. ACEH BARAT</li>


                                                <li>                                                         3 Hours
                                                </li>


                                                <li>
                                                    26 Dec 2018
                                                    - 26 Dec 2023
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521315645444580406?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">custom cobain</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>FAYZABAD</li>


                                                <li>                                                         4 Hours
                                                </li>


                                                <li>
                                                    01 Aug 2019
                                                    - 30 May 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521115514057110889?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">cobain display</h4>
                                            <hr>
                                            <div class="card_short_desc">yoyoyoyoyoy
                                            </div>
                                            <ul class="features check">
                                                <li>KOTA YOGYAKARTA</li>


                                                <li>                                                         6 Hours
                                                </li>


                                                <li>
                                                    22 Mar 2019
                                                    - 24 Sep 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 4,020,000,000,0...</div>
                                        </div>
                                    </article>
                                </a>
                            </li>

                        </ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>
            </div>
        </div>

        <div class="global-map-area section parallax" data-stellar-background-ratio="0.5" style="padding-bottom: 40px; background-position: 50% 1065.98px;">
            <div class="container description">
                <div class="text-center">
                    <h2>Temukan ratusan&nbsp;UMKM jasa dan wisata dengan&nbsp;<b>Gomodo</b></h2>
                </div>

            </div>
        </div>
        <div class="section new_bg">
            <div class="container">
                <div class="text-center description block">
                    <h1>Rekomendasi Gomodo</h1>
                    <p>Pilihan aktivitas terbaik dari kami</p>
                </div>
                <div id="activities_slide" class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">

                    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides image-box" style="width: 1600%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;" data-thumb-alt="">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215631846255713?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">cobain simple</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>EL ABADIA</li>


                                                <li>                                                         5 Hours
                                                </li>


                                                <li>
                                                    16 Jul 2019
                                                    - 30 Sep 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li data-thumb-alt="" style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215637986700762?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">custom v1 di coba maka di coba...</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>ASHKASHAM</li>


                                                <li>                                                         5 Hours
                                                </li>


                                                <li>
                                                    23 Jul 2019
                                                    - 31 Jan 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU525415468381391763?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Litle Japan</h4>
                                            <hr>
                                            <div class="card_short_desc">AAAAAA go to japan is fucking awsome
                                            </div>
                                            <ul class="features check">
                                                <li>KAB. ACEH BARAT</li>


                                                <li>                                                         6 Days
                                                </li>


                                                <li>
                                                    07 Dec 2018
                                                    - 07 Aug 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 10,000,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521315645444580406?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">custom cobain</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>FAYZABAD</li>


                                                <li>                                                         4 Hours
                                                </li>


                                                <li>
                                                    01 Aug 2019
                                                    - 30 May 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU524115637825339106?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Cicilan custom</h4>
                                            <hr>
                                            <div class="card_short_desc">cobain
                                            </div>
                                            <ul class="features check">
                                                <li>KAB. SLEMAN</li>


                                                <li>                                                         5 Hours
                                                </li>


                                                <li>
                                                    23 Jul 2019
                                                    - 04 May 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU5252415458206402383?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Jalan-Jalan Yogyakarta</h4>
                                            <hr>
                                            <div class="card_short_desc">brief desription
                                            </div>
                                            <ul class="features check">
                                                <li>KAB. ACEH BARAT</li>


                                                <li>                                                         3 Hours
                                                </li>


                                                <li>
                                                    26 Dec 2018
                                                    - 26 Dec 2023
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 50,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521115514057110889?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">cobain display</h4>
                                            <hr>
                                            <div class="card_short_desc">yoyoyoyoyoy
                                            </div>
                                            <ul class="features check">
                                                <li>KOTA YOGYAKARTA</li>


                                                <li>                                                         6 Hours
                                                </li>


                                                <li>
                                                    22 Mar 2019
                                                    - 24 Sep 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 4,020,000,000,0...</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                            <li style="width: 270px; margin-right: 30px; float: left; display: block;">
                                <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU5213157355529354?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
                                    <article class="box">
                                        <figure>
                                            <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
                                        </figure>
                                        <div class="details">
                                            <h4 class="box-title">Anu</h4>
                                            <hr>
                                            <div class="card_short_desc">hjfjjfhjh`
                                            </div>
                                            <ul class="features check">
                                                <li>BANAJ</li>


                                                <li>                                                         10 Hours
                                                </li>


                                                <li>
                                                    27 Nov 2019
                                                    - 30 Apr 2020
                                                </li>

                                            </ul>
                                            <hr>
                                            <span>Mulai Dari: </span>

                                            <div class="price"> IDR 10,000</div>
                                        </div>
                                    </article>
                                </a>
                            </li>
                        </ul></div><ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>
            </div>
        </div>
        <div class="global-map-area empty_bg" style="display: none;">
            <div class="container">
                <div class="add-clearfix">







                    <div class="icon-box style6 animated small-box fadeIn" style="animation-duration: 1s; visibility: visible;">

                        <div class="description logo_asoc_container row">
                            <div class="col-auto">
                                <img class="image-association aeli" src="../gomodo_files/aeli-image.png" alt="association image">
                            </div>
                            <div class="col-auto">
                                <img class="image-association" src="../gomodo_files/asita-image.png" alt="association image">
                            </div>
                            <div class="col-auto">
                                <img class="image-association" src="../gomodo_files/aspi-image.png" alt="association image">
                            </div>
                            <div class="col-auto">
                                <img class="image-association" src="../gomodo_files/iatta-image.png" alt="association image">
                            </div>
                            <div class="col-auto">
                                <img class="image-association ipi" src="../gomodo_files/ipi-image.png" alt="association image">
                            </div>
                        </div>
                    </div>































































































                </div>
            </div>
        </div>
    </section>



    <!-- footer start here -->
    <footer id="footer">
        <div class="footer-wrapper text-md-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <h2>Temukan</h2>
                        <ul class="discover triangle hover row text-left">
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/pres-releases">Siaran Pers</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/about_us">Tentang Kami</a></li>
                            <li class="col-xs-6"><a href="https://gomodo.id/partner-with-us">Bermitra dengan Kami</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/blog">Blog</a></li>
                            <li class="col-xs-6"><a href="https://gomodo.id/agent/login">Login Mitra</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/careers">Karir</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/help">Bantuan</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/policy">Kebijakan</a></li>
                            <li class="col-xs-6 "><a href="https://gomodo.id/explore/term-condition">Syarat dan Ketentuan</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2>Berlangganan Email</h2>
                        <p>Segera berlangganan email untuk mendapatkan penawaran menarik &amp; informasi terbaru dari Gomodo!</p>
                        <br>
                        <div class="form-newsletter">
                            <form method="GET" action="https://gomodo.id" accept-charset="UTF-8" id="homepage-newsletter">
                                <div class="icon-check text-left">
                                    <input type="email" class="input-text width-email-footer" name="email" placeholder="Email Anda" maxlength="50">
                                    <button class="subscribe loading-button width-button-email-footer">Berlangganan</button>
                                </div>
                            </form>
                        </div>

                        <br>
                        <span>Kami menghargai privasi Anda.</span>
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <h2>Gomodo</h2>
                        <p>Discover. Book. Empower.</p>
                        <i class="fa fa-phone-square"> 0274 - 4288 - 422</i>
                        <br>
                        <br>
                        <ul class="social-icons clearfix">
                            <li class="twitter"><a title="" href="https://twitter.com/Gomodo_official" data-toggle="tooltip" target="_blank" data-original-title="twitter"><i class="soap-icon-twitter"></i></a></li>
                            <li class="googleplus"><a title="" href="https://www.instagram.com/gomodo.official/" data-toggle="tooltip" target="_blank" data-original-title="instagram"><i class="soap-icon-instagram"></i></a></li>
                            <li class="facebook"><a title="" href="https://www.facebook.com/gomodo.official/" data-toggle="tooltip" target="_blank" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                            <li class="youtube"><a title="" href="https://www.youtube.com/channel/UC63SauNWdORLCBh5J9U5T9g" data-toggle="tooltip" target="_blank" data-original-title="youtube"><i class="soap-icon-youtube"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <h2 class="margin-bottom-0">Metode Pembayaran</h2>
                        <div class="bank-icons clearfix text-center">
                            <img class="image-bank" src="../gomodo_files/bri.png" alt="BRI Bank">
                            <img class="image-bank" src="../gomodo_files/bni.png" alt="BNI Bank">
                            <img class="image-bank" src="../gomodo_files/mandiri-bank.png" alt="Mandiri Bank">
                            <img class="image-bank" src="../gomodo_files/master-card.png" alt="Master Card">
                            <img class="image-bank" src="../gomodo_files/visa.png" alt="Visa">
                            <img class="image-bank" src="../gomodo_files/verifiedbyvisa.png" alt="Verified by Visa">
                            <img class="image-bank" src="../gomodo_files/mastercard-securecode.png" alt="Mastercard Securecode">
                            <img class="image-bank" src="../gomodo_files/ssl.png" alt="SSL">
                            <a href="https://www.certipedia.com/quality_marks/0000065832" target="_blank">
                                <img class="image-bank" src="../gomodo_files/pci-dss-watermark.png" alt="PCI" style="width:8rem;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom gray-area">
            <div class="container">
                <div class="logo pull-left">
                    <a href="https://gomodo.id/index.html" title="Gomodo">
                        <img src="../gomodo_files/logo_002.png" alt="Travelo HTML5 Template">
                    </a>
                </div>
                <div class="pull-right back-top">
                    <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p>© 2018 Powered by Gomodo Technologies</p>
                </div>
            </div>
        </div>
    </footer>
</div>
<script>window.gomodo = window.gomodo || {};</script><!-- Javascript -->
<script type="text/javascript" src="../gomodo_files/jquery-1.js"></script>
<script type="text/javascript" src="../gomodo_files/jquery_003.js"></script>
<script type="text/javascript" src="../gomodo_files/modernizr.js"></script>
<script type="text/javascript" src="../gomodo_files/jquery-migrate-1.js"></script>
<script type="text/javascript" src="../gomodo_files/jquery_004.js"></script>
<script type="text/javascript" src="../gomodo_files/jquery-ui.js"></script>
<script type="text/javascript" src="../gomodo_files/jquery_005.js"></script>
<script src="../gomodo_files/toastr.js"></script>
<!-- Twitter Bootstrap -->
<script type="text/javascript" src="../gomodo_files/bootstrap.js"></script>


<!-- parallax -->
<script type="text/javascript" src="../gomodo_files/jquery.js"></script>

<!-- waypoint -->
<script type="text/javascript" src="../gomodo_files/waypoints.js"></script>

<!-- load page Javascript -->
<script type="text/javascript" src="../gomodo_files/theme-scripts.js"></script>

<script>
    tjq(document).ready(function () {
        if (localStorage.getItem('sidebar') == 1) {
            tjq('#sidebar-recent-post-blog').toggleClass('active');
            tjq('#sidebar-popular-post-blog').removeClass('active');
            tjq('#recent-posts').toggleClass('in active');
            tjq('#popular-posts').removeClass('in active');
        }
    });
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi Kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    tjq('#main-menu ul li').not('.provider-login-nav, li.language, .ribbon').on('click', function (e) {
        tjq('.loading').addClass('show');
    });
    // tjq('.ribbon').click(function(e){
    //     e.preventDefault();
    //     tjq('.loading').removeClass('show');
    // });
    tjq(document).on('submit', 'form#homepage-newsletter', function (e) {
        e.preventDefault();
        loadingStart('form#homepage-newsletter');
        let data = tjq(this).serialize();
        tjq('form#homepage-newsletter').find('span.error').remove();
        tjq('form#homepage-newsletter').find('span.success').remove()
        tjq.ajax({
            url: "https://gomodo.id/explore/subscribe",
            data: data,
            success: function (res) {
                loadingEnd('form#homepage-newsletter', 'Berlangganan');
                tjq('form#homepage-newsletter').append(`<span class="success">${res.message}</span>`);
            },
            error: function (err) {
                let e;
                if (err.status === 422) {
                    e = err.responseJSON.errors.email[0];

                } else {
                    e = err.responseJSON.message
                }
                loadingEnd('form#homepage-newsletter', 'Berlangganan');
                tjq('form#homepage-newsletter').append(`<span class="error">${e}</span>`)
            }
        })
    });

    tjq('.recent-post-blog').click(function () {
        localStorage.setItem('sidebar', 1);
    });

    tjq('.popular-post-blog').click(function () {
        localStorage.removeItem('sidebar');
    });

    function loadingStart(place) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.addClass('loader');
        loaderButton.attr('disabled', true);
        loaderButton.html('<span class="lds-dual-ring"></span>Loading');
    }

    function loadingEnd(place, initialText) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.removeClass('loader');
        loaderButton.attr('disabled', false);
        loaderButton.html(initialText);
    }

    tjq(document).on('click', '#english, #indonesia', function () {
        let form = tjq('#landing-change-language');
        form.find('input[name=lang]').val(tjq(this).data('value'));
        form.submit();
    });

    // Provider Login Modal
    tjq(document).on('click', '.provider-login-nav', function (e) {
        e.preventDefault();
        tjq('#provider-modal').css('display', 'block');
    })
    tjq(document).on('click', '.close', function () {
        tjq('#provider-modal').css('display', 'none');
    });
    tjq(document).on('click', 'li.language', function (e) {
        e.preventDefault();
    });
    tjq(document).ready(function () {
        tjq('.provider-login-nav').removeClass('active');
    });
    tjq(document).on('click', function (event) {
        if (!tjq(event.target).closest(".modal-content, .provider-login-nav").length) {
            tjq('#provider-modal').css('display', 'none');
        }
    });

    // Prevent Char in Input Type Number
    tjq(document).on('change keydown', 'input[type="number"], input[type="tel"], .number', function (e) {
        if (!digitOnly(e)) {
            e.preventDefault();
        }
    });

    function digitOnly(e) {
        let allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight'];
        if (!allowed.includes(e.key)) {
            return false
        }
        return true
    }

    // Disable Paste in input type number
    tjq(document).on('paste', 'input[type="number"], input[type="tel"], .number', function (e) {
        e.preventDefault();
    });
    // Disable Scroll in input type number
    tjq(document).on('wheel', 'input[type="number"], input[type="tel"], .number', function (e) {
        tjq(this).blur();
    });
</script>
<!-- Flex Slider -->
<script type="text/javascript" src="../gomodo_files/jquery_002.js"></script>
<script type="text/javascript" src="../gomodo_files/shCore.js"></script>
<script type="text/javascript" src="../gomodo_files/shBrushJScript.js"></script>


<script type="text/javascript">
    tjq("#slideshow .flexslider").flexslider({
        animation: "fade",
        controlNav: false,
        animationLoop: true,
        directionNav: false,
        slideshow: true,
        slideshowSpeed: 5000,
    });

    tjq('.main-header').toggleClass('main-header-black');

    // tjq(".flexslider").flexslider({
    //     animation: "fade",
    //     controlNav: true,
    //     animationLoop: true,
    //     directionNav: true,
    //     slideshow: true,
    //     slideshowSpeed: 5000,
    //     minItems : 1,
    //     maxItems: 1,
    // });

    // (function() {
    //
    //     // store the slider in a local variable
    //     let $window = tjq(window),
    //         flexslider = { vars:{} };
    //
    //     // tiny helper function to add breakpoints
    //     function getGridSize() {
    //         return (window.innerWidth < 600) ? 1 :
    //             (window.innerWidth < 900) ? 3 : 4;
    //     }
    //     console.log(getGridSize());
    //
    //     tjq(function() {
    //         SyntaxHighlighter.all();
    //     });
    //
    //     $window.load(function() {
    //         tjq('.flexslider').flexslider({
    //             animation: "slide",
    //             animationLoop: false,
    //             itemWidth: 210,
    //             itemMargin: 5,
    //             minItems: getGridSize(), // use function to pull in initial value
    //             maxItems: getGridSize() // use function to pull in initial value
    //         });
    //     });
    //
    //     // check grid size on resize event
    //     $window.resize(function() {
    //         let gridSize = getGridSize();
    //         flexslider.vars.minItems = gridSize;
    //         flexslider.vars.maxItems = gridSize;
    //     });
    // }());
</script>

<div class="chaser hidden-mobile" style="visibility: visible; display: none;"><div class="container"><h1 class="logo navbar-brand"><a title="Gomodo" href="https://gomodo.id/"><img alt="" src="../gomodo_files/logo.png" style="width: 50px!important;"><span class="gomodo_text" style="color: #b5b5b4">GOMODO</span></a></h1><ul class="menu" style="">
            <li class="menu-item-has-children text-white">
                <a href="https://gomodo.id/">Beranda</a>
            </li>
            <li class="text-black">
                <a href="https://gomodo.id/explore/transports/search">Transportasi</a>
            </li>



            <li class="text-white">
                <a href="https://gomodo.id/explore/all-activities/search">Aktivitas</a>
            </li>
            <li class="text-white">
                <a href="https://gomodo.id/explore/all-destination">Destinasi</a>
            </li>
            <li class="text-white">
                <a href="https://gomodo.id/explore/help">Bantuan</a>
            </li>
            <li class="provider-login-nav text-white">
                <a href="">Partner</a>
            </li>





            <li class="ribbon">
                <a href="#"><img src="../gomodo_files/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                <ul class="menu-mini">
                    <li class="">
                        <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="../gomodo_files/uk.png" width="15px"> EN</a>
                    </li>
                    <li class="active">
                        <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="../gomodo_files/idn.png" width="15px"> ID</a>
                    </li>
                </ul>
            </li>
        </ul></div></div></body></html>