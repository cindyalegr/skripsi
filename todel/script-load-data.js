<script type="272a288d969f052c0f2e995a-text/javascript">
    let l = 'Menunggu..';
    let page = 1;
    let price = null;
    let total = 0;

    function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

    tjq("#price-range").slider({
    range: true,
    min: 0,
    max: 100000000,
    step: 100000,
    values: [0, 0],
    slide: function (event, ui) {
    if((ui.values[1] - ui.values[0]) <= 1000000){
    return false;
}
    tjq(".min-price-label").html("IDR " + formatNumber(ui.values[0]));
    tjq(".max-price-label").html("IDR " + formatNumber(ui.values[1]));
    price = ui.values[0] + '-' + ui.values[1];
}
});
    tjq('#price-range').draggable();
    tjq(".min-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 0)));
    tjq(".max-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 1)));
    price = tjq("#price-range").slider("values", 0) + '-' + tjq("#price-range").slider("values", 1);

    function render() {
    tjq(document).find('#content button').prop('disabled', true).html(l);
    if (page === 1) {
    loadingStart('#main');
    tjq('.listing-style1').empty();
}
    let keyword = tjq('input[name=q]').val();
    let city = tjq('input[name=city]').val();
    let sort = tjq('input[name=sortBy]').val();
    let transport = tjq('input[name=transport]').val();
    let data = {
    q: keyword,
    city: city,
    sort: sort,
    price: price,
    page: page,
    transport: transport,
    guides: getGuides(),
    categories: getCategories()
};

    if (page !== undefined || page !== '' || page !== null) {
    tjq.ajax({
    url: "https://gomodo.id/explore/render",
    type: "POST",
    data: data,
    dataType: 'html',
    success: function (data) {
    loadingEnd('#main','Memuat lebih banyak destinasi');
    tjq('#main .lds-dual-ring').show();
    if (page === 1) {
    tjq('.listing-style1').html(data)
} else {
    tjq('.listing-style1').append(data);
}
    if (tjq(document).find('.pagination-product').length > 0) {
    tjq('#total-product').text(formatNumber(tjq(document).find('.pagination-product').data('total')));
    if (tjq(document).find('.pagination-product').attr('data-nextpage') !== '') {
    page = tjq(document).find('.pagination-product').attr('data-nextpage');
} else {
    page = null
}

} else {
    page = null

}
    tjq('#main .lds-dual-ring').hide();
    tjq(document).find('.pagination-product').remove();
    if (page === null) {
    tjq('#load-more').remove();
}
    tjq(document).find('#content button').each(function () {
    tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
},
    error: function (e) {
    console.log(e)
    tjq(document).find('#content button').each(function () {
    tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
}
})
} else {
    tjq('#load-more').remove();
    tjq(document).find('#content button').each(function () {
    tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
}
}

    render();

    tjq(document).on('click', '#load-more', function () {
    if (!tjq(this).prop('disabled')) {
    loadingStart('#main');
    render(page)
}
});
    tjq(document).on('click', '#apply-filter', function () {
    if (!tjq(this).prop('disabled')) {
    page = 1;
    render(page)
}
});

    function getGuides() {
    let guides = [];
    tjq(document).find('#guides li.active').each(function (i, data) {
    guides.push(tjq(data).data('id'));
});
    return guides;
}

    function getCategories() {
    let categories = [];
    tjq(document).find('#product-categories li.active').each(function (i, data) {
    categories.push(tjq(data).data('id'));
});
    return categories;
}
</script>