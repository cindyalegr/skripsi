
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="w12ULBNDbAVwB28Yb2oLXIaDarlWYjISNpGnc6Cm">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta property="og:image" content="https://wisataukraina.gomodo.id/uploads/company_logo/1556886766-Logo_Wisata_Ukraina2.png"/>
    <meta property="og:image:alt" content="Wisata Ukraina"/>
    <title>wisata ukraina</title>
    <meta name="title" content="wisata ukraina">
    <meta property="og:title" content="wisata ukraina"/>


    <meta name="description" content="wisata ukraina berpengalaman menangani wisatawan dari Indonesia">
    <meta property="og:description"
          content="wisata ukraina berpengalaman menangani wisatawan dari Indonesia"/>
    <meta name="author" content="Gomodo Technologies">
    <meta property="fb:app_id" content="887163081476612"/>
    <meta name="keywords" content="wisata-ukraina,tour-ukraina,tour-murah-ukraina,visa-ukraina">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/landing/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="https://wisataukraina.gomodo.id/css/material.css" rel="stylesheet">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/app.css">
    <link rel="shortcut icon"
          href="https://wisataukraina.gomodo.id/uploads/company_logo/1556886766-Logo_Wisata_Ukraina2.png">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/badge-company.css">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/badge-discount.css">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/customer-inline-fix.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://wisataukraina.gomodo.id/dest-customer/lib/css/touchspin/jquery.bootstrap-touchspin.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153402040-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-153402040-1');
    </script>

    <meta name="google-site-verification" content="5VneIqbUKnCRAWOuwZrF9Gf4O2osJ55BqyyoEjGf4_U" />
    <style>
        .address {
            color: #fff;
            padding-bottom: 0.8rem;
            text-align: justify;
        }

        .address p {
            margin-bottom: 0px;
        }

        footer .powered {
            height: auto;
        }

        footer .phone-company span {
            display: inline-flex;
        }

        footer .phone-company {
            margin-top: 1rem;
        }

        footer .phone-company img {
            width: 20px;
        }

        footer p.about, footer .address #address, footer .social {
            font-weight: bold;
            margin-bottom: 0.6rem;
            margin-top: 0;
        }

        footer .address #address {
            margin-bottom: 0.95rem;
        }

        footer .pt-5 {
            padding-top: 2.5rem !important;
        }

        nav.navbar + section {
            margin-top: 49px !important;
        }

        #btn-navbar-toggle {
            cursor: pointer;
            background: none;
            border: none;
            display: none;
        }

        .relative img {
            width: 1.3rem;
        }

        .fa.fa-bars {
            color: #2699FB;
            font-size: 1.5rem;
        }

        .owl-nav .owl-next.disabled, .owl-nav .owl-prev.disabled {
            display: none
        }

        @media  only screen and (max-width: 480px) {
            #btn-navbar-toggle {
                display: block;
            }

            .collapse.navbar-collapse {
                background: #f1f8fd;
                margin-left: -1rem;
                margin-right: -1rem;
                border-top: 1px solid #dcdbdb;
            }

            footer .address #address {
                margin-top: 35px;
            }
        }
    </style>

</head>
<body>
<div class="loading">
    <div class="loading-content">
        <div class="spin">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
        <div class="loading-text">
            Memuat..
        </div>
    </div>
</div>
<nav class="navbar navbar-dark navbar-expand-md bg-style-1 container-fluid navbar-fixed-top" id="topNavbar">
    <div class="container">
        <a href="https://wisataukraina.gomodo.id" class="navbar-brand">
            <img src="https://wisataukraina.gomodo.id/uploads/company_logo/1556886766-Logo_Wisata_Ukraina2.png" alt="">
        </a>
        <div class="profile-info">
            <h3 id="company-name">Wisata Ukraina</h3>
        </div>
        <span class="relative">
                            </span>
        <button id="btn-navbar-toggle" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item text-center">
                    <a href="https://wisataukraina.gomodo.id/retrieve"
                       class="btn btn-primary retrieve">Cek Status Pemesanan</a>
                </li>
                <li class="nav-item text-center" id="landing-language" role="presentation">
                    <div class="current-language">
                        <div class="description-current-language">
                            Indonesia
                        </div>
                        <div class="flag-current-language">
                            <img class="img-circle" src="https://wisataukraina.gomodo.id/landing/img/idn.png" alt="">
                        </div>
                        <div class="caret-language">
                            <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="language-option" style="display: none;">
                        <ul>
                            <li data-value="en" class="pick-lang">
                                <div class="box-language">
                                    <div class="description-language">
                                        English
                                    </div>
                                    <div class="flag-language">
                                        <img src="https://wisataukraina.gomodo.id/landing/img/uk.png" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <form action="https://wisataukraina.gomodo.id/change-language" id="landing-change-language" method="POST">
                        <input type="hidden" name="_token" value="w12ULBNDbAVwB28Yb2oLXIaDarlWYjISNpGnc6Cm">
                        <input type="hidden" name="lang">
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content">
    <!-- Breadcrumb -->
    <div class="container pt-5">
        <ul class="breadcrumb">
            <li><a href="https://gomodo.id/explore/all-activities/search">Directory</a></li>
            <li><a href="https://wisataukraina.gomodo.id">Beranda</a></li>
            <li><a>HILLSONG CHURCH 4D KYIV UKRAINE</a></li>
        </ul>
    </div>
    <!-- Content -->
    <div id="product-detail" class="container pb-5">
        <div class="row">
            <div class="col-md-8 left-side-detail">
                <div class="card">
                    <div class="card-header">
                        <div class="owl-carousel">
                            <a data-fancybox="gallery" href="https://wisataukraina.gomodo.id/uploads/products/hfdYtMnqbUcMhCQYg2dK8rR4RbpO1x2Z1wZYVvHu.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://wisataukraina.gomodo.id/uploads/products/hfdYtMnqbUcMhCQYg2dK8rR4RbpO1x2Z1wZYVvHu.jpg"
                                                                                                                                                                                    alt="Card image cap"></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="box-product-tags py-3">
                            <span class="badge badge-warning product-tags">Wisata Keliling Kota</span>

                        </div>
                        <h3>
                            HILLSONG CHURCH 4D KYIV UKRAINE
                        </h3>
                        <table class="table-product">
                            <tr>
                                <td>
                                    <img src="https://wisataukraina.gomodo.id/img/pin.png" alt="">
                                </td>
                                <td colspan="3">
                                    Kyiv Oblast


                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <img src="https://wisataukraina.gomodo.id/img/wall-clock.png" alt="">
                                </td>
                                <td colspan="3">
                                    4
                                    Hari
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <img src="https://wisataukraina.gomodo.id/img/calendar.png" alt="">
                                </td>
                                <td>
                                    14 Aug 2020
                                </td>
                                <td>
                                    <span class="badge badge-warning bg-light-blue color-primary-blue">sampai</span>
                                </td>
                                <td>
                                    31 Oct 2020
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="https://wisataukraina.gomodo.id/img/160-chat.png" alt="">
                                </td>
                                <td colspan="3">
                                    Dipandu dalam English
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <h3>Tentang "HILLSONG CHURCH 4D KYIV UKRAINE"</h3>
                        <p class="card-text"><p style="text-align:justify;"><strong>PERIODE : <br></strong>08, 29 Agustus 2020<br>19 September 2020<br>10, 31 Oktober 2020</p>
                        <p><strong>BIAYA TERMASUK :</strong><br>1.Akomodasi di hotel Ibis Kiev *3 atau setaraf dengan makan pagi<br>2.Transfer ke hotel hari pertama dan transfer ke bandara hari ke empat hanya diantar oleh supir<br>3.Acara tour dan transportasi sesuai yang tercantum dalam acara perjalanan.<br>4.Tiket masuk tempat wisata sesuai dengan jadwal perjalanan<br>5.Lokal Guide berbahasa Inggris<br>6. PPN 1%</p>
                        <p><strong>BIAYA TIDAK TERMASUK :</strong><br>1.Tiket pesawat udara internasional CGK-IST-KBP-IST-CGK (rekomendasi Turkish Airlines)<br>2.Visa Turis Ukraina proses normal Rp. 1,500,000 per orang<br>3.Asuransi perjalanan dengan perlindungan EUR 30,000 dengan premi Rp. 190.000 per orang dan asuransi dengan perlindungan Covid19 sebesar Rp.100.000 per orang<br>4.Makan Siang (MS) dan Makan Malam (MM)</p>
                        <p>5.Tipping untuk lokal guide dan pengemudi USD 5 per hari per orang<br>6.Tour Leader<br>7.Biaya dokumen perjalanan seperti : paspor, entry permit, dll.<br>8.Pengeluaran pribadi seperti : telepon, room service, laundry, mini bar, tambahan makanan dan minuman serta pengeluaran lainnya.<br>9.Tour tambahan (Optional Tour) yang mungkin diadakan selama perjalanan.<br>10.Excess baggage (biaya kelebihan barang bawaan di atas 20 kg), Biaya bea masuk yang dikenakan oleh duane di Jakarta maupun di negara yang dikunjungi.<br>11.Biaya Single Supplement.<br>12.Tips untuk Pelayan restoran, dan Porter (jika ada) di Hotel dan Bandara.<br>13.Segala bentuk pengeluaran yang disebabkan oleh Force Majeur (Kehilangan, Pemogokan, Kerusuhan, Keterlambatan sarana angkutan, Bencana alam, dll)</p></p>
                        <hr>
                        <h3>Catatan Penting</h3>
                        <p class="card-text"><p><strong>TERM &amp; CONDITION</strong><br>1.Tour dilaksanakan minimal 4 orang peserta<br>2.Uang Muka Pendaftaran tidak dapat dikembalikan (down payment non-refundable) per peserta IDR 3.500.000.<br>3.Pelunasan biaya tour 30 hari kalender sebelum tanggal keberangkatan<br>4.Airport Tax Internasional, Fuel Surcharge, biaya visa dan asuransi perjalanan pribadi dapat berubah sewaktu-waktu, dan akan disesuaikan kepada peserta sebelum keberangkatan.<br>5.Dalam hal aplikasi visa (jika ada), Peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan dari pihak Kedutaan, dan biaya visa tetap harus dilunasi walaupun visa tidak disetujui oleh pihak Kedutaan.<br>6.Group akan diberangkatkan apabila mencapai jumlah minimum 4 peserta sesuai yang tercantum di bagian harga tersebut di atas.<br>7.Acara perjalanan dapat berubah / diputar berdasarkan kondisi tiap-tiap penerbangan dan hotel di masing-masing kota / negara. Apabila dalam periode tour di kota-kota yang dikunjungi sedang berlangsung pameran / konferensi, atau hotel yang ditawarkan sedang penuh, maka akan diganti dengan hotel-hotel lain yang setaraf di kota terdekat.<br>8.Apabila terjadi Force Majeur (kondisi di luar kendali seperti : kehilangan, kerusakan, gangguan, keterlambatan sarana angkutan/transportasi, bencana alam dll) yang dapat memengaruhi acara tour akan dirubah dan bersifat non-refundable (tidak dapat dikembalikan). Dan Biaya Tour tidak termasuk segala pengeluaran tambahan yang disebabkan oleh Force Majeur.<br>9.Deviasi akan dikenakan biaya sesuai dengan kondisi Airlines yang bersangkutan.<br>10.Pengunduran diri / pindah tanggal / pindah acara tour sebelum tanggal keberangkatan akan dikenakan biaya pembatalan, sbb :<br>a.setelah pendaftaran: Uang Muka Pendaftaran (non-refundable)<br>b.30 – 15 hari kalender sebelum tanggal keberangkatan : 50% dari biaya tour<br>c.14 hari kalender sebelum tanggal keberangkatan : 100% dari biaya tour<br>11.Dengan membayar uang muka pendaftaran Biaya Tour, Anda dianggap telah memahami dan menerima syarat &amp; kondisi di atas.</p></p>
                        <hr>
                        <div class="product-map embed-responsive embed-responsive-21by9">
                            <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDR7DFQIBGumoziD6B6a0n2EZgrKhQOWS4&q=50.402411394193074,30.532690550000098"
                                    width="600" height="450" frameborder="0" style="border:0"
                                    class="embed-responsive-item"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="block__item pt-3">
                            <div class="block__inner">
                                <h3>Rencana Perjalanan</h3>
                                <div class="list">
                                    <div class="list__item">
                                        <div class="list__time">Hari ke-1</div>
                                        <div class="list__border"></div>
                                        <div class="list__desc">
                                            <p style="text-align: justify;"><strong>D1 KYIV ARRIVAL (-/- /-)</strong> <br />Setibanya di <strong>Kyiv Boryspol International Airport (KBP)</strong>, Anda akan langsung diantar menuju hotel untuk beristirahat. Acara bebas. Bermalam di Kyiv.</p>
                                            <div class="border"></div>
                                        </div>
                                    </div>
                                    <div class="list__item">
                                        <div class="list__time">Hari ke-2</div>
                                        <div class="list__border"></div>
                                        <div class="list__desc">
                                            <p style="text-align: justify;"><strong>D2 KYIV (MP/-/-)</strong> <br />Setelah santap pagi, mempersiapkan diri untuk beribadah dan selanjutnya Anda akan mengunjungi <strong>Hillsong Church Kyiv</strong> sebuah Mega Church Kristen Karismatik yang berasal dari kota Sydney, New South Wales, Australia. Gereja ini didirikan pada tahun 1983 dengan nama Hills Christian Life Centre yang kemudian menyebar ke berbagai negara dan pada tahun 2007 berdirilah Hillsong Church di kota Kyiv. Setelah ibadah, Anda kembali ke hotel. Bermalam di Kyiv.</p>
                                            <div class="border"></div>
                                        </div>
                                    </div>
                                    <div class="list__item">
                                        <div class="list__time">Hari ke-3</div>
                                        <div class="list__border"></div>
                                        <div class="list__desc">
                                            <p style="text-align: justify;"><strong>D3 KYIV (MP/-/-)</strong> <br />Setelah santap pagi, Anda akan diajak mengunjungi <strong>Poshtova Square</strong> salah satu alun-alun bersejarah di kota Kyiv yang terdapat galeri kecil sebagai kantor post, kemudian Anda akan diajak menaiki <strong>Funicular Tram</strong>, yaitu sebuah transportasi dari jaman Uni Soviet yang digunakan untuk menghubungkan kota Podil atas dan Podil bawah yang berjarak hanya 238 meter saja. <strong>St. Michael Golden Domed Monastery</strong> sebuah biara berkubah emas dengan eksterior bangunan dengan gaya Ukrainian Baroque, sedangkan interiornya dengan gaya asli Byzantine, <strong>Independence Square</strong> merupakan alun-alun utama kota Kyiv yang merupakan pusat kegiatan dari warga Kyiv ataupun wisatawan asing berkumpul. Berbelanja di <strong>Toko Suvenir</strong> yang menjual produk buatan pengrajin seluruh Ukraina yang di kumpulkan dan dijual di toko tersebut. Berjalan &ndash; jalan di <strong>Kreshchatyk Street</strong>, yaitu jalan utama teramai di kota Kyiv yang banyak dikunjungi banyak turis, berbelanja di Department store terkenal di jalan Khreschatyk yang bernama <strong>TSUM Mall</strong>, mengunjungi gedung <strong>Universitas Nasional Taras Shevchenko</strong> yang berwarna merah yang menggambarkan aspirasi suara mahasiswa, kemudian kembali ke hotel. Bermalam di Kyiv.</p>
                                            <div class="border"></div>
                                        </div>
                                    </div>
                                    <div class="list__item">
                                        <div class="list__time">Hari ke-4</div>
                                        <div class="list__border"></div>
                                        <div class="list__desc">
                                            <p style="text-align: justify;"><strong>D4 KYIV DEPARTURE (MP/-/-)</strong> <br />Setelah santap pagi, acara bebas, check out hotel dan di antar menuju <strong>Kyiv Boryspol International Airport ( KBP )</strong> untuk penerbangan kembali ke tanah air.</p>
                                            <div class="border"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <form method="POST" action="https://wisataukraina.gomodo.id/product/book/SKU19971581597394614211" accept-charset="UTF-8"><input name="_token" type="hidden" value="w12ULBNDbAVwB28Yb2oLXIaDarlWYjISNpGnc6Cm">
                    <div class="card">
                        <script>
                            var disabled = [];
                        </script>

                        <div class="row mt-3 embed-remove-d-none d-none">
                            <div class="col-embed-4 mb-3">
                                <div class="owl-carousel mt-1">
                                    <a data-fancybox="gallery-widget" href="https://wisataukraina.gomodo.id/uploads/products/hfdYtMnqbUcMhCQYg2dK8rR4RbpO1x2Z1wZYVvHu.jpg" data-height="1000"><img class="card-img-top myImg" id="myImg" src="https://wisataukraina.gomodo.id/uploads/products/hfdYtMnqbUcMhCQYg2dK8rR4RbpO1x2Z1wZYVvHu.jpg"
                                                                                                                                                                                                   alt="Card image cap"></a>
                                </div>
                            </div>
                            <div class="col pr-0 pl-0">
                                <h3>
                                    HILLSONG CHURCH 4D KYIV UKRAINE
                                </h3>
                                <table class="table-product">
                                    <tr>
                                        <td>
                                            <img src="https://wisataukraina.gomodo.id/img/pin.png" alt="">
                                        </td>
                                        <td colspan="3">
                                            Kyiv Oblast


                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="https://wisataukraina.gomodo.id/img/wall-clock.png" alt="">
                                        </td>
                                        <td colspan="3">
                                            4
                                            Hari
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="https://wisataukraina.gomodo.id/img/calendar.png" alt="">
                                        </td>
                                        <td>
                                            14 Aug 2020
                                        </td>
                                        <td>
                                            <span class="badge badge-warning bg-light-blue color-primary-blue">sampai</span>
                                        </td>
                                        <td>
                                            31 Oct 2020
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        <div class="card-body hide-notice-err embed-row">
                            <div class="embed-col-6">
                                <div class="form-group">
                                    <label for="">Pilih Tanggal</label>
                                    <script>

                                    </script>
                                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input"
                                               name="schedule_date"

                                               data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                                        <div class="input-group-append" data-target="#datetimepicker1"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" id="togglePriceTier"
                                           class="cursor-p">Harga <i
                                            class="fa fa-chevron-down"></i></label>
                                    <div class="box-price-tier" style="display: none">
                                        <table class="table table-borderless price-tier bg-light-gray">
                                            <tr>
                                                <td>1
                                                    Sampai 8

                                                    Orang
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>IDR 7,065,000
                                                    /
                                                    Orang
                                                </th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Orang

                                    </label>
                                    <input type="number" name="pax" class="form-control touchspin"
                                           value="1" data-min="1"
                                           data-max="8">
                                </div>
                            </div>
                            <div class="embed-col-6">
                                <div class="bg-light-blue p-3 include">
                                    <h3>Harga termasuk</h3>
                                    <ul>
                                        <li>Penginapan</li>
                                        <li>Penjemputan</li>
                                        <li>Pengantaran</li>
                                    </ul>
                                </div>
                                <div class="bg-light-blue p-3 exclude">
                                    <h3>Harga tidak termasuk</h3>
                                    <ul>
                                        <li>Makan</li>
                                    </ul>
                                </div>
                                <div class="include text-center bg-light-blue mt-4 pb-3" id="ket" style="padding-top:1rem;display:none;">

                                </div>
                            </div>
                        </div>

                        <div class="card-footer bg-light-blue hide-notice-err">
                            <div class="text-center">
                                <h5>Total Pembayaran</h5>
                                <h3 class="bold" id="total_price">IDR 0</h3>
                                <button class="btn btn-primary btn-block"
                                        id="btn-book">Pesan</button>
                            </div>
                        </div>
                        <div id="show-notice-err" class="card-body bg-light-blue text-center d-none">
                            <span>Produk ini tidak aktif</span>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
<footer class="container-fluid bg-dark">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-4 text-white mb-md-0 mb-3 text-justify">
                <p class="text-white about">
                    Tentang kami
                </p>
                <p class="text-white order-0 about-us">
                <p style="text-align:justify;">Akhirnya untuk pertama kalinya Anda dapat berwisata ke Ukraina, negara terbesar di Eropa dengan empat musim, negeri yang eksotis alamnya, indah arsitektur gedungnya, aman kota wisatanya dan cantik wanitanya.</p>
                </p>
            </div>
            <div class="col-md-4 order-1 order-lg-1 d-flex">
                <div class="address">
                    <p id="address"> Alamat</p>
                    <p>PT. Jaya Citra Hanami</p>
                    <p>Gedung EduCenter Lt.2A Unit 22332<br />Jl. Sekolah Foresta No.8, BSD City 15331, Indonesia</p>

                    <div class="phone-company">
                                <span class="text-white"><img src="https://wisataukraina.gomodo.id/img/048-telephone-1.png" width="16"
                                                              alt="">  <span>6285945225215</span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-5">
                <ul class="social">
                    <p>Media Sosial</p>
                    <li>
                        <a href="mailto:info@wisataukraina.com" class="text-white font-weight-normal">
                            <img src="https://wisataukraina.gomodo.id/img/159-email.png" alt=""> info@wisataukraina.com
                        </a>

                    </li>
                    <li>
                        <a href="https://facebook.com/wisataukraina" class="text-white font-weight-normal" target="_blank">
                            <img src="https://wisataukraina.gomodo.id/img/099-facebook.png" alt=""> wisataukraina
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/wisataukraina" class="text-white font-weight-normal" target="_blank">
                            <img src="https://wisataukraina.gomodo.id/img/080-instagram.png" alt=""> wisataukraina
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <div class="powered">
                    <span>POWERED BY</span> <span class="bold"><a href="#">GOMODO</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://wisataukraina.gomodo.id/landing/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>


<script type="text/javascript" src="https://wisataukraina.gomodo.id/js/public/material.js"></script>
<script>
    // For Gomodo Booking Widget
    if (window.location !== window.parent.location) {
        var doc = $(document);
        doc.find('nav,footer,.breadcrumb, .left-side-detail, .embed-remove').remove();
        doc.find('.embed-col-6').addClass('col-embed-6');
        doc.find('.embed-row').addClass('row');
        doc.find('.embed-remove-d-none').removeClass('d-none');
    }
</script>
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    if (window.location === window.parent.location) {
        (function () {
            var options = {
                whatsapp: 6285945225215, // WhatsApp number
                call_to_action: "Hubungi Kami", // Call to action
                position: "left", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    }
</script>
<!-- /WhatsHelp.io widget -->

<script>
    $(document).on('click', '#btn-navbar-toggle', function () {
        let t = $(this);
        $(t.data('target')).slideToggle(100);
    });

    function loadingStart() {
        $('.loading').addClass('show');
    }

    function loadingFinish() {
        $('.loading').removeClass('show');
    }

    toastr.options = {
        "positionClass": "toast-bottom-right",
    };
    $(document).on('click', '.current-language', function () {
        $('.language-option').slideToggle(300)
    });
    $(document).on('click', '.box-language', function () {
        let form = $('#landing-change-language');
        form.find('input[name=lang]').val($(this).parent().data('value'));
        form.submit();
    })
    $(document).on('click', '.can-copy', function () {
        let t = $(this);
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val(t.find('.data-copied').text()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success(t.find('.data-copied').text(), 'Copied');
    })
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    // Prevent Char in Input Type Number
    $('input[type="number"]').on('change keydown', function onChange(e) {
        if (e.metaKey == false) { // Enable metakey
            if (e.keyCode > 13 && e.keyCode < 48 && e.keyCode != 39 && e.keyCode != 37 || e.keyCode > 57) {
                e.preventDefault(); // Disable char. Enable arrow
            }
            ;
            if (e.shiftKey === true) { // Disable symbols
                if (e.keyCode > 46 && e.keyCode < 65) {
                    e.preventDefault();
                }
            }
        }
    })
    // disable mousewheel on a input number field when in focus
    // (to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })

    function showDeleteVoucher() {
        $('#btn-delete-voucher').closest('.voucher-button').css('display', 'block');
        $('#btn-apply-voucher').closest('.voucher-button').css('display', 'none');
        $('#voucher_code').prop('disabled', true);
    }

    function hideDeleteVoucher() {
        $('#btn-apply-voucher').closest('.voucher-button').css('display', 'block');
        $('#btn-delete-voucher').closest('.voucher-button').css('display', 'none');
        $('#voucher_code').prop('disabled', false);
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="https://wisataukraina.gomodo.id/dest-customer/lib/js/momentjs-id.js"></script>
<script src="https://wisataukraina.gomodo.id/dest-customer/lib/js/touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="https://wisataukraina.gomodo.id/js/owl.carousel.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script>
    let minDate;

    minDate = moment().add("1", 'days');

    $(function () {
        let maxDate = moment("10/31/2020", 'MM/DD/YYYY');
        if (minDate > maxDate && maxDate > moment()) {

            $('#show-notice-err').removeClass('d-none').addClass('d-block');
            $('.hide-notice-err').hide();
        } else {
            $('#datetimepicker1').datetimepicker({
                format: 'L',
                // date:moment('MM/DD/YYYY'),
                minDate: minDate.set({hour:0,minute:0,second:0,millisecond:0}).toDate(),
                maxDate: moment("10/31/2020", 'MM/DD/YYYY').set({hour:23,minute:59,second:59,millisecond:0}).toDate(),
                daysOfWeekDisabled: disabled,
                locale: 'id'
            });
        }

        $(".owl-carousel").owlCarousel({
            items: 1,
            lazyLoad: true,
            autoplay: true,
            autoplayHoverPause: true,
            nav: window.location === window.parent.location
        });
        $(document).on('click', '#togglePriceTier', function (e) {
            let t = $(this);
            if (!$('.box-price-tier').is(':visible')) {
                t.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            } else {
                t.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
            }
            $('.box-price-tier').stop().slideToggle(700);

        });
        let touch = $("input[name='pax']");
        touch.TouchSpin({
            min: touch.data('min'),
            max: touch.data('max')
        });
        calculatePrice();
        $(document).on('change', 'input[name=pax]', function () {
            let pax  = parseInt($(this).val());
            if (isNaN(pax)){
                pax = 0;
            }
            $(this).val(pax)
            calculatePrice();
        });
        $(document).on('change.datetimepicker', '#datetimepicker1', function () {
            calculatePrice();
        });

        function loadingStartButton(ele) {
            ele.prop('disabled', true);
            ele.html('<i class="fa fa-spin fa-refresh"></i> Memuat..')
        }

        function loadingFinishButton(ele, text) {
            ele.prop('disabled', false);
            ele.html(text)
        }

        function calculatePrice() {
            toastr.remove()
            let date = moment($('input[name=schedule_date]').val(), 'MM/DD/YYYY').format('YYYY-MM-DD');
            let pax = $('input[name=pax]').val();
            let product = "SKU19971581597394614211";
            loadingStartButton($('#btn-book'));
            $('#btn-book').attr('disabed', true).prop('disabled', true);
            $.ajax({
                url: "https://wisataukraina.gomodo.id/schedule",
                data: {
                    product: product,
                    schedule_date: date,
                    pax: pax
                },
                success: function (data) {
                    $('#btn-book').removeAttr('disabed').prop('disabled', false);
                    $('#total_price').html(data.result.grand_total_text);
                    if (data.result.company_discount_price > 0){
                        $('#ket').html(data.result.pax + ' x ' + data.result.priceText+'<br>'+data.result.discount_label+'  :   '+data.result.company_discount_price_text).show()
                    }else{
                        $('#ket').html(data.result.pax + ' x ' + data.result.priceText).show()
                    }


                    loadingFinishButton($('#btn-book'), 'Pesan');
                    if (data.result.can_book == false){
                        zero_val();
                    }

                },
                error: function (data) {
                    $('#total_price').html('IDR 0');
                    $('#btn-book').attr('disabed', true).prop('disabled', true);
                    loadingFinishButton($('#btn-book'), 'Pesan');
                    zero_val();
                    if (data.status !== undefined) {
                        switch (data.status) {
                            case 500:
                                toastr.error('Server is busy', 'Terjadi kesalahan')
                                break;
                            default:
                                toastr.error(data.responseJSON.message, 'Terjadi kesalahan')
                                break;
                        }
                    }
                }
            })
        }
    });

    function zero_val(){
        var zero_text = $('#total_price').text();
        var res = parseInt(zero_text.charAt(4));
        if (res === 0){
            $('#btn-book').prop('disabled', true);
        }
    }
</script>
</body>
</html>
