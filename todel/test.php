<!--PRODUCT CARD-->
<?php while ($row=mysqli_fetch_assoc($getProduct)){?>
    <div class="col-sm-6 col-md-4">
        <a class="card_product_theme" href=" <?php echo 'detilproduk.php?id='.$row['id_produk']?>" target="_blank">
            <article class="box detail_box">
                <figure>
                    <img class="img-product" src="Images/<?php echo $row['thumbnail'];?>">
                </figure>
                <div class="details">
                    <h4 class="box-title"><?php echo $row['nama_produk'];?></h4>
                    <hr>
                    <div class="card_short_desc"><?php echo $row['deskripsi'];?>
                    </div>
                    <ul class="features check">
                        <?php
                        $sqlKota = "SELECT kota.nama_kota FROM kota INNER JOIN produk ON produk.id_kota = kota.id_kota WHERE produk.id_kota=".$row['id_kota'];
                        $queryKota = mysqli_query($con, $sqlKota);
                        $getKota = mysqli_fetch_assoc($queryKota);
                        ?>
                        <li><?php echo $getKota['nama_kota'];?></li>
                        <li> <?php echo $row['durasi'];?>
                        </li>
                        <li>
                            <?php echo $row['tanggal_awal']. " - ". $row['tanggal_akhir'];?>
                        </li>
                    </ul>
                    <hr>
                    <span>Mulai Dari: </span>
                    <div class="price"> <?php echo "IDR ".$row['harga'];?></div>
                </div>
            </article>
        </a>
    </div>
    <?php
}
?>

</div>


<!--Render destinasi-->

<div class="col-sm-6 col-md-4">
    <article class="box">
        <figure>
            <a href="https://gomodo.id/explore/city/search?city=1">
                <img src="https://gomodo.id/storage/uploads/directory/popular/state/1/popular-state-HOJV0m1564590983.jpg" alt="">
                <figcaption>
                    <h2 class="caption-title">Aceh</h2>
                </figcaption>
            </a>
        </figure>
    </article>
</div>

<!-- Render Transportasi-->
<div class="col-sm-6 col-md-4">
    <a class="card_product_theme" href="https://sekooter.gomodo.id/product/detail/SKU204117515991167264981?ref=directory&amp;ref-url=https://gomodo.id/explore/transports/search" target="_blank">
        <article class="box detail_box">
            <figure>
                <img class="img-product"
                     src="https://gomodo.id/uploads/products/thumbnail/wsHrRNvfdb5AMuGLswcWypzZG3OYDOumvMgoe7aD.jpg"
                     alt="">
            </figure>
            <div class="details">
                <h4 class="box-title">Watersport In Tanjung Benoa: P...</h4>
                <hr>
                <div class="card_short_desc">Parasailing, Banana Boat, dan Jet Ski
                </div>
                <ul class="features check">
                    <li>Kab. Badung</li>
                    <li> 6 Hours
                    </li>
                    <li>
                        03 Sep 2020
                        - 31 Dec 2020
                    </li>
                </ul>
                <hr>
                <span>Mulai Dari: </span>
                <div class="price"> IDR 490,000</div>
            </div>
        </article>
    </a>
</div>

<!--Filter Section-->

<div class="col-sm-4 col-md-3 sidebar">
    <h4 class="search-results-title">
        <i class="soap-icon-search"></i>
        <div class="total-product-found-container">
            <b id="total-product">0</b> hasil pencarian ditemukan
        </div>
    </h4>
    <div class="toggle-container filters-container">
        <div class="panel style1 arrow-right">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#price-filter" class="collapsed">Harga</a>
            </h4>
            <div id="price-filter" class="panel-collapse collapse">
                <div class="panel-content">
                    <div id="price-range"></div>
                    <br />
                    <span class="min-price-label pull-left"></span>
                    <span class="max-price-label pull-right"></span>
                    <div class="clearer"></div>
                </div>
            </div>
        </div>
        <div class="panel style1 arrow-right">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#language-filter" class="collapsed">Bahasa Pemandu Aktivitas (Jika ada)</a>
            </h4>
            <div id="language-filter" class="panel-collapse collapse">
                <div class="panel-content">
                    <ul class="check-square filters-option" id="guides">
                        <li data-id="1"><a href="#">English
                                <small>(560)
                                </small>
                            </a></li>

                    </ul>
                </div>
            </div>
        </div>
        <button class="btn-medium uppercase sky-blue1 full-width" id="apply-filter" data-lang="Saring">Saring
        </button>
    </div>
</div>

<!--Data Provider -->
<div class="col-lg-4 col-md-6 col-product">
    <a href="https://adikmultiwisata.gomodo.id/product/detail/SKU100614715989659385044">
        <div class="card">
            <img class="card-img-top" src="./provider_files/6kTb5VCU9SuKZTYa2FZ9rysxIqCvhY2F9anxqUGo.jpg" alt="Card image cap">
            <div class="card-body">
                <div class="box-product-tags py-3">
                </div>
                <h3>
                    Mau Liburan Di Jogja Dengan Avanza
                </h3>
                <p class="card-text">Mari berliburan ke wisata jogja,dengan kami adikmultiwisata</p>
                <table class="table-product">
                    <tbody>
                        <tr>
                            <td>
                                <img src="./provider_files/pin.png" alt="">
                            </td>
                            <td colspan="3">Kota Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="./provider_files/wall-clock.png" alt="">
                            </td>
                            <td colspan="3">6 Jam</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="./provider_files/calendar.png" alt="">
                            </td>
                            <td>01 Sep 2020</td>
                            <td>
                                <span class="badge badge-warning bg-light-blue color-primary-blue">Sampai</span>
                            </td>
                            <td>30 Sep 2020</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="bg-light-blue text-center card-footer">
                <p>Mulai Dari</p>
                <p class="bold fs-22">IDR 2,500 / Orang</p>
                <button class="btn btn-primary">Pesan Sekarang</button>
            </div>
        </div>
    </a>
</div>

<!--DATA PRODUK LANDING-->
<li style="width: 270px; margin-right: 30px; float: left; display: block;" data-thumb-alt="">
    <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU52115610222369814?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
        <article class="box">
            <figure>
                <img class="img-product" src="../gomodo_files/J2ui0mMfiq8xAoyVeoyAfCbWevPXEwAf1uMX7ycH.jpg" alt="" draggable="false">
            </figure>
            <div class="details">
                <h4 class="box-title">Cobain/\;'custom</h4>
                <hr>
                <div class="card_short_desc">cobain
                </div>
                <ul class="features check">
                    <li>ASHKASHAM</li>
                    <li>                                                         20 Days
                    </li>
                    <li>
                        27 Nov 2019
                        - 01 May 2020
                    </li>
                </ul>
                <hr>
                <span>Mulai Dari: </span>
                <div class="price"> IDR 49,000</div>
            </div>
        </article>
    </a>
</li>
<li data-thumb-alt="" style="width: 270px; margin-right: 30px; float: left; display: block;">
    <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215631846255713?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
        <article class="box">
            <figure>
                <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
            </figure>
            <div class="details">
                <h4 class="box-title">cobain simple</h4>
                <hr>
                <div class="card_short_desc">cobain
                </div>
                <ul class="features check">
                    <li>EL ABADIA</li>
                    <li>                                                         5 Hours
                    </li>
                    <li>
                        16 Jul 2019
                        - 30 Sep 2020
                    </li>
                </ul>
                <hr>
                <span>Mulai Dari: </span>
                <div class="price"> IDR 50,000</div>
            </div>
        </article>
    </a>
</li>

<!--DUMP-->

<li style="width: 270px; margin-right: 30px; float: left; display: block;" data-thumb-alt="">
    <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU52115610222369814?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
        <article class="box">
            <figure>
                <img class="img-product" src="../gomodo_files/J2ui0mMfiq8xAoyVeoyAfCbWevPXEwAf1uMX7ycH.jpg" alt="" draggable="false">
            </figure>
            <div class="details">
                <h4 class="box-title">Cobain/\;'custom</h4>
                <hr>
                <div class="card_short_desc">cobain
                </div>
                <ul class="features check">
                    <li>ASHKASHAM</li>
                    <li>20 Days</li>
                    <li>
                        27 Nov 2019
                        - 01 May 2020
                    </li>
                </ul>
                <hr>
                <span>Mulai Dari: </span>
                <div class="price"> IDR 49,000</div>
            </div>
        </article>
    </a>
</li>
<li data-thumb-alt="" style="width: 270px; margin-right: 30px; float: left; display: block;">
    <a class="card_product_theme" href="https://myadventure.gomodo.id/product/detail/SKU521215631846255713?ref=directory&amp;ref-url=https://gomodo.id" target="_blank">
        <article class="box">
            <figure>
                <img class="img-product" src="../gomodo_files/no-product-image.png" alt="" draggable="false">
            </figure>
            <div class="details">
                <h4 class="box-title">cobain simple</h4>
                <hr>
                <div class="card_short_desc">cobain
                </div>
                <ul class="features check">
                    <li>EL ABADIA</li>


                    <li>                                                         5 Hours
                    </li>


                    <li>
                        16 Jul 2019
                        - 30 Sep 2020
                    </li>

                </ul>
                <hr>
                <span>Mulai Dari: </span>

                <div class="price"> IDR 50,000</div>
            </div>
        </article>
    </a>
</li>

<!--JS Transport-->
function render() {
tjq(document).find('#content button').prop('disabled', true).html(l);
if (page === 1) {
loadingStart('#main');
tjq('.listing-style1').empty();
}
let keyword = tjq('input[name=q]').val();
let city = tjq('input[name=city]').val();
let sort = tjq('input[name=sortBy]').val();
let transport = tjq('input[name=transport]').val();
let data = {
q: keyword,
city: city,
sort: sort,
price: price,
page: page,
transport: transport,
guides: getGuides(),
categories: getCategories()
};

if (page !== undefined || page !== '' || page !== null) {
tjq.ajax({
url: "https://gomodo.id/explore/render",
type: "POST",
data: data,
dataType: 'html',
success: function (data) {
loadingEnd('#main','Memuat lebih banyak transportasi');
tjq('#main .lds-dual-ring').show();
if (page === 1) {
tjq('.listing-style1').html(data)
} else {
tjq('.listing-style1').append(data);
}
if (tjq(document).find('.pagination-product').length > 0) {
tjq('#total-product').text(formatNumber(tjq(document).find('.pagination-product').data('total')));
if (tjq(document).find('.pagination-product').attr('data-nextpage') !== '') {
page = tjq(document).find('.pagination-product').attr('data-nextpage');
} else {
page = null
}

} else {
page = null

}
tjq('#main .lds-dual-ring').hide();
tjq(document).find('.pagination-product').remove();
if (page === null) {
tjq('#load-more').remove();
}
tjq(document).find('#content button').each(function () {
tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
},
error: function (e) {
console.log(e)
tjq(document).find('#content button').each(function () {
tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
}
})
} else {
tjq('#load-more').remove();
tjq(document).find('#content button').each(function () {
tjq(this).prop('disabled', false).html(tjq(this).attr('data-lang'));
})
}
}

render();

tjq(document).on('click', '#load-more', function () {
if (!tjq(this).prop('disabled')) {
loadingStart('#main');
render(page)
}
});