<?php
include "connection.php";
?>

<!DOCTYPE html>
<html>
<head>

    <title>Temukan berbagai aktivitas wisata &amp; kegiatan oudtdoor menarik di Indonesia hanya di Gomodo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="gomodo_files/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="gomodo_files/font-awesome.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="gomodo_files/animate.css">
    <link rel="stylesheet" href="gomodo_files/toastr.css">
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link id="main-style" rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/updates.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
<div id="page-wrapper">
    <div class="loading">
        <div class="loading-content">
            <div class="spin">
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
            <div class="loading-text">
                Memuat..
            </div>
        </div>
    </div>
    <div id="provider-modal" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <div class="provider-login">
                <h1>Login Sebagai Partner</h1>
                <a href="#">
                    <button class="button btn-medium uppercase sky-blue1">Login di sini</button>
                </a>
            </div>
            <div class="provider-signup">
                <h1>Register Sebagai Partner</h1>
                <a href="#">
                    <button class="button btn-medium uppercase sky-blue1">Daftar di sini</button>
                </a>
            </div>
        </div>
    </div>

    <header id="header" class="navbar-static-top">
        <div class="main-header not_home">
            <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                Mobile Menu Toggle
            </a>
            <div class="container">
                <h1 class="gomodo_logo logo navbar-brand">
                    <a href="home.php" title="Gomodo">
                        <img src="assets/logo.png" class="logo-gomodo" alt="Gomodo" />
                        <span class="gomodo_text">GOMODO</span>
                    </a>
                </h1>
                <nav id="main-menu" role="navigation">
                    <ul class="menu">
                        <li class="menu-item-has-children text-white">
                            <a href="home.php">Beranda</a>
                        </li>
                        <li class="text-black ">
                            <a href="transport.php">Transportasi</a>
                        </li>
                        <li class="text-white active">
                            <a href="aktivitas.php">Aktivitas</a>
                        </li>
                        <li class=" text-white ">
                            <a href="destinasi.php">Destinasi</a>
                        </li>
                        <li class="text-white ">
                            <a href="bantuan.html">Bantuan</a>
                        </li>
                        <li class="provider-login-nav text-white">
                            <a href="">Partner</a>
                        </li>
                        <li class="ribbon">
                            <a href="#"><img src="assets/idn.png" class="flag-image" alt="english" width="15px;"> ID</a>
                            <ul class="menu-mini">
                                <li class="">
                                    <a href="#" id="english" data-value="en" title="English"><img class="img-circle" src="assets/uk.png" width="15px"> EN</a>
                                </li>
                                <li class="active">
                                    <a href="#" class="text-black" id="indonesia" data-value="id" title="Indonesia"><img class="img-circle" src="assets/idn.png" width="15px"> ID</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <nav id="mobile-menu-01" class="mobile-menu collapse">
                <ul id="mobile-primary-menu" class="menu">
                    <li class="menu-item-has-children">
                        <a href="home.php">Beranda</a>
                    </li>
                    <li class="">
                        <a href="transport.php">Transportasi</a>
                    </li>
                    <li class="active">
                        <a href="aktivitas.php">Aktivitas</a>
                    </li>
                    <li class="">
                        <a href="destinasi.php">Destinasi</a>
                    </li>
                    <li class="">
                        <a href="bantuan.html">Bantuan</a>
                    </li>
                    <li class="provider-login-nav">
                        <a href="">Partner</a>
                    </li>
                </ul>
                <ul class="mobile-topnav container">
                    <li class="ribbon language menu-color-skin">
                        <a href="#">Indonesia</a>
                        <ul class="menu mini">
                            <li class=""><a href="#" id="indonesia" data-value="en" title="English">English</a></li>
                            <li class="active"><a href="#" id="indonesia" data-value="id" title="Indonesia">Indonesia</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>
    </header>

    <div class="page-title-container">
        <div class="container">
            <div class="page-title pull-left">
                <h2 class="entry-title">Eksplor Aktivitas</h2>
            </div>
            <ul class="breadcrumbs pull-right">
                <li><a href="home.php">Beranda</a></li>
                <li class="active">Aktivitas</li>
            </ul>
        </div>
    </div>
    <section id="content">
        <?php
        if (isset($_GET["keyword"])){
            ?><input name="keyword" type="hidden" value="<?php echo $_GET["keyword"];?>"><?php
        }
        ?>
        <div class="container all-activities">
            <div id="main">
                <div class="lds-dual-ring display-none"></div>
                <div class="row">
                    <div class="col-sm-4 col-md-3 sidebar">
                        <h4 class="search-results-title">
                            <i class="soap-icon-search"></i>
                            <div class="total-product-found-container">
                                <b id="total-product">0</b> hasil pencarian ditemukan
                            </div>
                        </h4>
                        <div class="toggle-container filters-container">
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#price-filter" class="collapsed">Harga</a>
                                </h4>
                                <div id="price-filter" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <input type="hidden" name="minPrice" id="min-price-value">
                                        <input type="hidden" name="maxprice" id="max-price-value">
                                        <div id="price-range"></div>
                                        <br/>
                                        <span class="min-price-label pull-left"></span>
                                        <span class="max-price-label pull-right"></span>
                                        <div class="clearer"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#accomodation-type-filter" class="collapsed">Aktivitas</a>
                                </h4>
                                <div id="accomodation-type-filter" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <ul class="check-square filters-option" id="categories">
                                            <?php
                                            if(isset($_GET["keyword"])){
                                                $sqlKategori = "SELECT detcat.id_detcat, detcat.nama_detcat, COUNT(produk.id_produk) AS jumlah 
                                                    FROM brdetcat INNER JOIN detcat ON detcat.id_detcat = brdetcat.id_detcat
                                                    INNER JOIN produk ON produk.id_produk = brdetcat.id_produk
                                                    WHERE produk.id_category = 1
                                                    AND produk.nama_produk LIKE '%".$_GET["keyword"]."%'
                                                    GROUP BY detcat.id_detcat
                                                ";
                                            }
                                            else{
                                                $sqlKategori = "SELECT detcat.id_detcat, detcat.nama_detcat, COUNT(produk.id_produk) AS jumlah 
                                                    FROM brdetcat INNER JOIN detcat ON detcat.id_detcat = brdetcat.id_detcat
                                                    INNER JOIN produk ON produk.id_produk = brdetcat.id_produk
                                                    WHERE produk.id_category = 1
                                                    GROUP BY detcat.id_detcat
                                                ";
                                            }
                                            $getKategori = mysqli_query($con, $sqlKategori);
                                            while ($row = mysqli_fetch_assoc($getKategori))
                                            {
                                                ?>
                                                <li class="categories" data-id="<?php echo $row["id_detcat"];?>"><a href="#"><?php echo $row["nama_detcat"];?>
                                                        <small>(<?php echo $row["jumlah"];?>)
                                                        </small>
                                                    </a></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel style1 arrow-right">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#language-filter" class="collapsed">Bahasa Pemandu Aktivitas (Jika ada)</a>
                                </h4>
                                <div id="language-filter" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <ul class="check-square filters-option" id="guides">
                                            <?php
                                            if(isset($_GET["keyword"])){
                                                $sqlBahasa = "SELECT bahasa.id_bahasa, bahasa.nama_bahasa, COUNT(produk.id_produk) AS jumlah FROM brbahasa 
                                                INNER JOIN bahasa ON bahasa.id_bahasa = brbahasa.id_bahasa 
                                                INNER JOIN produk ON produk.id_produk = brbahasa.id_produk 
                                                WHERE produk.id_category = 1
                                                AND produk.nama_produk LIKE '%".$_GET["keyword"]."%'
                                                GROUP BY bahasa.id_bahasa";
                                            }
                                            else{
                                                $sqlBahasa = "SELECT bahasa.id_bahasa, bahasa.nama_bahasa, COUNT(produk.id_produk) AS jumlah FROM brbahasa
                                                INNER JOIN bahasa ON bahasa.id_bahasa = brbahasa.id_bahasa
                                                INNER JOIN produk ON produk.id_produk = brbahasa.id_produk
                                                WHERE produk.id_category = 1
                                                GROUP BY bahasa.id_bahasa";
                                            }
                                            $getBahasa = mysqli_query($con, $sqlBahasa);
                                            while ($row = mysqli_fetch_assoc($getBahasa))
                                            {
                                                ?>
                                                <li class="guides" data-id="<?php echo $row["id_bahasa"];?>"><a href="#"><?php echo $row["nama_bahasa"];?>
                                                        <small>(<?php echo $row["jumlah"];?>)
                                                        </small>
                                                    </a></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <button class="btn-medium uppercase sky-blue1 full-width" id="apply-filter" data-lang="Saring">Saring
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div class="activities_list">
                            <div class="lds-dual-ring display-none"></div>
                            <div class="row image-box hotel listing-style1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer">
        <div class="footer-wrapper text-md-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <h2>Temukan</h2>
                        <ul class="discover triangle hover row text-left">
                            <li class="col-xs-6 "><a href="#">Siaran Pers</a></li>
                            <li class="col-xs-6 "><a href="#">Tentang Kami</a></li>
                            <li class="col-xs-6"><a href="#">Bermitra dengan Kami</a></li>
                            <li class="col-xs-6 "><a href="#">Blog</a></li>
                            <li class="col-xs-6"><a href="#">Login Mitra</a></li>
                            <li class="col-xs-6 "><a href="#">Karir</a></li>
                            <li class="col-xs-6 "><a href="bantuan.html">Bantuan</a></li>
                            <li class="col-xs-6 "><a href="#">Kebijakan</a></li>
                            <li class="col-xs-6 "><a href="#">Syarat dan Ketentuan</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2>Berlangganan Email</h2>
                        <p>Segera berlangganan email untuk mendapatkan penawaran menarik &amp; informasi terbaru dari Gomodo!</p>
                        <br />
                        <div class="form-newsletter">
                            <form method="GET" action="#" accept-charset="UTF-8" id="homepage-newsletter">
                                <div class="icon-check text-left">
                                    <input type="email" class="input-text width-email-footer" name="email" placeholder="Email Anda" maxlength="50" />
                                    <button class="subscribe loading-button width-button-email-footer">Berlangganan</button>
                                </div>
                            </form>
                        </div>
                        <br />
                        <span>Kami menghargai privasi Anda.</span>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <h2>Gomodo</h2>
                        <p>Discover. Book. Empower.</p>
                        <i class="fa fa-phone-square"> 0274 - 4288 - 422</i>
                        <br />
                        <br />
                        <ul class="social-icons clearfix">
                            <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li>
                            <li class="googleplus"><a title="instagram" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-facebook-f"></i></i></a></li>
                            <li class="youtube"><a title="youtube" href="#" data-toggle="tooltip" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <h2 class="margin-bottom-0">Metode Pembayaran</h2>
                        <div class="bank-icons clearfix text-center">
                            <img class="image-bank" src="assets/bri.png" alt="BRI Bank">
                            <img class="image-bank" src="assets/bni.png" alt="BNI Bank">
                            <img class="image-bank" src="assets/mandiri-bank.png" alt="Mandiri Bank">
                            <img class="image-bank" src="assets/master-card.png" alt="Master Card">
                            <img class="image-bank" src="assets/visa.png" alt="Visa">
                            <img class="image-bank" src="assets/verifiedbyvisa.png" alt="Verified by Visa">
                            <img class="image-bank" src="assets//mastercard-securecode.png" alt="Mastercard Securecode">
                            <img class="image-bank" src="assets/ssl.png" alt="SSL">
                            <a href="#" target="_blank">
                                <img class="image-bank" src="assets/pci-dss-watermark.png" alt="PCI" style="width:8rem;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom gray-area">
            <div class="container">
                <div class="pull-right back-top">
                    <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="fas fa-chevron-up"></i></a>
                </div>
                <div class="copyright pull-right">
                    <p>&copy; 2018 Powered by Gomodo Technologies</p>
                </div>
            </div>
        </div>
    </footer>
</div>

<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery.noconflict.js"></script>
<script type="text/javascript" src="https://gomodo.id/explore-assets/js/modernizr.2.7.1.min.js"></script>
<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery-ui.1.10.4.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://gomodo.id/explore-assets/js/bootstrap.js"></script>

<script type="text/javascript" src="https://gomodo.id/explore-assets/js/jquery.stellar.min.js"></script>

<script type="text/javascript" src="https://gomodo.id/explore-assets/js/waypoints.min.js"></script>

<script type="text/javascript" src="https://gomodo.id/explore-assets/js/theme-scripts.js"></script>
<script>
    tjq(document).ready(function(){
        var price = null;


        filter_data();

        tjq('#apply-filter').click(function() {
            filter_data();
        });

        tjq('.guides').click(function(){
            if(tjq(this).hasClass("active")){
                tjq(this).removeClass('active');
            }else{
                tjq(this).addClass('active');
            }
        });

        tjq('.categories').click(function(){
            if(tjq(this).hasClass("active")){
                tjq(this).removeClass('active');
            }else{
                tjq(this).addClass('active');
            }
        });

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        tjq('#price-range').slider({
            range:true,
            min:0,
            max:10000000,
            values:[0, 0],
            step:5000,
            slide: function (event, ui) {
                if((ui.values[1] - ui.values[0]) <= 50000){
                    return false;
                }
                tjq(".min-price-label").html("IDR " + formatNumber(ui.values[0]));
                tjq(".max-price-label").html("IDR " + formatNumber(ui.values[1]));
                price = ui.values[0] + '-' + ui.values[1];
                tjq('#min-price-value').val(ui.values[0]);
                tjq('#max-price-value').val(ui.values[1]);

            }
        });

        tjq('#price-range').draggable();
        tjq(".min-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 0)));
        tjq(".max-price-label").html("IDR " + formatNumber(tjq("#price-range").slider("values", 1)));
        price = tjq("#price-range").slider("values", 0) + '-' + tjq("#price-range").slider("values", 1);

        function filter_data()
        {
            tjq('.listing-style1').html('<div id="loading" style="" ></div>');
            var action = 'fetch_data';
            var minimum_price = tjq('#min-price-value').val();
            var maximum_price = tjq('#max-price-value').val();
            // var brand = get_filter('brand');
            // var ram = get_filter('ram');
            // var storage = get_filter('storage');
            var q = '1';
            var keyword = tjq('input[name=keyword]').val()
            console.log(keyword);
            // var price: price;

            tjq.ajax({
                url:"fetch-data.php",
                method:"POST",
                data:{
                    action:action,
                    q:q,
                    keyword: keyword,
                    minimum_price: minimum_price,
                    maximum_price: maximum_price,
                    // price: price
                    guides: getGuides(),
                    categories: getCategories()

                },
                success:function(data){
                    tjq('.listing-style1').html(data);
                    let total = tjq(data).filter('.col-sm-6').length;
                    tjq('#total-product').text(total);
                }
            });
        }

        function get_filter(class_name)
        {
            var filter = [];
            tjq('.'+class_name+':checked').each(function(){
                filter.push(tjq(this).val());
            });
            return filter;
        }

        tjq(document).on('click', '#apply-filter', function (){
            filter_data();
        })

        function getGuides(){
            var guides = [];
            tjq(document).find('#guides li.active').each(function (i, data){
                guides.push(tjq(data).data('id'));
            })
            return guides;
        }

        function getCategories(){
            var categories = [];
            tjq(document).find('#categories li.active').each(function (i, data){
                categories.push(tjq(data).data('id'));
            })
            return categories;
        }

    });
</script>

<script type="text/javascript">

    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    tjq('#main-menu ul li').not('.provider-login-nav, li.language, .ribbon').on('click', function (e) {
        tjq('.loading').addClass('show');
    });

    function loadingStart(place) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.addClass('loader');
        loaderButton.attr('disabled', true);
        loaderButton.html('<span class="lds-dual-ring"></span>Loading');
    }

    function loadingEnd(place, initialText) {
        var loaderButton = tjq(place).find('.loading-button');
        loaderButton.removeClass('loader');
        loaderButton.attr('disabled', false);
        loaderButton.html(initialText);
    }

    // Provider Login Modal
    tjq(document).on('click', '.provider-login-nav', function (e) {
        e.preventDefault();
        tjq('#provider-modal').css('display', 'block');
    })
    tjq(document).on('click', '.close', function () {
        tjq('#provider-modal').css('display', 'none');
    });
    tjq(document).on('click', 'li.language', function (e) {
        e.preventDefault();
    });
    tjq(document).ready(function () {
        tjq('.provider-login-nav').removeClass('active');
    });
    tjq(document).on('click', function (event) {
        if (!tjq(event.target).closest(".modal-content, .provider-login-nav").length) {
            tjq('#provider-modal').css('display', 'none');
        }
    });

    // Prevent Char in Input Type Number
    tjq(document).on('change keydown', 'input[type="number"], input[type="tel"], .number', function (e) {
        if (!digitOnly(e)) {
            e.preventDefault();
        }
    });

    function digitOnly(e) {
        let allowed = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'Delete', 'ArrowLeft', 'ArrowRight'];
        if (!allowed.includes(e.key)) {
            return false
        }
        return true
    }

    // Disable Paste in input type number
    tjq(document).on('paste', 'input[type="number"], input[type="tel"], .number', function (e) {
        e.preventDefault();
    });
    // Disable Scroll in input type number
    tjq(document).on('wheel', 'input[type="number"], input[type="tel"], .number', function (e) {
        tjq(this).blur();
    });
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="39be0d05b20505f614db789e-|49" defer=""></script></body>
</html>