-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2020 at 10:10 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahasa`
--

CREATE TABLE `bahasa` (
  `id_bahasa` int(11) NOT NULL,
  `nama_bahasa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bahasa`
--

INSERT INTO `bahasa` (`id_bahasa`, `nama_bahasa`) VALUES
(1, 'Indonesia'),
(2, 'Inggris'),
(3, 'Perancis'),
(4, 'Jepang'),
(5, 'Spanyol'),
(6, 'Arab'),
(7, 'Mandarin');

-- --------------------------------------------------------

--
-- Table structure for table `brbahasa`
--

CREATE TABLE `brbahasa` (
  `id_brbahasa` int(11) NOT NULL,
  `id_bahasa` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brbahasa`
--

INSERT INTO `brbahasa` (`id_brbahasa`, `id_bahasa`, `id_produk`) VALUES
(1, 2, 1),
(2, 1, 1),
(3, 1, 18),
(4, 2, 18),
(5, 1, 22),
(7, 2, 23);

-- --------------------------------------------------------

--
-- Table structure for table `brdetcat`
--

CREATE TABLE `brdetcat` (
  `id_brdetcat` int(11) NOT NULL,
  `id_detcat` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brdetcat`
--

INSERT INTO `brdetcat` (`id_brdetcat`, `id_detcat`, `id_produk`) VALUES
(1, 2, 1),
(2, 1, 1),
(3, 3, 1),
(4, 4, 1),
(5, 1, 2),
(6, 2, 2),
(7, 3, 2),
(8, 4, 2),
(9, 1, 6),
(10, 5, 6),
(11, 1, 7),
(12, 1, 8),
(13, 2, 8),
(14, 1, 9),
(15, 5, 9),
(16, 1, 10),
(17, 5, 10),
(18, 6, 21),
(19, 7, 21),
(20, 8, 22),
(21, 8, 23),
(22, 9, 28);

-- --------------------------------------------------------

--
-- Table structure for table `brexcl`
--

CREATE TABLE `brexcl` (
  `id_brexcl` int(11) NOT NULL,
  `id_exclusion` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brexcl`
--

INSERT INTO `brexcl` (`id_brexcl`, `id_exclusion`, `id_produk`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 3, 5),
(4, 1, 6),
(5, 3, 6),
(6, 3, 7),
(7, 1, 7),
(8, 1, 8),
(9, 3, 8),
(10, 1, 9),
(11, 3, 9),
(12, 1, 10),
(13, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `brincl`
--

CREATE TABLE `brincl` (
  `id_brincl` int(11) NOT NULL,
  `id_inclusion` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `brincl`
--

INSERT INTO `brincl` (`id_brincl`, `id_inclusion`, `id_produk`) VALUES
(1, 3, 1),
(2, 3, 5),
(3, 4, 5),
(4, 3, 6),
(5, 4, 6),
(6, 3, 22),
(7, 4, 22);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id_category` int(11) NOT NULL,
  `nama_category` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `nama_category`) VALUES
(1, 'Aktivitas'),
(2, 'Transportasi');

-- --------------------------------------------------------

--
-- Table structure for table `detcat`
--

CREATE TABLE `detcat` (
  `id_detcat` int(11) NOT NULL,
  `nama_detcat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detcat`
--

INSERT INTO `detcat` (`id_detcat`, `nama_detcat`) VALUES
(1, 'Sewa Mobil'),
(2, 'Wisata dengan Bus'),
(3, 'Wisata Bus Kota'),
(4, 'Wisata Seharian Penuh'),
(5, 'Wisata Aktif'),
(6, 'Aktivitas di Luar'),
(7, 'Snorkeling'),
(8, 'Olahraga Air'),
(9, 'Wisata Makanan');

-- --------------------------------------------------------

--
-- Table structure for table `exclusion`
--

CREATE TABLE `exclusion` (
  `id_exclusion` int(11) NOT NULL,
  `nama_exclusion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exclusion`
--

INSERT INTO `exclusion` (`id_exclusion`, `nama_exclusion`) VALUES
(1, 'Penginapan'),
(2, 'Pengantaran'),
(3, 'Makan'),
(4, 'Penjemputan');

-- --------------------------------------------------------

--
-- Table structure for table `inclusion`
--

CREATE TABLE `inclusion` (
  `id_inclusion` int(11) NOT NULL,
  `nama_inclusion` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inclusion`
--

INSERT INTO `inclusion` (`id_inclusion`, `nama_inclusion`) VALUES
(1, 'Penginapan'),
(2, 'Makan'),
(3, 'Penjemputan'),
(4, 'Pengantaran');

-- --------------------------------------------------------

--
-- Table structure for table `itinerary`
--

CREATE TABLE `itinerary` (
  `id_itinerary` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `detail` text NOT NULL,
  `id_produk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL,
  `nama_kota` varchar(50) NOT NULL,
  `id_provinsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id_kota`, `nama_kota`, `id_provinsi`) VALUES
(1, 'Yogyakarta', 10),
(2, 'Badung', 13),
(3, 'Bekasi', 8),
(4, 'Klaten', 9),
(5, 'Gianyar', 13),
(6, 'Sleman', 10),
(7, 'Lombok', 14),
(8, 'Kepulauan Seribu', 7),
(9, 'Pesawaran', 6),
(10, 'Karangasem', 13),
(11, 'Klungkung', 13),
(12, 'Kab. Lombok Timur', 14),
(13, 'Kab. Lombok Barat', 14),
(14, 'Depok', 8),
(15, 'Purwakarta', 8),
(16, 'Kab. Magelang', 9),
(17, 'Pandeglang', 12),
(18, 'Kab. Manggarai Barat', 15);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `id_picture` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `id_produk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id_picture`, `picture`, `id_produk`) VALUES
(1, 'ProdukKaravanBekasi1.jpg', 1),
(2, 'ProdukKaravanBekasi2.jpg', 1),
(3, 'IsuzuElf.jpg', 2),
(4, 'SuzukiAPV.jpg', 4),
(5, 'ToyotaHiace.jpg', 3),
(6, '1561683591-banner-safari-jogja-transport.png', 5),
(7, 'SJT Dieng.png', 6),
(8, 'SJT Innova Reborn.png', 7),
(9, '1561784379-20190217_104435_0001.png', 8),
(10, '1561782752-20190217_103952_0001.png', 9),
(11, '1561686797-PicsArt_02-16-01.39.05.png', 10),
(12, 'mahalombok.jpg', 11),
(13, 'avanzabinda.jpg', 12),
(14, 'ALPHARDTIPE VILFIRE(2016 UP)FULLDAY.jpg', 14),
(15, 'camry binda.jpg', 15),
(16, 'innovabinda.jpg', 16),
(17, 'elfbinda.jpg', 17),
(18, 'lombokkusuma.jpg', 18),
(19, 'mazu191.jpg', 19),
(20, 'mazu192.jpg', 19),
(21, 'mazu193.jpg', 19),
(22, 'mazu201.jpg', 20),
(23, 'mazu202.jpg', 20),
(24, 'pahawang.jpg', 21),
(25, '1.jpg', 22),
(26, 'skoter2.jpg', 22),
(27, 'skot22-1.jpg', 23),
(28, 'skot22-2.jpg', 23),
(29, 'skot22-3.jpg', 23),
(30, 'sko24.jpg', 24),
(31, 'bakas11.jpg', 25),
(32, 'bakas12.jpg', 25),
(33, 'bakasjimbaran.jpg', 26),
(34, 'pucakmanik.jpg', 27),
(35, 'pucak.jpg', 27),
(36, 'sembalun.jpg', 28),
(37, 'sasak2.jpg', 29),
(38, 'sasak.jpg', 29),
(39, 'KAYAK1.jpg', 30),
(40, 'kayak2.jpg', 30),
(41, 'tebing1.jpg', 31),
(42, 'vw1.jpg', 32),
(43, 'vw2.jpg', 32),
(44, 'masak.jpg', 33),
(45, 'peucang1.jpg', 34),
(46, 'peucang2.jpg', 34),
(47, 'speedboat.jpg', 35),
(48, '1day.jpg', 36),
(49, '1day1.jpg', 36),
(50, 'avanxa.jpeg', 37);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `durasi` varchar(20) DEFAULT NULL,
  `tentang` varchar(500) DEFAULT NULL,
  `catatan` varchar(500) DEFAULT NULL,
  `harga` double NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `id_kota` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `nama_tempat` varchar(50) NOT NULL,
  `thumbnail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi`, `durasi`, `tentang`, `catatan`, `harga`, `tanggal_awal`, `tanggal_akhir`, `id_kota`, `id_provider`, `id_category`, `nama_tempat`, `thumbnail`) VALUES
(1, 'Sewa Bus Pariwisata', 'Sewa Bus', '1 Hari', 'sewa bus bekasi melayani transportasi kapasitas bus dari 8 seat hingga 59 seat', 'sewa bus pariwisata bekasi \r\n\r\nharga sewa tidak termasuk tol, parkir dan tips driver \r\n\r\nharga sewa sudah termasuk BBM solar, driver dan armada\r\n\r\n', 2500000, '2020-07-31', '2020-12-31', 3, 5, 2, 'Bekasi', 'ProdukKaravanBekasi1.jpg'),
(2, 'Isuzu Elf', 'Sewa Minibus', '1 Hari', '', 'Airport Area, Jimbaran, Nusa Dua, Uluwatu, Seminyak, Legian, Kuta, Kerobokan, Ubud ,Denpasar. Please contact us if your hotel stay outside of above area mentioned extra cost charge apply.\r\nExtra hour used is will be applied Rp50.000/hour charge\r\nAll the p', 1330000, '2020-07-01', '2021-02-27', 5, 6, 2, 'Bali Semara Tour', 'IsuzuElf.jpg'),
(3, 'Toyota Hiace', 'Sewa Minibus', '1 Hari', '', 'Pickup Area\r\nAirport Area, Jimbaran, Nusa Dua, Uluwatu, Seminyak, Legian, Kuta, Kerobokan, Ubud ,Denpasar. Please contact us if your hotel stay outside of above area mentioned extra cost charge apply.\r\n\r\nTerms & Conditions\r\nExtra hour used is will be applied.', 1330000, '2020-08-01', '2021-02-28', 5, 6, 2, 'Bali Semara Tour', 'ToyotaHiace.jpg'),
(4, 'Suzuki APV', 'Sewa Mobil Bali', '1 Hari', '', 'Pickup Area Airport Area, Jimbaran, Nusa Dua, Uluwatu, Seminyak, Legian, Kuta, Kerobokan, Ubud ,Denpasar. Please contact us if your hotel stay outside of above area mentioned extra cost charge apply. Terms & Conditions Extra hour used is will be appl\r\n\r\n', 532000, '2020-08-01', '2021-03-01', 5, 6, 2, 'Bali Semara Tour', 'SuzukiAPV.jpg'),
(5, 'Sewa Mobil Jogja AVANZA', 'Sewa Mobil Jogja', '1 Hari', 'Paket Transport\r\n\r\nCity TOUR Yogyakarta dengan Avanza dan sejenisnya. \r\n\r\nBebas tentukan destinasi yang sejalur\r\nDurasi waktu yang tidak mengikat (Maksimal 23:00)\r\nBBM & Driver Sudah termasuk\r\nParkir & Makan Driver belum termasuk\r\nTanya langsung via WA ka', 'Destinasi dalam provinsi Yogyakarta (Borobudur & Prambanan termasuk).\r\n\r\nDilarang Merokok di dalam kendaraan & Membawa Makanan Durian ke dalam kendaraan.', 450000, '2020-07-01', '2021-07-01', 6, 7, 2, 'Djuragan Wisata Indonesia', '1561683591-banner-safari-jogja-transport.png'),
(6, 'Paket Transport City Tour DIENG Avanza', 'Sewa Mobil Jogja', '1 Hari', 'Paket transport wisata ke Dieng dari Jogjakarta.\r\n\r\n4-6 penumpang \r\nAvanza, Mobilio, Luxio dan sejenisnya\r\nkeberangkatan dari Jogjakarta dst.\r\nuntuk sunrise Sikunir Keberangkatan 23:00\r\nsudah termasuk BBM & Driver \r\nbelum termasuk Tiket wisata & Parkir.\r\n', 'Dilarang merokok didalam kendaraan', 650000, '2020-07-01', '2021-07-01', 6, 7, 2, 'Djuragan Wisata Indonesia', 'SJT Dieng.png'),
(7, 'Sewa Mobil Jogja INNOVA', 'Sewa Mobil Jogja', '1 Hari', 'Paket Transport\r\n\r\nCity TOUR Yogyakarta dengan Innova dan sejenisnya. \r\n\r\nBebas tentukan destinasi yang sejalur\r\nDurasi waktu yang tidak mengikat (Maksimal 23:00)\r\nBBM & Driver Sudah termasuk\r\nParkir & Makan Driver belum termasuk\r\nTanya langsung via WA ka', 'Destinasi dalam provinsi Yogyakarta (Borobudur & Prambanan termasuk).\r\n\r\nDilarang Merokok di dalam kendaraan & Membawa Makanan Durian ke dalam kendaraan.', 750000, '2020-07-01', '2021-07-01', 6, 7, 2, 'Djuragan Wisata Indonesia', 'SJT Innova Reborn.png'),
(8, 'Sewa Minibus Jogja ELF 10 Seat', 'Sewa Minibus Jogja', '1 Hari', 'Paket Transport\r\n\r\nCity TOUR Yogyakarta dengan Isuzu Elf dan sejenisnya 10-12 Seats. \r\n\r\nBebas tentukan destinasi yang sejalurDurasi waktu yang tidak mengikat (Maksimal 22:00)\r\nBBM & Driver Sudah termasuk\r\nParkir & Makan Driver belum termasuk\r\nPemakaian J', 'Tidak boleh merokok didalam kendaraan\r\n\r\nTidak boleh membawa Durian kedalam kendaraan\r\n\r\n', 750000, '2020-08-01', '2021-07-01', 6, 7, 2, 'Djuragan Wisata Indonesia', '1561784379-20190217_104435_0001.png'),
(9, 'Paket Transport City Tour DIENG HI Ace', 'Sewa Minibus', '1 Hari', 'Paket transport wisata ke Dieng dari Jogjakarta.\r\n\r\n10 - 15 penumpang\r\nHI Ace, Isuzu ELF dan sejenisnya\r\nkeberangkatan dari Jogjakarta dst.\r\nuntuk sunrise Sikunir Keberangkatan 23:00\r\nsudah termasuk BBM & Driver\r\nbelum termasuk Tiket wisata & Parkir.\r\nTan', 'Dilarang Merokok didalam kendaraan', 1250000, '2020-09-01', '2021-05-01', 1, 7, 2, 'Djuragan Wisata', '1561782752-20190217_103952_0001.png'),
(10, 'City Tour Jogja HIACE', 'Sewa Minibus', '1 Hari', 'Paket Transport\r\n\r\nCity TOUR Yogyakarta dengan HI Ace dan sejenisnya 12-18 Seat. \r\n\r\nBebas tentukan destinasi yang sejalurDurasi waktu yang tidak mengikat (Maksimal 22:00)\r\nBBM & Driver Sudah termasuk\r\nParkir & Makan Driver belum termasuk\r\nPemakaian Jogja', 'Destinasi dalam provinsi Yogyakarta (Borobudur & Prambanan termasuk).\r\n\r\nDilarang Merokok di dalam kendaraan & Membawa Makanan Durian ke dalam kendaraan.', 1100000, '2020-09-01', '2021-02-01', 1, 7, 2, 'Djuragan Wisata Indonesia', '1561686797-PicsArt_02-16-01.39.05.png'),
(11, 'Antar Jemput Bandara & Pelabuhan', 'Sewa Mobil Lombok', '1 Hari', 'Antar Jemput Bandara Pelabuhan (Airport & Port Shuttle) Adalah Perjalanan Dari Bandara Ke Pelabuhan Bangsal Atau dari Pelabuhan bangsal Ke bandara Dengan Menempuh Waktu Perjalanan +- 1 Jam ( 1 Hour ) \r\n\r\n', 'Pelanggan ( Customer ) Harus memberitahukan Terlebih dahulu Waktu Chek in dan Chek Out Minimal 1 Hari Sebelumnya\r\n\r\n', 600000, '2020-09-01', '2021-02-06', 7, 8, 2, 'Lombok International Airport', 'mahalombok.jpg'),
(12, 'Sewa Avanza Klaten', 'Sewa Mobil Klaten', '1 Hari', 'Anda sedang mencari sarana transportasi di seputaran Jawa Tengah, baik untuk liburan keluarga atau keperluan bisnis? Kami Binda Rental, menyediakan jasa sewa Avanza. Toyota Avanza salah satu jenis kendaraan untuk sewa mobil di Bali, yang kami sediakan unt', 'Kapasitas mobil Toyota Avanza, 6 penumpang termasuk sopir.\r\nRuang bagasi kendaraan maksimal 2 koper.\r\nKelebihan waktu pemakaian lebih dari 24 jam, akan dikenakan biaya overtime.\r\nLokasi penjemputan di semua wilayah Klaten.\r\nPenjemputan luar kota akan dike', 225000, '2020-09-01', '2021-02-01', 4, 3, 2, 'Binda Car Rental', 'avanzabinda.jpg'),
(14, 'Sewa Alphard Klaten', 'Sewa Mobil Klaten', '1 Hari', 'Anda sedang mencari sarana transportasi di seputaran Jawa Tengah, baik untuk liburan keluarga atau keperluan bisnis? Kami Binda Rental, menyediakan jasa sewa Toyota Alphard. Toyota Alphard adalah salah satu mobil mewah yang banyak dicari dengan berbagai f', 'Harga rental Toyota Alphard dihitung per 12 Jam dalam satu hari.\r\nKelebihan pemakaian per jam dalam satu hari akan dikenakan biaya overtime Rp160.000 per jamnya.\r\nHarga sudah termasuk, mobil Toyota Alphard, supir dan bahan bakar minyak.', 1600000, '2020-09-01', '2021-01-01', 4, 3, 2, 'Binda Car Rental', 'ALPHARDTIPE VILFIRE(2016 UP)FULLDAY.jpg'),
(15, 'Sewa Camry Klaten', 'Sewa Mobil Klaten', '1 Hari', 'Bagi anda yang memerlukan kendaraan tipe sedan, untuk sewa mobil dan driver di Jawa Tengah. Kami Binda Car Rental, menyediakan jasa sewa Camry di Jawa Tengah. Sewa mobil Toyota Camry di Jawa Tengah yang kami sediakan hanya dengan supir professional, dan h', 'Kapasitas mobil Toyota Camry, 4 penumpang termasuk sopir.\r\nRuang bagasi kendaraan maksimal 2 koper.\r\nKelebihan waktu pemakaian lebih dari 12 jam, akan dikenakan biaya overtime.\r\nLokasi penjemputan di semua wilayah Klaten.\r\nPenjemputan luar kota akan diken', 1600000, '2020-09-01', '2021-02-01', 4, 3, 2, 'Binda Car Rental', 'camry binda.jpg'),
(16, 'Sewa Innova Klaten', 'Sewa Mobil Klaten', '1 Hari', 'Anda sedang mencari sarana transportasi di seputaran Jawa Tengah, baik untuk liburan keluarga atau keperluan bisnis? Kami Binda Rental, menyediakan jasa sewa Innova. Toyota Innova salah satu jenis kendaraan untuk sewa mobil di Klaten, yang kami sediakan u', 'Kapasitas mobil Toyota Innova, 6 penumpang termasuk sopir.\r\nRuang bagasi kendaraan maksimal 2 koper.\r\nKelebihan waktu pemakaian lebih dari 24 jam, akan dikenakan biaya overtime.\r\nLokasi penjemputan di semua wilayah Klaten.\r\nPenjemputan luar kota akan dike', 450000, '2020-09-02', '2021-02-01', 4, 3, 2, 'Binda Car Rental', 'innovabinda.jpg'),
(17, 'Sewa Elf Klaten', 'Sewa Mobil Klaten', '1 Hari', 'Bagi anda yang berlibur ke Klaten dengan jumlah peserta melebihi 6 orang, maka anda harus mencari kendaraan jenis microbus. Mobil microbus seperti Isuzu Elf, Toyota Hiace atau Bus pariwisata 20 seat. Khusus pada halaman ini, kami Binda Car Rental, menawar', 'Harga sewa Elf di atas, untuk durasi sewa 12 jam dalam 1 hari.\r\nKelebihan pemakaian 12 Jam dalam sehari akan dikenakan biaya overtime sebesar 10 % dari harga rental Isuzu Elf.\r\nHarga rental Elf diatas termasuk mobil, supir dan BBM.\r\nBiaya rental Isuzu ELF', 950000, '2020-09-01', '2021-02-01', 4, 3, 2, 'Binda Car Rental', 'elfbinda.jpg'),
(18, 'PAKET WISATA LOMBOK 2 HARI 1 MALAM', 'Wisata Lombok', '36 jam', 'Siapa yang tidak suka menjelajahi keindahan bawah laut Lombok? Tempat ini adalah surganya keindahan alam dan makhluk-makhluk laut yang luar biasa.\r\n\r\nDengan tur ini, kamu akan diajak snorkeling di Gili Meno, Air, dan Trawangan. Dan setelah itu, kamu bisa ', 'Waktu keberangkatan dan waktu dimulainya tur dapat dikonfirmasi dan ditentukan langsung antara peserta tur dan Kusuma Lombok Tour saat melakukan konfirmasi pemesanan.', 1200000, '2020-09-01', '2021-03-01', 7, 10, 1, 'Kusuma Tour Lombok', 'lombokkusuma.jpg'),
(19, 'Kelas Menyelam SSI Open Water di Pulau  Pramuka', 'Diving Course', '3 Hari', 'SSI Open Water Diver\r\n- Classroom sessions, diving theory (Video or Online training)\r\n- Pool sessions, practical skills.\r\n- Open Water Session (1 Snorkeling & 4 scuba dives)\r\n\r\nIncludes:\r\n- Certificate\r\n- Log Book\r\n- Decompression Table\r\n- Scuba Gear Comp', 'Persyaratan penting :\r\n1. Tidak diperbolehkan bagi yang berpenyakit Jantung, Sinusitis, asma dan penyakit paru lainnya.\r\n2. Bagi yang pernah terkena penyakit Covid 19 harus membawa surat keterangan dokter serta surat hasil check up.\r\n3. Usia minimum 10 tahun\r\n\r\n \r\n\r\nExclude :\r\n1. Ancol Ticket\r\n\r\nPool Session dan Ujian Laut akan dilaksanakan di Pulau Pramuka\r\n\r\n\r\nMeeting point dan registrasi dilakukan di Marina Ancol Pier 16', 5750000, '2020-09-01', '2021-02-01', 8, 11, 1, 'Mazu Dive Center', 'mazu191.jpg'),
(20, 'Kelas Menyelam dengan Penyelam Mazu untuk Pemula(SSI Open Water)', 'Diving Course', '3 Hari', 'SSI Open Water Diver\r\n- Classroom sessions, diving theory (Video or Online training)\r\n- Pool sessions, practical skills.\r\n- Open Water Session (1 Snorkeling & 4 scuba dives)', 'Persyaratan penting :\r\n1. Tidak diperbolehkan bagi yang berpenyakit Jantung, Sinusitis, asma dan penyakit paru lainnya.\r\n2. Bagi yang pernah terkena penyakit Covid 19 harus membawa surat keterangan dokter serta surat hasil check up.\r\n3. Usia minimum 10 tahun\r\n\r\nExclude :\r\n1. Ancol Ticket\r\n\r\nPool Session akan dilaksanakan di Kolam Simprug atau kolam Sepolwan\r\nUjian Laut akan dilaksanakan di Pulau Pramuka\r\nSaat pelaksanaan ujian Laut meeting point dan registrasi dilakukan di Marina Ancol Pier 16', 4750000, '2020-09-01', '2021-02-28', 8, 11, 1, 'Mazu Dive Center', 'mazu201.jpg'),
(21, 'One Day Trip Pulau Pahawang', 'Island Trip', '1 Hari', 'TUJUAN WISATA :\r\n\r\na. Pulau Pahawang Besar\r\n\r\n- Cukuh Bedil\r\n\r\n- Taman Nemo\r\n\r\n- Pahawang Kuring\r\n\r\n- Jelarangan\r\n\r\nb. Pulau Pahawang Kecil\r\n\r\nc. Pulau Kelagian Kecil/ besar', '• Calon peserta membawa surat keterangan sehat Puskes/ klinik (jika ada)\r\n\r\n• Calon peserta mengikuti aturan protokol kesehatan\r\n\r\n• Calon peserta melakukan dp mininal 30% dari harga tersebut\r\n\r\n• Apabila salah satu/ rombongan Peserta sudah melakukan dp, dan\r\n\r\nmembatalkan trip dinyatakan DP angus/ hilang terkecuali ada yg\r\n\r\nmenggantikan, / perubahan jadwal untuk group (konfirmasi)\r\n\r\n• Apabila peserta di saat melakukan Sekrining dan suhu > 38° Akan di pisahkan\r\n\r\ndari rombongan (ODP)', 800000, '2020-09-01', '2020-12-31', 9, 12, 1, 'Wisata Pahawang', 'pahawang.jpg'),
(22, 'Watersport In Tanjung Benoa: Package A', 'Olahraga Air', '6 jam', 'Paket ini terdiri dari 1x Single Parasailing, 1x putaran Banana Boat dan Rolling Donut.\r\n\r\nGratis antar jemput di area:\r\nNusa Dua, Tanjung Benoa, Jimbaran, Kuta, dan Legian', 'Tiket ini tidak dapat dibatalkan dan dana tidak dapat dikembalikan\r\n\r\nJika cuaca tidak menentu, mohon konfirmasi H-1 untuk penggantian jadwa', 450000, '2020-09-01', '2021-03-01', 2, 13, 1, 'Tanjung Benoa', 'skoter2.jpg'),
(23, 'Watersport In Tanjung Benoa: Package B', 'Olahraga Air', '6 jam', 'Paket ini terdiri dari 1x Single Parasailing, 1x putaran Banana Boat dan Jet Ski.\r\n\r\nGratis antar jemput di area:\r\nNusa Dua, Tanjung Benoa, Jimbaran, Kuta, dan Legian', 'Tiket ini tidak dapat dibatalkan dan dana tidak dapat dikembalikan\r\n\r\nJika cuaca tidak menentu, mohon konfirmasi H-1 untuk penggantian jadwal', 490000, '2020-09-01', '2021-03-01', 2, 13, 1, 'Tanjung Benoa', 'skot22-3.jpg'),
(24, 'Rafting In Telaga Waja Bali', 'Olahraga Air', '7 jam', 'Trek sungai yang mencapai 2,5 - 3 jam untuk diarungi ini patut untuk dicoba. Diselingi dengan alam Bali Timur yang mempunyai tebing-tebing berbatu, persawahan, dan hutan. Rafting ini akan dipandu oleh instruktur yang sangat berpengalaman.', 'Harga sudah termasuk penjemputan di area Ubud, Sanur, Kuta, Candi Dasa, Sidemen, Canggu, Jimbaran, Nusa Dua, Ungasan, Pecatu, Uluwatu, Amed\r\nPenjemputan dilakukan mulai pukul 07:00\r\n\r\nTiket tidak dapat dibatalkan dan dana tidak dapat dikembalikan\r\n\r\n', 650000, '2020-09-01', '2021-01-31', 10, 13, 1, 'Sungai Telaga Waja', 'sko24.jpg'),
(25, 'Bakas Cooking Class', 'Kelas Memasak', '6 jam', 'If you’ve tried Bali’s unique cuisine while traveling to the island and want to be able to recreate it back home, this Balinese cooking class is an ideal activity for you. Visit a local market to shop for fresh ingredients. Learn about the elements and techniques of Balinese food from a professional Balinese cook, then enjoy eating the fruits (and vegetables) of your labor at the end of the class', 'Program Inclusions : \r\n\r\n• Hotel transfer service Ubud , Sanur ,Nusa Dua , Kuta \r\n• Visiting traditional markets (Only Morning Session)\r\n• Lunch\r\n• Cooking Class Certificate\r\n• Air-conditioned vehicle\r\n\r\nExclusions: \r\n\r\n\r\n• Manner Tips\r\n• Other hotel transfer service areas (the additional charge applied)\r\n• All things except inclusions\r\n• Individual traveler’s insurance', 950000, '2020-09-01', '2021-01-31', 11, 14, 1, 'Bakas Cooking Class', 'bakas11.jpg'),
(26, 'Uluwatu Sunset Candle Light Dinner Jimbaran', 'Destinasi Kuliner', '7 jam', 'uluwatu sunset and candles light dinner at jimbaran bay. Seeing the sunset and having dinner at Jimbaran bay is a one program that you dedicate to your partner to make it more affectionate \r\n\r\n', 'pickup from hotel at 14:30pm\r\n\r\nitinerary :\r\n\r\nUluwatu temple see the sunset \r\n\r\nKecak & Fire dance ( optional )\r\n\r\nJimbaran bay barbeque candle light dinner \r\n\r\nReturn back to hotel', 1100000, '2020-09-01', '2021-01-31', 2, 14, 1, 'Pantai Jimbaran', 'bakasjimbaran.jpg'),
(27, 'Trekking ke Pucak Manik & Batu Goak dengan Pemandu', 'Mendaki Gunung', '3 jam', 'Mendaki ke kawasan Pucak Manik untuk menikmati air terjun Pucak Manik melewati rute Batu Goak (batu yang menyerupai burung) di kawasan hutan desa Wanagiri dengan pemandu lokal', 'Harga Termasuk:\r\n- Air Mineral\r\n- Tongkat\r\n- Pemandu Lokal\r\n- Asuransi\r\n\r\nTur ini tidak dapat dibatalkan', 265000, '2020-10-01', '2021-02-01', 2, 13, 1, 'Wanagiri Pucak Manik Waterfall', 'pucakmanik.jpg'),
(28, 'Cooking Class Geofood Sembalun', 'Kelas Memasak', '3 jam', 'Kelas memasak ini memanfaatkan sayur mayur fresh yang di tanam oleh masyarakat dengan proses bumbu tradisional yang lumrah di pakai oleh orang Sembalun. ', 'Cocok untuk yang ingin mendapatkan sensasi makanan di daerah dingin', 200000, '2020-10-01', '2021-02-01', 12, 15, 1, 'Sembalun', 'sembalun.jpg'),
(29, 'Gala Dinner Kebudayaan Sasak', 'Destinasi Kuliner', '2 jam', 'Prosesi penyambutan tradisional gala dinner masyarakat sasak selatan Rinjani, pendampingan pagar ayu, gamelan, pentas tari tradisional dan (tari api kondisional), jangger dan dulang makanan sasak.', 'Kegiatan gala dinner kebuayaan masyarakat sasak selatan rinjani hanya dilaksanakan 1 sekali seminggu, yakni pada malam minggu saja.\r\n\r\n', 500000, '2020-10-01', '2021-01-01', 12, 15, 1, 'Bale Bale Cafe n Homestay Lombok Timur', 'sasak.jpg'),
(30, 'Kayaking in Taman Kaldera Setu Jatijajar', 'Olahraga Kayak', '3 jam', 'Melepas rasa penat dari kepadatan ibukota namun dengan waktu yang terbatas? Aktifitas kayak di Taman Kaldera bisa jadi jawaban kamu. Bukan hanya kayak saja yang bisa kamu nikmati di sini, lingkungan hijau yang asri dengan sarana dan edukasi Hidroponik juga ada, jangan lupakan juga untuk mengabadikan momen di tiap sudutnya.', 'Harga Sudah Termasuk:\r\n- Welcome drink dan Snack\r\n- Fasilitator\r\n- Pelampung\r\n- Air mineral (free flow)\r\n- Ruang ganti\r\n\r\nHarga Belum Termasuk:\r\n- Pengeluaran Pribadi\r\n- Makan\r\n- Perlengkapan Mandi', 75000, '2020-10-01', '2021-02-01', 14, 16, 1, 'Situ Jatijajar', 'kayak2.jpg'),
(31, 'Via Ferrata Gunung Parang 900 MDPL', 'Mendaki Gunung', '3 jam', 'Pacu adrenalin kamu dengan meniti via ferrata pertama di Indonesia. Skywalker via ferrata adalah operator pertama via ferrata di indonesia, terletak di Gunung Parang Purwakarta, berlokasi tidak jauh dari jakarta menjadikan liburan di skywalker pilihan yang tepat dan berkesan. Rasakan pengalaman menantang meniti tangga besi di jalur yang sudah disiapkan dengan ketinggian 900 MDPL.', 'Himbauan yang Harus Diperhatikan:\r\n- Menggunakan pakaian yang nyaman kaus tangan panjang akan sangat membantu\r\n- Mengunakan sepatu kets\r\n- Membawa topi, sun block, kacamata hitam, dan baju ganti\r\n- Membawa air minum\r\n\r\nKetentuan Pembatalan:\r\nPembatalan yang dilakukan pada H-2 sebelum tanggal kunjungan akan dikembalikan Rp 150.000,-/orang\r\nPembatalan yang dilakukan pada H-1 dana tidak dapat dikembalikan\r\n\r\nForce Majeure:\r\nPengembalian dana penuh atau penggantian jadwal\r\n\r\nKetentuan Pengembalian D', 350000, '2020-10-01', '2021-02-01', 15, 16, 1, 'Tebing Parang', 'tebing1.jpg'),
(32, 'Borobudur VW Short Trip: Desa Wanurejo & Pembuatan Gerabah', 'Tour Singkat', '3 jam', 'Menulusuri destinasi sekitar Borobudur dengan Mobil VW antique warna warni yang dapat memuat hingga 4 penumpang dan 1 driver. Untuk New Normal, dapat memuat 3 penumpang dan 1 driver.\r\nDi paket Short Cultural trip A ini, Anda bisa menikmati Desa Wisata Wanurejo.\r\nPerpaduan antara berwisata menikmati keasrian alam pedesaan, kearifan lokal aktivitas & adat masyarakat desa dan homestay-homestay bergaya Jawa Klasik yang hanya ada di Desa Wisata Wanurejo. Tempat Souvenir kerajinan Gerabah,\r\nwisatawan ', 'Ketentuan Pembatalan:\r\nPembatalan yang dilakukan pada H-7 sebelum tanggal kunjungan akan dikembalikan senilai Rp 300.000,-\r\nPembatalan yang dilakukan pada H-2 sebelum tanggal kunjungan akan dikembalikan senilai Rp 187.500,-\r\nPembatalan yang dilakukan pada H-1 dana tidak dapat dikembalikan\r\n\r\nKetentuan Pengembalian Dana:\r\nPengembalian dana akan diproses dalam waktu 5 hari kerja\r\n\r\n\r\nForce Majeure:\r\nPengembalian dana penuh atau penggantian jadwal', 487500, '2020-10-01', '2021-01-01', 16, 13, 1, 'Desa Wanurejo', 'vw2.jpg'),
(33, 'Cooking Lesson: Makanan Khas Jawa', 'Kelas Memasak', '3 jam', 'Berkeliling desa Candirejo dengan Dokar untuk menikmati kekayaan kearifan lokal Desa Candirejo khususnya dalam kuliner. Mencari sayur di lahan pertanian dan bumbu dapur di warung warga. Kemudian singgah di rumah penduduk untuk belajar masakan tradisional jawa merupakan sebuah pengalaman yang patut untuk dicoba.\r\n', 'Ketentuan Pengembalian Dana:\r\nPengembalian dana akan diproses dalam waktu 5 hari kerja\r\nJika timbul biaya transfer, maka akan dikurangi dari total pengembalian dana', 1050000, '2020-10-01', '2021-02-01', 16, 13, 1, 'Joglo Bu Condro Magelang', 'masak.jpg'),
(34, 'Open Trip Pulau Peucang Ujung Kulon 2h1m Start Pelabuhan Sumur/Dermaga Taman Jaya', 'Open Trip', '2 Hari', 'Destinasi: Pulau Peucang, Treking Pohon Kiara ( Arah menuju Karang Copong), Snorkeling Ciapus & Suminoh, Cidaon (Savana Banteng & Hunting Sunset), Sungai Cigenter (Canoing), Pulau Badul', 'Tidak ada pengembalian dana, namun tersedia penjadwalan ulang dengan menghubungi admin H-4 sebelum Keberangkatan. \r\nPembatalan H-3 sebelum keberangkatan akan dipotong 50% dari biaya Paket wisata. \r\n\r\npembatalan H-1 atau dihari pelaksanaan peserta tidak hadir di meeting point akan dikenakan biaya 100% ', 600000, '2020-10-01', '2021-02-01', 17, 17, 1, 'Pulau Peucang', 'peucang1.jpg'),
(35, 'Rental Speedboat Multifungsi', 'Sewa Speedboat', '3 jam', 'rental speedboat untuk berolahraga di air, memancing, dan menyeberang', 'Ada baiknya Anda menyiapkan stamina dan fisik dengan istirahat yang cukup sebelum naik speedboat.', 1200000, '2020-10-01', '2021-02-01', 17, 17, 2, 'Pulau Peutjang', 'speedboat.jpg'),
(36, 'Open Trip One Day Sailing Komodo Island', 'Open Trip', '1 Hari', 'Kelilingi Pulau Komodo yang indah. Anda akan disajikan dengan indahnya Pantai Pasir Pink, mengunjungi Pulau Komodo, snorkeling di Manta Point, dan mengunjungi Pulau Kanawa', 'Ketentuan Pembatalan:\r\nTidak dapat dibatalkan kecuali karena Force Majeure', 1350000, '0000-00-00', '0000-00-00', 18, 13, 1, 'Pulau Komodo', '1day.jpg'),
(37, 'Komodo Airport Taxi Service', 'Jasa Penjemputan', '1 Jam', 'Kami adalah perusahan jasa taxi/transport di Komodo Airport, Labuan Bajo. Harga voucher hanya berlaku untuk one way ransfer dalam kota Labuan Bajo. Untuk pleayanan keluar dari kota Labuan Bajo akan dikenakan biaya tambahan @ IDR 100,000/jarak 4km', 'Layanan maximal penumpang 4 orang, jika jumlah penumpang lebi dari 4 orang kami berhak untuk tidak mengizinkan penumpang tambahan. \r\n\r\n', 70000, '2020-10-01', '2021-02-01', 18, 8, 2, 'Pulau Komodo', 'avanxa.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `id_provider` int(11) NOT NULL,
  `nama_provider` varchar(50) NOT NULL,
  `tagline` varchar(100) NOT NULL,
  `tentang` varchar(255) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nohp` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fb` varchar(50) DEFAULT NULL,
  `ig` varchar(50) DEFAULT NULL,
  `banner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`id_provider`, `nama_provider`, `tagline`, `tentang`, `alamat`, `nohp`, `email`, `fb`, `ig`, `banner`) VALUES
(1, 'Odiranew Trans Bali', 'STANDARD & LUXURY TRANSPORT', 'We provide car rental services in Jogyakarta, Surabaya, Makassar, Bali and Lombok for your vocation / activity needs.\r\n\r\nOur experiance has 15 years serving various companies, agencies that have worked with us.\r\n\r\nOrder now we will definitely prepare for ', 'Jl.Perum Tropic Residence Blok C.3 Jimbaran Taran Griya, Bali - Indonesia 80361', '+6281236340026', 'odira.transbali72@gmail.com', 'odira bali', 'odirabali', 'OdiraBanner.jpg'),
(3, 'Binda Rental', 'Binda Rental menyediakan berbagai persewaan armada mobil di Klaten', 'Binda Rental menyediakan berbagai persewaan armada mobil di Klaten.\r\n\r\n \r\n\r\nArmada mobil kami mulai dari Avanza, Xenia, Innova, Elf, Hiace, dan Bus.', 'Jln. Rajawali 107, Kel. Kabupaten, Kec. Klaten Tengah, Klaten.', '6285729434045', 'sugassupratman66@gmail.com', '', '', 'BindaBanner.jpg'),
(4, 'Abad Transport', 'Abad Transport menyediakan berbagai persewaan mobil, mulai dari yang Matic maupun Manual', 'Abad Transport menyediakan berbagai persewaan mobil, mulai dari yang Matic maupun Manual. Kami juga menawarkan berbagai mobil yang sesuai dengan selera anda. Contohnya, Agya dan Brio. Ada beberapa paket yuntuk mahasiswa juga, sangat mudah pesan nya dan ba', '', '081804033123', 'ibnunuralfian@gmail.com', 'tngunungcermai@gmail.com', 'abad_transport_jogja', 'AbadTransport.jpg'),
(5, 'Karavan Holiday', 'tempat nya sewa transportasi', 'sewa bus bekasi merupakan jasa layanan transportasi terpercaya, kami melayani beberapa armada diantra nya yaitu Hiace Commuter 15 seat,  ELF SHORT 15 seat, ELF LONG 19 seat, Medium Bus 31 seat Big Bus 44, 47 & 59 seat', '', '081282361836', 'sewabusbekasi@yahoo.com', 'sewa bus bekasi', '@sewabus_bekasi', 'KaravanBekasi.jpg'),
(6, 'Bali Semara Tour', 'Bali Transport Services', 'We offer you affordable price and comfortable car for Bali transport services include professional driver to support your journey with us in Bali. you may arrange your own itineraries cruising the island, if you don’t  have any plans, you can following ou', 'Gang Taman Beji 1, Blok B1 No 1. Batubulan, Bali', '+6287861414008', 'info@balisemaratour.com', '', '', 'bali-transport-services.jpg'),
(7, 'Safari Jogja Transport', 'KAMI ADA UNTUK ANDA', 'Safari Jogja Transport memberikan Jasa Transport dalam & luar kota bagi anda, KAMI ADA UNTUK  menemani  ANDA. KAMI ADA UNTUK melayani ANDA.', 'Kantor: Jln Kadisono, sampaan Berbah. Gg Puntodewo No 9.   Garasi: Jln Kayen, Maguwoharjo.', '6285740018009', 'jalanjalanjogjaid@gmail.com', ' jalanjalanjogjaid@gmail.com', 'jalanjalanjogjaid', '1561683591-banner-safari-jogja-transport.png'),
(8, 'Maha Bisnis Tour', 'Kenyamanan, Keamanan dan Kepuasan Pelanggan Yang Paling Utama', 'jasa Perjalanan Wisaa  (Lombok Travel Services )\r\n\r\nMelayani Perjalanan Wisata Murah Lombok Diantaranya (Serving Lombok Travel Cheap Among them)\r\n\r\nAntar Jemput Bandara & Pelabuhan (Airport & Port Shuttle)\r\nTour Lombok (Lombok Tour)\r\nFull Day\r\nPrivate Car', 'Jl. Sidemen,Dusun Eat Greneng, Desa Lembahsari, Kecamatan Batulayar, Kab. Lombok Barat - NTB', '6287851936910', 'mahabisnis46@gmail.com', 'bajang.dese.14418', 'raden_wongenom', 'maha.jpg'),
(10, 'Kusuma Lombok Tour', 'Wisata Lombok', '1. Menjadi perusahaan biro perjalanan wisata , (Tour Travel Agent di Pulau Lombok ) yang terdepan dan terpercaya.\r\n\r\n2. Selalu berusaha menyediakan/memberikan pelayanan terbaik dari yang lebih baik dan berkomitmen membuat perjalanan wisata yang unik serta', 'Dusun Rebile, Desa Tanak Awu', '6285333035035', 'kusumatourlombok@gmail.com', 'kusumalombok', 'kusumalomboktour', 'bannerkusuma.jpg'),
(11, 'Mazu Divers', 'Mazu Divers adalah Dive Center yang tepat untuk anda yang ingin belajar menyelam dan trip selam di J', 'Mazu Divers adalah SSI (SCUBA SCHOOL INTRNATIONAL) Dive Center di Jakarta, berlokasi di Pulau Pramuka, Kepulauan Seribu. Kami menyediakan Paket trip menyelam dan paket pelatihan selam mulai dari Open water, Advanced, Specialty course , Dive master, Dive g', 'Gedung TPI Pulau Pramuka Kios No 1  Kel. Pulau Panggang Kec. Kepulauan Seribu Utara', '62811977760', 'marketing@mazudivers.com', 'mazudivers', 'mazudivers', 'mazubanner.jpg'),
(12, 'Karya Wisata Pahawang', 'Karya Wisata Pahawang', 'Nikmati keindahan Pahawang bersama kami', 'Pulau Pahawang , marga punduh', '6285273637427', 'pahawangkaryawisata@gmail.com', 'karya wisata pahawang', 'pulau_pahawang', 'pahawangbanner.jpg'),
(13, 'Sekooter', 'Keliling dunia bukan cuma mimpi lagi dengan Sekooter', 'Sekooter adalah agen tur yang memiliki kantor operasional yang berlokasi di berbagai belahan dunia, menyediakan paket tur mewah harga murah.', 'Jl. Nitipuran No.88, Sonosewu, Ngestiharjo, Kec. Kasihan, Bantul, Daerah Istimewa Yogyakarta 55184', '6282252747272', 'info.sekooter@gmail.com', 'Sekooter', 'sekooterholiday', 'sekoter.jpg'),
(14, 'Bakas Cooking Class', 'I make sure you are memorable in our class', 'Bakas cooking class is one of cooking class in the midlle east part of Bali. Before the class will be escourt you to market ,rice field and Bali house. Cooking classes will be held in native homes. You will feel a different atmosphere in our place, the at', 'Br kreteg dusun kawan desa bakas klungkung,bali,indonesia', '628123816525', 'bakascookingclass@gmail.com', 'Bakas Cooking Class', 'bakas.cookingclass', 'bakas.jpg'),
(15, 'e-Lombok', 'Geopark Rinjani Ticketing', 'e-Lombok merupakan kolaboratif pariwisata lokal yang mengedepankan prinsip pemerataan ekonomi lokal, konservasi lingkungan, dan penguatan sistem sosial-budaya masyarakat pariwisata Lombok. Kami menyediakan sistem booking yang aman dan nyaman untuk mengeks', 'Rinjani Geopark, Sembalun Lawang, Sembalun, Kabupaten Lombok Timur, Nusa Tenggara Bar.', '6281947017670', 'e.lombok.geopark@gmail.com', 'geoparkrinjaniticketing', 'geoparkrinjaniticketing', 'elombok.jpg'),
(16, 'TurBro', 'Pengalaman luar biasa untuk pribadi yang luar biasa', 'TurBro adalah penyedia jasa pariwisata online. Kami menyediakan jasa wisata yang berbeda dan unik. Untuk para pribadi yang mencari pengalaman unik dan berkesan. Ayo tunggu apa lagi, warnai hidupmu bersama TurBro! ', 'Syntesis square  Jl. Gatot Subroto No.177A, RT.9/RW.1, Menteng Dalam, Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12870', '082135210797', 'tourbroindonesia@gmail.com', 'TourBro Indonesia', 'tourbroindonesia', 'turbro.jpg'),
(17, 'Ujung Kulon Adventure', 'Operator Tour Lokal Taman Nasional Ujung Kulon', 'Operator Tour Lokal Wisata Taman Nasional Ujung Kulon Menyediakan paket  Private dan Open Trip yang berangkat setiap Hari Jumat - Minggu.Kami juga menyediakan paket mancing, diving, Ziarah Di daerah Banten serta paket outing kantor, family gathering, dan ', 'Kampung Haseum RT 002 RW 002 Desa Cigorondong Kecamatan Sumur Kabupaten Pandeglang Provinsi Banten', '6281906217228', 'ujungkulonadventure@gmail.com', 'Ujung kulon', 'ujungkulon_adventure', 'ujungkulon.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL,
  `nama_provinsi` varchar(50) NOT NULL,
  `banner` varchar(2555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`, `banner`) VALUES
(1, 'Aceh', 'AcehDest.jpg'),
(2, 'Sumatera Utara', 'SumutDest.jpg'),
(3, 'Sumatera Barat', 'SumbarDest.jpg'),
(4, 'Riau', 'RiauDest.jpg'),
(5, 'Jambi', 'JambiDest.jpg'),
(6, 'Lampung', 'LampungDest.jpg'),
(7, 'DKI Jakarta', 'DKIDest.jpg'),
(8, 'Jawa Barat', 'JabarDest.jpg'),
(9, 'Jawa Tengah', 'JatengDest.jpg'),
(10, 'DI Yogyakarta', 'DIYDest.jpg'),
(11, 'Jawa Timur', 'JatimDest.jpg'),
(12, 'Banten', 'BantenDest.jpg'),
(13, 'Bali', 'BaliDest.jpg'),
(14, 'Nusa Tenggara Barat', 'NTBDest.jpg'),
(15, 'Nusa Tenggara Timur', 'NTTDest.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahasa`
--
ALTER TABLE `bahasa`
  ADD PRIMARY KEY (`id_bahasa`);

--
-- Indexes for table `brbahasa`
--
ALTER TABLE `brbahasa`
  ADD PRIMARY KEY (`id_brbahasa`),
  ADD KEY `id_bahasa` (`id_bahasa`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `brdetcat`
--
ALTER TABLE `brdetcat`
  ADD PRIMARY KEY (`id_brdetcat`),
  ADD KEY `id_detcat` (`id_detcat`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `brexcl`
--
ALTER TABLE `brexcl`
  ADD PRIMARY KEY (`id_brexcl`),
  ADD KEY `id_exclusion` (`id_exclusion`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `brincl`
--
ALTER TABLE `brincl`
  ADD PRIMARY KEY (`id_brincl`),
  ADD KEY `id_inclusion` (`id_inclusion`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `detcat`
--
ALTER TABLE `detcat`
  ADD PRIMARY KEY (`id_detcat`);

--
-- Indexes for table `exclusion`
--
ALTER TABLE `exclusion`
  ADD PRIMARY KEY (`id_exclusion`);

--
-- Indexes for table `inclusion`
--
ALTER TABLE `inclusion`
  ADD PRIMARY KEY (`id_inclusion`);

--
-- Indexes for table `itinerary`
--
ALTER TABLE `itinerary`
  ADD PRIMARY KEY (`id_itinerary`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`),
  ADD KEY `id_provinsi` (`id_provinsi`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `id_kota` (`id_kota`),
  ADD KEY `id_provider` (`id_provider`),
  ADD KEY `id_category` (`id_category`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id_provider`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahasa`
--
ALTER TABLE `bahasa`
  MODIFY `id_bahasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `brbahasa`
--
ALTER TABLE `brbahasa`
  MODIFY `id_brbahasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `brdetcat`
--
ALTER TABLE `brdetcat`
  MODIFY `id_brdetcat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `brexcl`
--
ALTER TABLE `brexcl`
  MODIFY `id_brexcl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `brincl`
--
ALTER TABLE `brincl`
  MODIFY `id_brincl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detcat`
--
ALTER TABLE `detcat`
  MODIFY `id_detcat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `exclusion`
--
ALTER TABLE `exclusion`
  MODIFY `id_exclusion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inclusion`
--
ALTER TABLE `inclusion`
  MODIFY `id_inclusion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itinerary`
--
ALTER TABLE `itinerary`
  MODIFY `id_itinerary` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `id_picture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `id_provider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brbahasa`
--
ALTER TABLE `brbahasa`
  ADD CONSTRAINT `brbahasa_ibfk_1` FOREIGN KEY (`id_bahasa`) REFERENCES `bahasa` (`id_bahasa`),
  ADD CONSTRAINT `brbahasa_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `brdetcat`
--
ALTER TABLE `brdetcat`
  ADD CONSTRAINT `brdetcat_ibfk_1` FOREIGN KEY (`id_detcat`) REFERENCES `detcat` (`id_detcat`),
  ADD CONSTRAINT `brdetcat_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `brexcl`
--
ALTER TABLE `brexcl`
  ADD CONSTRAINT `brexcl_ibfk_1` FOREIGN KEY (`id_exclusion`) REFERENCES `exclusion` (`id_exclusion`),
  ADD CONSTRAINT `brexcl_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `brincl`
--
ALTER TABLE `brincl`
  ADD CONSTRAINT `brincl_ibfk_1` FOREIGN KEY (`id_inclusion`) REFERENCES `inclusion` (`id_inclusion`),
  ADD CONSTRAINT `brincl_ibfk_2` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `itinerary`
--
ALTER TABLE `itinerary`
  ADD CONSTRAINT `itinerary_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `kota`
--
ALTER TABLE `kota`
  ADD CONSTRAINT `kota_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id_provinsi`);

--
-- Constraints for table `picture`
--
ALTER TABLE `picture`
  ADD CONSTRAINT `picture_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`);

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`),
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`id_provider`) REFERENCES `provider` (`id_provider`),
  ADD CONSTRAINT `produk_ibfk_3` FOREIGN KEY (`id_category`) REFERENCES `category` (`id_category`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
