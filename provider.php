<?php
include "connection.php"
?>
<!doctype html>
<html lang="en">
<head>
    <title>sewa bus pariwisata bekasi</title>
<meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/landing/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://sewabusbekasi.gomodo.id/css/material.css" rel="stylesheet">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/app.css">
    <link rel="shortcut icon" href="https://sewabusbekasi.gomodo.id/dest-operator/img/logo1.png">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/badge-company.css">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/badge-discount.css">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/customer-inline-fix.css">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://sewabusbekasi.gomodo.id/css/owl.theme.default.min.css">
    <style>
        .owl-nav {
            display: none !important;
        }

        .owl-stage {
            display: flex;
        }

        #banner {
            height: auto;
            width: 100%;
        }

        #banner img {
            width: 100%;
            max-height: 37rem;
        }

        .pagination-product .pagination .disabled {
            background-color: white;
            color: #3099fb;
        }

        html {
            scroll-behavior: smooth
        }

        .col-top-product .card {
            height: 100%;
        }

        .col-top-product .card .card-img-top {
            height: 200px;
            object-fit: cover;
        }

        .col-top-product .card span.product-tags {
            max-width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .col-top-product a {
            color: #3a4555;
            text-decoration: none;
        }

        .col-top-product h3 {
            font-size: 20px;
            font-weight: 700;
        }

        .col-top-product .card:hover {
            -webkit-transform: scale(1.01);
            transform: scale(1.01);
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
        }

        #list-product .col-top-product {

        }

        #container-top-product {
            padding: 64px 0px;
            background-color: #fafafa;
            margin-bottom: 64px;
        }
        *[class^='Main__ItemsContainer']{
            display: none;
        }

    </style>

    <style>
        .address {
            color: #fff;
            padding-bottom: 0.8rem;
            text-align: justify;
        }

        .address p {
            margin-bottom: 0px;
        }

        footer .powered {
            height: auto;
        }

        footer .phone-company span {
            display: inline-flex;
        }

        footer .phone-company {
            margin-top: 1rem;
        }

        footer .phone-company img {
            width: 20px;
        }

        footer p.about, footer .address #address, footer .social {
            font-weight: bold;
            margin-bottom: 0.6rem;
            margin-top: 0;
        }

        footer .address #address {
            margin-bottom: 0.95rem;
        }

        footer .pt-5 {
            padding-top: 2.5rem !important;
        }

        nav.navbar + section {
            margin-top: 49px !important;
        }

        #btn-navbar-toggle {
            cursor: pointer;
            background: none;
            border: none;
            display: none;
        }

        .relative img {
            width: 1.3rem;
        }

        .fa.fa-bars {
            color: #2699FB;
            font-size: 1.5rem;
        }

        .owl-nav .owl-next.disabled, .owl-nav .owl-prev.disabled {
            display: none
        }

        @media  only screen and (max-width: 480px) {
            #btn-navbar-toggle {
                display: block;
            }

            .collapse.navbar-collapse {
                background: #f1f8fd;
                margin-left: -1rem;
                margin-right: -1rem;
                border-top: 1px solid #dcdbdb;
            }

            footer .address #address {
                margin-top: 35px;
            }
        }
    </style>

</head>
<body>
<div class="loading">
    <div class="loading-content">
        <div class="spin">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
        <div class="loading-text">
            Memuat..
        </div>
    </div>
</div>
<nav class="navbar navbar-dark navbar-expand-md bg-style-1 container-fluid navbar-fixed-top" id="topNavbar">
    <div class="container">
        <?php
        $sqlProvider = "SELECT * FROM provider WHERE id_provider=".$_GET['id'];
        $queryProvider = mysqli_query($con, $sqlProvider);
        $getProvider = mysqli_fetch_assoc($queryProvider);
        ?>
        <input type="hidden" name="provider" value="<?php echo $_GET["id"];?>">
        <a href="provider.php?id=<?php echo $_GET['id'];?>" class="navbar-brand">
            <img src="Images/logo1.png" alt="">
        </a>
        <div class="profile-info">
            <h3 id="company-name"><?php echo $getProvider['nama_provider'];?></h3>
        </div>
        <span class="relative">
                            </span>
        <button id="btn-navbar-toggle" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item text-center" id="landing-language" role="presentation">
                    <div class="current-language">
                        <div class="description-current-language">
                            Indonesia
                        </div>
                        <div class="flag-current-language">
                            <img class="img-circle" src="https://sewabusbekasi.gomodo.id/landing/img/idn.png" alt="">
                        </div>
                        <div class="caret-language">
                            <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="language-option" style="display: none;">
                        <ul>
                            <li data-value="en" class="pick-lang">
                                <div class="box-language">
                                    <div class="description-language">
                                        English
                                    </div>
                                    <div class="flag-language">
                                        <img src="https://sewabusbekasi.gomodo.id/landing/img/uk.png" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content">
    <div class="container-fluid" id="banner">

        <img src="Images/<?php echo $getProvider['banner'];?>" class="img-fluid" alt="">
    </div>

    <div class="intro container py-5" id="intro" style="padding-bottom: 1rem!important;">
        <div class="row">
            <div class="col-12 text-center">
                <p><?php echo $getProvider['tagline'];?></p>
            </div>
        </div>
    </div>
    <div class="container mb-5" id="search-section">
        <div class="row">
<!-- TODO: Cek Styling -> Error kalo pake form padahal kan penting
           <form action="provider.php" method="post">-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="search">Cari</label>
                        <input class="form-control" placeholder="Cari Berdasarkan nama, dsb.." name="keyword" type="search" onchange="filter_data()">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="location">Lokasi</label>
                        <select class="form-control" name="city" onchange="filter_data()">
                            <option selected="selected" value="">Pilih Kota</option>
                        <?php
                        $sqlLokasi = "SELECT DISTINCT kota.id_kota, kota.nama_kota FROM kota INNER JOIN produk ON produk.id_kota = kota.id_kota WHERE produk.id_provider=".$_GET['id'];
                        $queryLokasi = mysqli_query($con, $sqlLokasi);
                        while ($rowLokasi = mysqli_fetch_assoc($queryLokasi)){
                            ?>

                                <option value="<?php echo $rowLokasi['id_kota'];?>"><?php echo $rowLokasi['nama_kota'];?></option>
                        <?php
                        }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="sort">Urut Berdasarkan</label>
                        <select class="form-control" name="sortBy" onchange="filter_data()">
                            <option value="" selected="selected">Urut Berdasarkan</option>
                            <option value="newest">Terbaru</option>
                            <option value="oldest">Terdahulu</option>
                            <option value="cheapest">Termurah</option>
                            <option value="most_expensive">Termahal</option>
                            <option value="name_asc">Alphabet A - Z</option>
                            <option value="name_desc">Alphabet Z - A</option>
                        </select>
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
    <div class="container-fluid" id="list-product">
        <div class="row">
            <div class="container">
                <div class="row product-feed">

                </div>
            </div>
        </div>

    </div>
</section>
<footer class="container-fluid bg-dark">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-4 text-white mb-md-0 mb-3 text-justify">
                <p class="text-white about">
                    Tentang kami
                </p>
                <p class="text-white order-0 about-us">
                <p><?php echo $getProvider['tentang'];?></p>
                <p>&nbsp;</p>
                </p>
            </div>
            <div class="col-md-4 order-1 order-lg-1 d-flex">
                <div class="address">
                    <div class="phone-company">
                                <span class="text-white">
                                    <img src="https://sewabusbekasi.gomodo.id/img/048-telephone-1.png" width="16" alt="">
                                    <span><?php echo $getProvider['nohp'];?></span>
                                </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-5">
                <ul class="social">
                    <p>Media Sosial</p>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="https://sewabusbekasi.gomodo.id/img/159-email.png" alt=""> <?php echo $getProvider['email'];?>
                        </a>

                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="https://sewabusbekasi.gomodo.id/img/099-facebook.png" alt=""> <?php echo $getProvider['fb'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="https://sewabusbekasi.gomodo.id/img/080-instagram.png" alt=""> @<?php echo $getProvider['ig'];?>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <div class="powered">
                    <span>POWERED BY</span> <span class="bold"><a href="home.php">GOMODO</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://sewabusbekasi.gomodo.id/landing/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script type="text/javascript" src="https://sewabusbekasi.gomodo.id/js/public/material.js"></script>

<script>
    $(document).on('click', '#btn-navbar-toggle', function () {
        let t = $(this);
        $(t.data('target')).slideToggle(100);
    });

    function loadingStart() {
        $('.loading').addClass('show');
    }

    function loadingFinish() {
        $('.loading').removeClass('show');
    }

    toastr.options = {
        "positionClass": "toast-bottom-right",
    };
    $(document).on('click', '.current-language', function () {
        $('.language-option').slideToggle(300)
    });

    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    // Prevent Char in Input Type Number
    $('input[type="number"]').on('change keydown', function onChange(e) {
        if (e.metaKey == false) { // Enable metakey
            if (e.keyCode > 13 && e.keyCode < 48 && e.keyCode != 39 && e.keyCode != 37 || e.keyCode > 57) {
                e.preventDefault(); // Disable char. Enable arrow
            }
            ;
            if (e.shiftKey === true) { // Disable symbols
                if (e.keyCode > 46 && e.keyCode < 65) {
                    e.preventDefault();
                }
            }
        }
    })
    filter_data();

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function filter_data()
    {
        $('.product-feed').html('<div id="loading" style="" ></div>');
        var action = 'fetch_data_provider';
        var provider = $('input[name=provider]').val();
        var keyword = $('input[name=keyword]').val();
        var urut = $('select[name=sortBy]').val();
        var city = $('select[name=city]').val();
        console.log(urut);

        // var price: price;

        $.ajax({
            url:"fetch-data-provider.php",
            method:"POST",
            data:{
                action:action,
                // q:q,
                provider: provider,
                keyword: keyword,
                urut: urut,
                city: city

            },
            success:function(data){
                $('.product-feed').html(data);

            }
        });
    }

    function get_filter(class_name)
    {
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        return filter;
    }

    $('.common_selector').click(function(){
        filter_data();
    });

</script>
<script src="https://sewabusbekasi.gomodo.id/js/owl.carousel.min.js"></script>

</body>
</html>
